package com.nmg.apps.deviceinfo;

import android.content.Context;
//import android.support.test.InstrumentationRegistry;
//import android.support.test.runner.AndroidJUnit4;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.namogoo.android.namogoolib.NMGLibrary;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class NMGLibraryTest {

    @Test
    public void GetVendorId_EqualToInput_True() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        String vID = "VID123456";
        NMGLibrary.newInstance(appContext, "VID123456");
        String nmgVid = NMGLibrary.getInstance().getVendorId();
        assertEquals(vID, nmgVid);
    }
}
