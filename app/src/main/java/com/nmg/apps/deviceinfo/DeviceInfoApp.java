package com.nmg.apps.deviceinfo;

import android.app.Application;

import com.namogoo.android.namogoolib.NMGLibrary;

import java.util.UUID;


/**
 * Created by Udi on 12/04/2020.
 */

public class DeviceInfoApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        String vendorId = "GA4D28O5Y";
        String vendorSessionId = UUID.randomUUID().toString();
        String vendorCustomerId = "xxyyy1234";
        NMGLibrary.newInstance(getApplicationContext(), vendorId, vendorSessionId, vendorCustomerId);
    }
}
