package com.nmg.apps.deviceinfo.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent
import com.namogoo.android.namogoolib.helper.Utils
import com.nmg.apps.deviceinfo.MainActivity
import com.nmg.apps.deviceinfo.R
import java.text.SimpleDateFormat
import java.util.*

class CustomerJourneyAdapter( list: ArrayList<CustomerJourneyEvent>, internal var mActivity: MainActivity) : RecyclerView.Adapter<CustomerJourneyAdapter.CJViewHolder>(){
    private var list: ArrayList<CustomerJourneyEvent> = list

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CJViewHolder {
        val itemView = LayoutInflater.from(mActivity).inflate(R.layout.row_customer_journey, parent, false)
        return CJViewHolder(itemView)
    }

    fun setEvents(newEventsList:  ArrayList<CustomerJourneyEvent>) {
        this.list.clear()
        this.list.addAll(newEventsList)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CJViewHolder, position: Int) {
         holder?.bindData(list[position])
    }

    inner class CJViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(customerJourneyEvent: CustomerJourneyEvent) {

            val tvEventType: TextView? = itemView.findViewById(R.id.tv_event_type);
            val tvName: TextView? = itemView.findViewById(R.id.tv_event_name)
            val tvData: TextView? = itemView.findViewById(R.id.tv_event_data)
            val tvTime: TextView? = itemView.findViewById(R.id.tv_event_time)

            var typeToDisplay = if (customerJourneyEvent.lifecycleEventData != null) customerJourneyEvent.lifecycleEventData.eventType.name else customerJourneyEvent.userInteractionEventData.eventType.name
            //if (typeToDisplay.startsWith("ACTIVITY_")) {
            //    typeToDisplay = typeToDisplay.substring((9))
            //}
            tvEventType?.text = typeToDisplay.replace("_", " ");

            tvName?.text = if (customerJourneyEvent.lifecycleEventData != null) customerJourneyEvent.lifecycleEventData.targetId else customerJourneyEvent.userInteractionEventData.targetId
            var value: String?
            value = if (customerJourneyEvent.lifecycleEventData != null) "" else customerJourneyEvent.userInteractionEventData.value
            if (value == null)
            {
                value = ""
            }
            //if (customerJourneyEventPojo.targetHierarchy != null)
            //{
                //value = customerJourneyEventPojo.targetHierarchy
            //}
            tvData?.text =   value
            var time : Long
            time = if (customerJourneyEvent.lifecycleEventData != null) customerJourneyEvent.lifecycleEventData.eventTime else customerJourneyEvent.userInteractionEventData.eventTime
            tvTime?.text = Utils.getDateStringSeconds(time);

        }
    }
}