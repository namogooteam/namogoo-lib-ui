package com.nmg.apps.deviceinfo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.namogoo.android.namogoolib.pojo.info.DependencyPojo
import com.nmg.apps.deviceinfo.MainActivity
import com.nmg.apps.deviceinfo.R
import java.util.*

class DependenciesAdapter(internal var list: ArrayList<DependencyPojo>, internal var mActivity: MainActivity) : RecyclerView.Adapter<DependenciesAdapter.DependencyViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DependencyViewHolder {
        val itemView = LayoutInflater.from(mActivity).inflate(R.layout.row_dependency, parent, false)
        return DependencyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: DependencyViewHolder, position: Int) {
         holder?.bindData(list[position])
    }

    inner class DependencyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(dependencyPojo: DependencyPojo) {

            val tvName: TextView? = itemView.findViewById(R.id.tv_name);
            val tvData: TextView? = itemView.findViewById(R.id.tv_data)

            tvName?.text = dependencyPojo.name
            tvData?.text = dependencyPojo.version

        }
    }
}