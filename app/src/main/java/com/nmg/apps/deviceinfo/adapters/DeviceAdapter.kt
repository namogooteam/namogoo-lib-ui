package com.nmg.apps.deviceinfo.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.namogoo.android.namogoolib.helper.Utils
import com.namogoo.android.namogoolib.pojo.info.AppInfo
import com.nmg.apps.deviceinfo.MainActivity
import com.nmg.apps.deviceinfo.R
import com.nmg.apps.deviceinfo.utilities.Methods


/**
 * Created by Udi
 */

class DeviceAdapter(internal var flag: Int?, internal var appslist: ArrayList<AppInfo>, internal var mActivity: MainActivity) : RecyclerView.Adapter<DeviceAdapter.DeviceVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceAdapter.DeviceVH {
        val itemView = LayoutInflater.from(mActivity).inflate(R.layout.row_infomation, parent, false)
        return DeviceVH(itemView)
    }

    override fun onBindViewHolder(holder: DeviceAdapter.DeviceVH, position: Int) {
        holder?.bindData(appslist[position])
    }

    override fun getItemCount(): Int {
        return appslist.size
    }

    inner class DeviceVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(deviceInfo: AppInfo) {

            val ivAppLogo: ImageView? = itemView.findViewById(R.id.iv_app_icon)
            val tvAppname: TextView? = itemView.findViewById(R.id.tv_app_name)
            val tvAppPackageName: TextView? = itemView.findViewById(R.id.tv_app_package_name)
            val tvAppPackageData: TextView? = itemView.findViewById(R.id.tv_app_package_data);

            tvAppname?.text = deviceInfo.appLable
            tvAppPackageName?.text = deviceInfo.packageName + " - " + deviceInfo.versionName
            ivAppLogo?.setImageDrawable(deviceInfo.appLogo)
            tvAppPackageData?.text = mActivity.getString(R.string.app_data, Utils.getDateString(deviceInfo.installTime), Utils.getDateString(deviceInfo.lastUpdateTime))

            itemView.setOnClickListener({
                Methods.avoidDoubleClicks(itemView)

                val launchIntent = mActivity.packageManager.getLaunchIntentForPackage(deviceInfo.packageName)
                if (launchIntent != null) {
                    startActivity(mActivity, launchIntent, null)
                }
            })
        }
    }
}
