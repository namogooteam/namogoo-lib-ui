package com.nmg.apps.deviceinfo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nmg.apps.deviceinfo.MainActivity
import com.nmg.apps.deviceinfo.R
import com.nmg.apps.deviceinfo.models.SimInfo
import com.nmg.apps.deviceinfo.utilities.Methods

/**
 * Created by Udi
 */
class SimAdapter(private val mainActivity: MainActivity, private var simInformationData: ArrayList<SimInfo>): RecyclerView.Adapter<SimAdapter.SimVH>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimVH {
        val itemView = LayoutInflater.from(mainActivity).inflate(R.layout.row_sim_item, parent, false)
        return SimVH(itemView)
    }

    override fun getItemCount(): Int = simInformationData.size

    override fun onBindViewHolder(holder: SimVH, position: Int) {
        holder?.bindData(simInformationData[position])
    }

    inner class SimVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bindData(simInfo: SimInfo) {

            val tvLabel: TextView? = itemView.findViewById(R.id.tvLabel)
            val tvSimInformation: TextView? = itemView.findViewById(R.id.tvSimInformation)

            tvLabel?.text = simInfo.simLable
            tvSimInformation?.text = simInfo.simData

            itemView.setOnClickListener({
                Methods.avoidDoubleClicks(itemView)
            })
        }
    }
}