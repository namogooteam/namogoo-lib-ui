package com.nmg.apps.deviceinfo.fragments

import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.annotation.RequiresApi
import com.namogoo.android.namogoolib.NMGLibrary
import com.nmg.apps.deviceinfo.R
import kotlinx.android.synthetic.main.fragment_contact_us.*


class ContactUsFragment : BaseFragment(), View.OnClickListener {

    var ivMenu: ImageView? = null
    var ivBack: ImageView? = null
    var tvTitle: TextView? = null

    var etName: EditText? = null
    var etEmail: EditText? = null
    var etComments: EditText? = null
    var btnSumbit: Button? = null

    var mName: String = ""
    var mEmail: String = ""
    var mComments: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contact_us, container, false)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = activity!!.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(R.color.colorPrimaryDark)
            window . navigationBarColor = resources . getColor (R.color.colorPrimaryDark)

        }

        ivMenu = view.findViewById(R.id.iv_menu)
        ivBack = view.findViewById(R.id.iv_back)
        tvTitle = view.findViewById(R.id.tv_title)

        etComments = view.findViewById(R.id.et_comments)
        etEmail = view.findViewById(R.id.et_mail)
        etName = view.findViewById(R.id.et_name)
        btnSumbit = view.findViewById(R.id.submit)

        etName?.addTextChangedListener(object:TextWatcher {
            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                mName = s.toString()
                verifyAllInputs()
            }
        })

        etEmail?.addTextChangedListener(object:TextWatcher {
            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                mEmail = s.toString()
                verifyAllInputs()
            }
        })

        etComments?.addTextChangedListener(object:TextWatcher {
            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                mComments = s.toString()
                verifyAllInputs()
            }
        })

        btnSumbit?.setOnClickListener(View.OnClickListener {
            submitForm()
        })
        return view
    }

    private fun verifyAllInputs() {
        submit.isEnabled = mName.isNotEmpty() && mEmail.isNotEmpty() && mComments.isNotEmpty()
    }

    private fun submitForm() {
        //NMGLibrary.getInstance().getFireBaseDB().sendComment(mName, mEmail, mComments)
        etComments?.setText("")
        etName?.setText("")
        etEmail?.setText("")

        Toast.makeText(context, getString(R.string.sent_comments), Toast.LENGTH_LONG).show()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initToolbar()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden && isAdded) {
            initToolbar()
        }
    }

    private fun initToolbar() {
        ivMenu?.visibility = View.VISIBLE
        ivBack?.visibility = View.GONE
        tvTitle?.text = mResources.getString(R.string.connect_us)
        ivMenu?.setOnClickListener {
            mActivity.openDrawer()
        }
    }

    override fun onClick(p0: View?) {

    }

}