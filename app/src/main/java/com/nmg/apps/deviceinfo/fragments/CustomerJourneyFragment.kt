package com.nmg.apps.deviceinfo.fragments

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.namogoo.android.namogoolib.NMGLibrary
import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent
import com.nmg.apps.deviceinfo.R
import com.nmg.apps.deviceinfo.adapters.CustomerJourneyAdapter
import kotlinx.android.synthetic.main.toolbar_ui.*
import java.util.ArrayList
import java.util.Observer

class CustomerJourneyFragment: BaseFragment() {

    var rvCJList: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
//        return inflater.inflate(R.layout.fragment_blue_tooth, container, false)
        val contextThemeWrapper = ContextThemeWrapper(activity, R.style.CustomerJourneyTheme)
        val localInflater = inflater.cloneInContext(contextThemeWrapper)
        val view = localInflater.inflate(R.layout.fragment_customer_journey, container, false)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = activity!!.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(R.color.dark_purple)
            window.navigationBarColor = resources.getColor(R.color.dark_purple)

        }
        rvCJList = view.findViewById(R.id.rv_cj_list)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initToolbar()
        rvCJList?.layoutManager = LinearLayoutManager(mActivity)
        rvCJList?.hasFixedSize()
        initCJList()
    }

    private fun initCJList() {
        val adapter = CustomerJourneyAdapter(arrayListOf(), mActivity)
        rvCJList?.adapter = adapter
        NMGLibrary.getInstance().customerJourney!!.observe(this, androidx.lifecycle.Observer {
            customerJourneyEvents ->
                adapter!!.setEvents(customerJourneyEvents!! as ArrayList<CustomerJourneyEvent>
            )
        })
/*
        NMGLibrary.getInstance().getCustomerJourney(CustomerJourneyResult {
            val adapter = CustomerJourneyAdapter(it , mActivity)
            rvCJList?.adapter = adapter
        })

 */


    }

    private fun initToolbar(): Unit {
        mActivity?.iv_menu?.visibility = View.VISIBLE
        mActivity?.iv_back?.visibility = View.GONE
        mActivity?.tv_title?.text = mResources.getString(R.string.customer_journey)
        mActivity?.iv_menu?.setOnClickListener {
            mActivity.openDrawer()
        }
    }
}