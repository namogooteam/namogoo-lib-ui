package com.nmg.apps.deviceinfo.fragments

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.namogoo.android.namogoolib.NMGLibrary
import com.nmg.apps.deviceinfo.R
import com.nmg.apps.deviceinfo.adapters.DependenciesAdapter
import kotlinx.android.synthetic.main.toolbar_ui.*

class DependenciesFragment: BaseFragment() {

    var rvDependenciesList: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
//        return inflater.inflate(R.layout.fragment_blue_tooth, container, false)
        val contextThemeWrapper = ContextThemeWrapper(activity, R.style.DependenciesTheme)
        val localInflater = inflater.cloneInContext(contextThemeWrapper)
        val view = localInflater.inflate(R.layout.fragment_dependencies, container, false)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = activity!!.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(R.color.dark_green_blue)
            window.navigationBarColor = resources.getColor(R.color.dark_green_blue)

        }
        rvDependenciesList = view.findViewById(R.id.rv_dependencies_list)
        return view;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initToolbar()
        rvDependenciesList?.layoutManager = LinearLayoutManager(mActivity)
        rvDependenciesList?.hasFixedSize()
        initDependenciesList()
    }

    private fun initDependenciesList() {
        val list = NMGLibrary.getInstance().deviceInfo.getDependencies();
        val adapter = DependenciesAdapter(list, mActivity)
        rvDependenciesList?.adapter = adapter
    }

    private fun initToolbar(): Unit {
        mActivity?.iv_menu?.visibility = View.VISIBLE
        mActivity?.iv_back?.visibility = View.GONE
        mActivity?.tv_title?.text = mResources.getString(R.string.dependencies)
        mActivity?.iv_menu?.setOnClickListener {
            mActivity.openDrawer()
        }
    }
}