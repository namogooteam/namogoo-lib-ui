package com.nmg.apps.deviceinfo.fragments

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import com.namogoo.android.namogoolib.NMGLibrary
import com.nmg.apps.deviceinfo.R
import java.text.DecimalFormat

class DisplayFragment : BaseFragment(){

    var ivMenu: ImageView? = null
    var ivBack: ImageView? = null
    var tvTitle: TextView? = null

    var tvScreenSize: TextView? = null
    var tvPhysicalSize: TextView? = null
    var tvRefreshRate: TextView? = null
    var tvXDpi: TextView? = null
    var tvYDpi: TextView? = null
    var tvScreenDensityBucket: TextView? = null
    var tvScreenName: TextView? = null
    var tvScreenDPI: TextView? = null
    var tvScreenLogicalDensity: TextView? = null
    var tvScreenScaledDensity: TextView? = null
    var tvScreenUsableWidth: TextView? = null
    var tvScreenUsableHeight: TextView? = null
    var tvScreenTotalWidth: TextView? = null
    var tvScreenTotalHeight: TextView? = null
    var tvIndependentWidth: TextView? = null
    var tvIndependentHeight: TextView? = null
    var tvScreenDefaultOrientation: TextView? = null
    var tvMaxGpuSize: TextView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        val view = inflater.inflate(R.layout.fragment_display, container, false)
        val contextThemeWrapper = ContextThemeWrapper(activity, R.style.DisplayTheme)
        val localInflater = inflater.cloneInContext(contextThemeWrapper)
        val view = localInflater.inflate(R.layout.fragment_display, container, false)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = activity!!.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(R.color.dark_violet_one)
            window.navigationBarColor = resources.getColor(R.color.dark_violet_one)

        }
        ivMenu = view.findViewById(R.id.iv_menu)
        ivBack = view.findViewById(R.id.iv_back)
        tvTitle = view.findViewById(R.id.tv_title)

        tvScreenSize = view.findViewById(R.id.tv_screen_size)
        tvPhysicalSize = view.findViewById(R.id.tv_physical_size)
        tvRefreshRate = view.findViewById(R.id.tv_refresh_rate)
        tvXDpi = view.findViewById(R.id.tv_xdpi)
        tvYDpi = view.findViewById(R.id.tv_ydpi)
        tvScreenName = view.findViewById(R.id.tv_screen_name)
        tvScreenDensityBucket = view.findViewById(R.id.tv_display_bucket)
        tvScreenDPI = view.findViewById(R.id.tv_display_dpi)
        tvScreenLogicalDensity = view.findViewById(R.id.tv_logical_density)
        tvScreenScaledDensity = view.findViewById(R.id.tv_scaled_density)
        tvScreenUsableWidth = view.findViewById(R.id.tv_usable_width)
        tvScreenUsableHeight = view.findViewById(R.id.tv_usable_height)
        tvScreenTotalWidth = view.findViewById(R.id.tv_screen_total_width)
        tvScreenTotalHeight = view.findViewById(R.id.tv_screen_total_height)
        tvIndependentWidth = view.findViewById(R.id.tv_independent_width)
        tvIndependentHeight = view.findViewById(R.id.tv_independent_height)
        tvScreenDefaultOrientation = view.findViewById(R.id.tv_default_orientation)
        tvMaxGpuSize = view.findViewById(R.id.tv_max_gpu)

        return view
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initToolbar()
        getDisplayInfo()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden && isAdded) {
            initToolbar()
        }
    }

    private fun initToolbar() {
        ivMenu?.visibility = View.VISIBLE
        ivBack?.visibility = View.GONE
        tvTitle?.text = mResources.getString(R.string.display)
        ivMenu?.setOnClickListener {
            mActivity.openDrawer()
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getDisplayInfo() {
        //val display = (mActivity.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
        //mActivity.windowManager.defaultDisplay.getMetrics(dm)

        tvScreenSize?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenSize

        //mActivity.windowManager.defaultDisplay.getMetrics(dm)

        tvPhysicalSize?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().physicalSize.toString().plus(mResources.getString(R.string.inches))

        /*** Screen default orientation */
        tvScreenDefaultOrientation?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenDefaultOrientation;

        /*** Display screen width and height */
            tvScreenTotalWidth?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenTotalWidth.toString().plus(mResources.getString(R.string.px))
            tvScreenTotalHeight?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenTotalHeight.toString().plus(mResources.getString(R.string.px))

        /*** Screen refresh rate */
            tvRefreshRate?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().refreshRate.toString().plus(mResources.getString(R.string.fps))

        /*** Display name */
            tvScreenName?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenName

        /*** Max GPU Texture size */

       /* Handler().postDelayed({
            val gpu = GPU(mActivity)
            gpu.loadOpenGLGles10Info { result -> result.toString()
                tvMaxGpuSize?.text = result.GL_MAX_TEXTURE_SIZE.toString().plus(mResources.getString(R.string.x)).plus(result.GL_MAX_TEXTURE_SIZE)
            }
        }, 1000)*/

        /*** Screen Display buckets */
        tvScreenDensityBucket?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenDensityBucket

        /*** Screen Dpi */
        tvScreenDPI?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenDPI.toString().plus(mResources.getString(R.string.dpi))

        /*** Screen logical density */
        tvScreenLogicalDensity?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenLogicalDensity.toString()

        /*** Screen scaled density */
        tvScreenScaledDensity?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenScaledDensity.toString()

        /*** Screen xDpi and yDpi */
        tvXDpi?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().getxDpi().toString().plus(mResources.getString(R.string.dpi))
        tvYDpi?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().getyDpi().toString().plus(mResources.getString(R.string.dpi))


        /*** Screen usable width and height */
        tvScreenUsableWidth?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenUsableWidth.toString().plus(mResources.getString(R.string.px))
        tvScreenUsableHeight?.text = NMGLibrary.getInstance().deviceInfo.getDisplay().screenUsableHeight.toString().plus(mResources.getString(R.string.px))

    }

    private fun returnToDecimalPlaces(values: Double): String {
        val df = DecimalFormat("#.00")
        return df.format(values)
    }
}
