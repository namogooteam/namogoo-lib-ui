package com.nmg.apps.deviceinfo.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.namogoo.android.namogoolib.NMGLibrary;
import com.namogoo.android.namogoolib.helper.Utils;
import com.namogoo.android.namogoolib.pojo.enums.VendorEventType;
import com.nmg.apps.deviceinfo.R;
import com.nmg.apps.deviceinfo.shopping.MainShoppingActivity;
import com.nmg.apps.deviceinfo.utilities.KeyUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {

    Unbinder unbinder;
    int mode;
    @BindView(R.id.iv_menu)
    ImageView ivMenu;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_manufacturer)
    TextView tvManufacturer;
    @BindView(R.id.tv_brand_name)
    TextView tvBrand;
    @BindView(R.id.tv_model_number)
    TextView tvModel;
    @BindView(R.id.tv_board)
    TextView tvBoard;
    @BindView(R.id.tv_hardware)
    TextView tvHardware;
    @BindView(R.id.tv_serial_no)
    TextView tvSerialNo;
    @BindView(R.id.tv_android_id)
    TextView tvAndroidId;
    @BindView(R.id.tv_screen_resolution)
    TextView tvScreenResolution;
    @BindView(R.id.tv_boot_loader)
    TextView tvBootLoader;
    @BindView(R.id.tv_user)
    TextView tvUser;
    @BindView(R.id.tv_host)
    TextView tvHost;
    @BindView(R.id.tv_time_zone)
    TextView tvTimeZone;
    @BindView(R.id.tv_default_language)
    TextView tvDefaultLanguage;
    @BindView(R.id.tv_default_text_orientation)
    TextView tvTextOrientation;
    @BindView(R.id.tv_last_reboot)
    TextView tvLastReboot;
    @BindView(R.id.namogoo_logo)
    ImageView ivNamogooLogo;
    @BindView(R.id.shopping)
    TextView tvShopping;
    @BindView(R.id.wv)
    WebView webView;

    public static HomeFragment getInstance(int mode) {
        HomeFragment homeFragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KeyUtil.KEY_MODE, mode);
        homeFragment.setArguments(bundle);

        return homeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            window.setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark));

        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initToolbar();
        getBundleData();
        getDeviceInfo();

        ivNamogooLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Delete entry")
                        .setMessage("Are you sure you want to delete this entry?")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        tvShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shoppingIntent = new Intent(getContext(), MainShoppingActivity.class);
                startActivity(shoppingIntent);
            }
        });


        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl("https://www.google.com");

        ArrayList<String> arraylist = new ArrayList<>();
        arraylist.add("one");
        arraylist.add("two");
        HashMap<String, Object> payload = new HashMap<>();
        payload.put("aaa","111");
        payload.put("bbb", 222);
        payload.put("ccc", arraylist);
        NMGLibrary.getInstance().reportEvent(VendorEventType.CLICK_ADD_TO_CART, payload);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden && isAdded()) {
            initToolbar();
        }
    }

    private void initToolbar() {
        ivMenu.setVisibility(View.VISIBLE);
        ivBack.setVisibility(View.GONE);
        tvTitle.setText(mResources.getString(R.string.device));
        tvTitle.setTextColor(getActivity().getResources().getColor(android.R.color.black));
        ivMenu.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);

        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.openDrawer();
            }
        });
    }

    private void getDeviceInfo() {

        tvManufacturer.setText("".concat(NMGLibrary.getInstance().getDeviceInfo().getBuild().getManufacturer()));
        tvBrand.setText("".concat(NMGLibrary.getInstance().getDeviceInfo().getBuild().getBrand()));
        tvModel.setText("".concat(NMGLibrary.getInstance().getDeviceInfo().getBuild().getModel()));
        tvBoard.setText("".concat(NMGLibrary.getInstance().getDeviceInfo().getBuild().getBoard()));
        tvHardware.setText("".concat(NMGLibrary.getInstance().getDeviceInfo().getBuild().getHardware()));
        tvSerialNo.setText("".concat(NMGLibrary.getInstance().getDeviceInfo().getBuild().getSerial()));
        tvAndroidId.setText("".concat(NMGLibrary.getInstance().getDeviceInfo().getBuild().getAndroidID()));

        int width = NMGLibrary.getInstance().getDeviceInfo().getDisplay().getScreenTotalWidth();
        int height = NMGLibrary.getInstance().getDeviceInfo().getDisplay().getScreenTotalHeight();

        tvScreenResolution.setText("".concat(width + " * " + height + " " + "Pixels"));
        tvBootLoader.setText(NMGLibrary.getInstance().getDeviceInfo().getBuild().getBootloader());
        tvHost.setText(NMGLibrary.getInstance().getDeviceInfo().getBuild().getHost());
        tvUser.setText(NMGLibrary.getInstance().getDeviceInfo().getBuild().getUser());
        tvTimeZone.setText(NMGLibrary.getInstance().getDeviceInfo().getBuild().getDateTimeLocation());
        tvDefaultLanguage.setText(NMGLibrary.getInstance().getDeviceInfo().getBuild().getDefaultLanguage());
        tvTextOrientation.setText(NMGLibrary.getInstance().getDeviceInfo().getBuild().getTextOrient());
        tvLastReboot.setText(Utils.getDateStringSeconds(NMGLibrary.getInstance().getDeviceInfo().getOS().getLastReboot()));
    }

    /**
     * Get data from bundle
     */
    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(KeyUtil.KEY_MODE)) {
                mode = bundle.getInt(KeyUtil.KEY_MODE);
            }
        }
    }
}
