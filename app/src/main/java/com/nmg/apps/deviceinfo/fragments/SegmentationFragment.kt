package com.nmg.apps.deviceinfo.fragments

import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import com.namogoo.android.namogoolib.NMGLibrary
import com.namogoo.android.namogoolib.pojo.info.SegmentationPojo
import com.nmg.apps.deviceinfo.R
import kotlinx.android.synthetic.main.toolbar_ui.*

class SegmentationFragment: BaseFragment() {

    var segmentsState: LinearLayout? = null
    var emptySegmentsState: LinearLayout? = null
    var cardPrivacy: CardView? = null
    var cardPrice: CardView? = null
    var cardTech: CardView? = null
    var cardNamogoo: CardView? = null
    var cardPrivacyMis: CardView? = null
    var cardPriceMis: CardView? = null
    var cardTechMis: CardView? = null
    var cardNamogooMis: CardView? = null
    var ivPrivacyMis: ImageView? = null
    var ivPriceMis: ImageView? = null
    var ivTechMis: ImageView? = null
    var ivNamogooMis: ImageView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val contextThemeWrapper = ContextThemeWrapper(activity, R.style.SegmentationTheme)
        val localInflater = inflater.cloneInContext(contextThemeWrapper)
        val view = localInflater.inflate(R.layout.fragment_segmentation, container, false)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = activity!!.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(R.color.dark_orange)
            window.navigationBarColor = resources.getColor(R.color.orange)

        }
        segmentsState = view.findViewById(R.id.segments_state)
        emptySegmentsState = view.findViewById(R.id.empty_segments_state)
        cardPrivacy = view.findViewById(R.id.card_privacy)
        cardPrice = view.findViewById(R.id.card_price)
        cardTech = view.findViewById(R.id.card_tech)
        cardNamogoo = view.findViewById(R.id.card_namogoo)

        cardPrivacyMis = view.findViewById(R.id.card_privacy_mis)
        cardPriceMis = view.findViewById(R.id.card_price_mis)
        cardTechMis = view.findViewById(R.id.card_tech_mis)
        cardNamogooMis = view.findViewById(R.id.card_namogoo_mis)

        ivPrivacyMis = view.findViewById(R.id.iv_privacy_mis)
        ivPriceMis = view.findViewById(R.id.iv_price_mis)
        ivTechMis = view.findViewById(R.id.iv_tech_mis)
        ivNamogooMis = view.findViewById(R.id.iv_namogoo_mis)
        return view;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initToolbar()

        showSegments()
    }

    private fun showSegments() {
        val segments = NMGLibrary.getInstance().getSegments()
        //if (segments.size == 0) {
            //emptySegmentsState?.visibility = View.VISIBLE
            //segmentsState?.visibility = View.GONE
            //return
        //}

        emptySegmentsState?.visibility = View.GONE
        segmentsState?.visibility = View.VISIBLE

        if (segments.contains(SegmentationPojo.Segment.PRIVACY_SENSITIVE)) {
            cardPrivacy?.visibility = View.VISIBLE
            cardPrivacyMis?.visibility = View.GONE
        } else {
            cardPrivacy?.visibility = View.GONE
            cardPrivacyMis?.visibility = View.VISIBLE
        }

        if (segments.contains(SegmentationPojo.Segment.PRICE_SENSITIVE)) {
            cardPrice?.visibility = View.VISIBLE
            cardPriceMis?.visibility = View.GONE
        } else {
            cardPrice?.visibility = View.GONE
            cardPriceMis?.visibility = View.VISIBLE
        }

        if (segments.contains(SegmentationPojo.Segment.TECH_SAVVY)) {
            cardTech?.visibility = View.VISIBLE
            cardTechMis?.visibility = View.GONE
        } else {
            cardTech?.visibility = View.GONE
            cardTechMis?.visibility = View.VISIBLE
        }

        if (segments.contains(SegmentationPojo.Segment.NAMOGOO_GROUPIE)) {
            cardNamogoo?.visibility = View.VISIBLE
            cardNamogooMis?.visibility = View.GONE
        } else {
            cardNamogoo?.visibility = View.GONE
            cardNamogooMis?.visibility = View.VISIBLE
        }

        val matrix = ColorMatrix()
        matrix.setSaturation(0f) //0 means grayscale
        val cf = ColorMatrixColorFilter(matrix)
        ivPriceMis?.colorFilter = cf
        ivPrivacyMis?.colorFilter = cf
        ivTechMis?.colorFilter = cf
        ivNamogooMis?.colorFilter = cf

    }


    private fun initToolbar(): Unit {
        mActivity?.iv_menu?.visibility = View.VISIBLE
        mActivity?.iv_back?.visibility = View.GONE
        mActivity?.tv_title?.text = mResources.getString(R.string.segmentation)
        mActivity?.iv_menu?.setOnClickListener {
            mActivity.openDrawer()
        }
    }
}