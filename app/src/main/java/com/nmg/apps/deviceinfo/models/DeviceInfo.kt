package com.nmg.apps.deviceinfo.models

import android.graphics.drawable.Drawable

/**
 * Created by Udi
 */

class DeviceInfo(var flags: Int, var appLogo: Drawable, var appLable: String, var packageName: String)
