package com.nmg.apps.deviceinfo.shopping

data class CartItem(var product: Product, var quantity: Int = 0)