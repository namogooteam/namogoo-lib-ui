package com.nmg.apps.deviceinfo.shopping

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.nmg.apps.deviceinfo.R
import io.paperdb.Paper
import kotlinx.android.synthetic.main.activity_shopping_main.*

import retrofit2.Call
import retrofit2.Response
import java.util.*

class MainShoppingActivity : AppCompatActivity() {

    private val ITEMS_IN_STORE = 10;
    private lateinit var apiService: APIService
    private lateinit var productAdapter: ProductAdapter

    private var products = mutableListOf<Product>()
//    val APIConfig = APIConfig.

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Paper.init(this)

        setContentView(R.layout.activity_shopping_main)

        while (ShoppingCart.getCart().size > 0)
            ShoppingCart.removeItem(ShoppingCart.getCart()[0], this)

        setSupportActionBar(toolbar)
        apiService = APIConfig.getRetrofitClient(this).create(APIService::class.java)


        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorPrimary))

        swipeRefreshLayout.isRefreshing = true


//        val layoutManager = StaggeredGridLayoutManager(this, Lin)

        products_recyclerview.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)


        cart_size.text = ShoppingCart.getShoppingCartSize().toString()

        getProducts()


        basketButton.setOnClickListener {

            startActivity(Intent(this, ShoppingCartActivity::class.java))
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.shopping_menu, menu)
        return true
    }


    fun getProducts() {

        apiService.getProducts().enqueue(object : retrofit2.Callback<Meals> {
            override fun onFailure(call: Call<Meals>, t: Throwable) {

                print(t.message)
                Log.d("Data error", t.message)
                Toast.makeText(this@MainShoppingActivity, t.message, Toast.LENGTH_SHORT).show()

            }

            override fun onResponse(call: Call<Meals>, response: Response<Meals>) {

                swipeRefreshLayout.isRefreshing = false
//                swipeRefreshLayout.isEnabled = false

                val meals = response.body()!!
                val p = meals.meals[0]
                val rand = Random(System.currentTimeMillis())
                p.price = (rand.nextInt(90) + 10).toString()
                products.add(p)
                if (ITEMS_IN_STORE > products.size)
                {
                    getProducts()
                    return
                }
                productAdapter = ProductAdapter(this@MainShoppingActivity, products)

                products_recyclerview.adapter = productAdapter

//                productAdapter.notifyDataSetChanged()

            }

        })
    }

}
