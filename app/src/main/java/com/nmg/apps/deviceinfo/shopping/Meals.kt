package com.nmg.apps.deviceinfo.shopping

import com.google.gson.annotations.SerializedName

class Meals {
    @SerializedName("meals")
    var meals: List<Product> = arrayListOf()
}