package com.nmg.apps.deviceinfo.shopping

import com.google.gson.annotations.SerializedName

data class Photo(
    @SerializedName("filename")
    var filename: String? = null
)