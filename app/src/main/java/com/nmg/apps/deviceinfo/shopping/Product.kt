package com.nmg.apps.deviceinfo.shopping

//import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("strInstructions")
    var description: String? = null,

    @SerializedName("idMeal")
    var id: Int? = null,

    @SerializedName("strMeal")
    var name: String? = null,

    @SerializedName("price")
    var price: String? = null,

    @SerializedName("strMealThumb")
    var photo: String? = null
)