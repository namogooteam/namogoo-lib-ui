package com.namogoo.android.namogoolib;

import androidx.test.platform.app.InstrumentationRegistry ;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import android.content.Context;


import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class NMGLibraryInstrumentedTest {
    private static Context appContext;
    private static NMGLibrary nmgLibrary;
    private static String vIDExpected;
    private static String expectedSession;
    private static String expectedVendorCustomerId;

    @BeforeClass
    public static void setup() {
        appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        vIDExpected = "VID_" + UUID.randomUUID().toString();
        expectedSession = UUID.randomUUID().toString();
        expectedVendorCustomerId = UUID.randomUUID().toString();
        nmgLibrary = NMGLibrary.newInstance(appContext, vIDExpected, expectedSession, expectedVendorCustomerId);
    }
    @Test
    public void useAppContext() throws Exception {
        assertEquals("com.namogoo.android.namogoolib.test", appContext.getPackageName());
    }

    @Test
    public void GetVendorId_EqualToInput_True() throws Exception {
        String vIdActual = nmgLibrary.getVendorId();
        assertEquals(vIDExpected, vIdActual);
    }

    @Test
    public void GetVendorSessionId_EqualToInput_True() throws Exception {
        String actualSession = NMGLibrary.getInstance().getVendorSessionId();
        assertEquals(expectedSession, actualSession);
    }

    @Test
    public void GetVendorCustomerId_EqualToInput_True() throws Exception {
        String actualVendorCustomerId = nmgLibrary.getVendorCustomerId();
        assertEquals(expectedVendorCustomerId, actualVendorCustomerId);
    }

}
