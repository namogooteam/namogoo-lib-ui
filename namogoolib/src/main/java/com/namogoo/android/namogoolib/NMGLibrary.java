package com.namogoo.android.namogoolib;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import androidx.databinding.Observable;
import androidx.lifecycle.LiveData;

import com.namogoo.android.namogoolib.db.CustomerJourneyEventScribe;
import com.namogoo.android.namogoolib.db.dao.DeviceDataScribe;
import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent;
import com.namogoo.android.namogoolib.handler.ActivityLifecycleCallbacksHandler;
import com.namogoo.android.namogoolib.handler.UserInteractionHandler;
import com.namogoo.android.namogoolib.helper.Config;
import com.namogoo.android.namogoolib.info.DeviceInfoManager;
import com.namogoo.android.namogoolib.info.SegmentationLoader;
import com.namogoo.android.namogoolib.pojo.enums.ServiceRequest;
import com.namogoo.android.namogoolib.pojo.enums.VendorEventType;
import com.namogoo.android.namogoolib.pojo.info.SegmentationPojo;
import com.namogoo.android.namogoolib.reporter.DataDispatcher;
import com.namogoo.android.namogoolib.service.CustomerJourneyService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static com.namogoo.android.namogoolib.service.CustomerJourneyService.INIT_JOURNEY_REQUEST;
import static com.namogoo.android.namogoolib.service.CustomerJourneyService.JOURNEY_CONFIG_REQUEST;
import static com.namogoo.android.namogoolib.service.CustomerJourneyService.SEND_JOURNEY_DATA;

public class NMGLibrary {
    private static final String TAG = "NMG-Library";
    private static NMGLibrary nmgLibrary;

    private Context mContext;

    private DeviceInfoManager infoLoaderManager;
    private DataDispatcher dataDispatcher;
    private CustomerJourneyEventScribe customerJourneyEventScribe;
    private DeviceDataScribe deviceDataScribe;

    private SegmentationLoader segmentationLoader;
    private String mSessionId;
    private String vendorId;
    private String vendorSessionId;
    private String vendorCustomerId;
    private boolean isFisrtSession;


    private LiveData<List<CustomerJourneyEvent>> sessionEvents;

    //******
    //   Public static
    //******
    public static NMGLibrary newInstance(Context applicationContext, String vendorId)
    {
        return NMGLibrary.newInstance(applicationContext, vendorId, null, null);

    }

    public static NMGLibrary newInstance(Context applicationContext, String vendorId, String vendorSessionId, String vendorCustomerId)
    {
        if (nmgLibrary == null)
        {
            synchronized (NMGLibrary.class) {
                if (nmgLibrary == null) {
                    nmgLibrary = new NMGLibrary(applicationContext, vendorId, vendorSessionId, vendorCustomerId);
                }
            }
        }
        return nmgLibrary;
    }

    public static NMGLibrary getInstance()
    {
        return nmgLibrary;
    }

    //******
    //   Constructors
    //******
    NMGLibrary(Context applicationContext, String vendorId, String vendorSessionId, String vendorCustomerId)
    {
        this.vendorId = vendorId;
        this.vendorSessionId = vendorSessionId;
        this.vendorCustomerId = vendorCustomerId;
        sessionEvents = null;
        this.mContext = applicationContext.getApplicationContext() == null ? applicationContext : applicationContext.getApplicationContext();
        deviceDataScribe = new DeviceDataScribe(getContext());
        infoLoaderManager = new DeviceInfoManager(getContext());
        initJourneyConfig();
        //only for main process
        if (!Application.getProcessName().endsWith("nmgCJ")) {
            initRegisterActivityListener();
            callCJService(ServiceRequest.CONFIG_REQUEST);
            callCJService(ServiceRequest.INIT_JOURNEY_SESSION);
        }
    }

    //******
    //   Public
    //******
    public String getVersion()
    {
        return BuildConfig.VERSION_NAME;
    }

    public DeviceInfoManager getDeviceInfo()
    {
        return infoLoaderManager;
    }

    public void reportEvent(VendorEventType eventType, HashMap<String, Object> payload) {
        customerJourneyEventScribe.reportCustomEvent(eventType, payload, getSessionId());
    }

    public LiveData<List<CustomerJourneyEvent>> getCustomerJourney()
    {
        sessionEvents = customerJourneyEventScribe.getLiveDataAllSessionEvents(mSessionId);
        return sessionEvents;
    }


    public SegmentationLoader getSegmentationLoader()
    {
        if (segmentationLoader == null)
        {
            segmentationLoader = new SegmentationLoader();
        }
        return segmentationLoader;
    }

    public ArrayList<SegmentationPojo.Segment> getSegments()
    {
        //getFireBaseDB().reportSegments(getSegmentationLoader().getSegmentation(true).getSegments());
        //return getSegmentationLoader().getSegmentation(false).getSegments();
        return new ArrayList<>();
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorSessionAndCustomerId(String vendorSessionId, String vendorCustomerId) {
        this.vendorSessionId = vendorSessionId;
        this.vendorCustomerId = vendorCustomerId;
    }

    public String getVendorCustomerId() {
        return vendorCustomerId;
    }

    public String getVendorSessionId() {
        return vendorSessionId;
    }

    //******
    //   Private
    //******
    private void initRegisterActivityListener() {
        if (Config.getCustomerJourneyConfig().getEventTypesConfig().getEnableLifecycle() == false)
        {
            return;
        }
        Application application = (Application) mContext;
        ActivityLifecycleCallbacksHandler activityLifecycleCallbacksHandler = new ActivityLifecycleCallbacksHandler(getSessionId()) {
        };
        application.registerActivityLifecycleCallbacks(activityLifecycleCallbacksHandler);
        customerJourneyEventScribe = new CustomerJourneyEventScribe(getContext());

        activityLifecycleCallbacksHandler.addOnChangedCallback(customerJourneyEventScribe.getLifeCycleCallback());

        UserInteractionHandler userInteractionHandler = new UserInteractionHandler(getSessionId(), deviceDataScribe, getVendorId());
        activityLifecycleCallbacksHandler.addOnChangedCallback(userInteractionHandler.getLifeCycleCallback());
        userInteractionHandler.addOnChangedCallback(customerJourneyEventScribe.getUserInteractionCallback());

        dataDispatcher = new DataDispatcher(getContext(), deviceDataScribe);
        customerJourneyEventScribe.addObservableReportCustomerJourneyEventCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                callCJService(ServiceRequest.SEND_JOURNEY_DATA);
            }
        });


    }



    private Context getContext()
    {
        return mContext;
    }

    private synchronized String getSessionId()
    {
        if (mSessionId == null)
        {
            mSessionId = UUID.randomUUID().toString();
        }
        return mSessionId;
    }

    private void callCJService(ServiceRequest serviceRequest) {
        Intent serviceIntent = new Intent(getContext(), CustomerJourneyService.class);
        switch (serviceRequest) {
            case CONFIG_REQUEST:
                serviceIntent.putExtra(JOURNEY_CONFIG_REQUEST, true);
                break;
            case SEND_JOURNEY_DATA:
                serviceIntent.putExtra(SEND_JOURNEY_DATA, true);
                break;
            case INIT_JOURNEY_SESSION:
                serviceIntent.putExtra(INIT_JOURNEY_REQUEST, getSessionId());
                break;
        }
        try {
            getContext().startService(serviceIntent);
        } catch (Exception e) {
            e.printStackTrace();
            //might be in background
        }
    }

    private void initJourneyConfig() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                deviceDataScribe.initJourneyConfig();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }
}
