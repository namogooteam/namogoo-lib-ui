package com.namogoo.android.namogoolib.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.namogoo.android.namogoolib.db.dao.CustomerJourneyEventsDao;
import com.namogoo.android.namogoolib.db.dao.DeviceDao;
import com.namogoo.android.namogoolib.db.dao.JourneyConfigDao;
import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent;
import com.namogoo.android.namogoolib.db.entities.Device;
import com.namogoo.android.namogoolib.db.entities.JourneyConfig;

@Database(entities = {Device.class, CustomerJourneyEvent.class, JourneyConfig.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract DeviceDao deviceDao();
    public abstract CustomerJourneyEventsDao customerJourneyEventsDao();
    public abstract JourneyConfigDao journeyConfigDao();
}