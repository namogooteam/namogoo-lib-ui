package com.namogoo.android.namogoolib.db;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.namogoo.android.namogoolib.pojo.events.LifeCycleEventPojo;
import com.namogoo.android.namogoolib.pojo.events.UserInteractionEventPojo;
import com.namogoo.android.namogoolib.pojo.server.CustomerJourneyConfigResponsePojo;

import java.lang.reflect.Type;

public class Converters {
    private static Gson gson = new Gson();
    private static Type lifecycleType = new TypeToken<LifeCycleEventPojo>() {
    }.getType();
    private static Type userInteractionType = new TypeToken<UserInteractionEventPojo>() {
    }.getType();
    private static Type customerJourneyConfigResponseType = new TypeToken<CustomerJourneyConfigResponsePojo>() {
    }.getType();

    @TypeConverter
    public static String lifeCycleEventPojoToString(LifeCycleEventPojo lifeCycleEventPojo) {
        return gson.toJson(lifeCycleEventPojo, lifecycleType);
    }

    @TypeConverter
    public static LifeCycleEventPojo stringToLifeCycleEventPojo(String json) {
        return gson.fromJson(json, lifecycleType);
    }

    @TypeConverter
    public static String userInteractionEventPojoToString(UserInteractionEventPojo userInteractionEventPojo) {
        return gson.toJson(userInteractionEventPojo, userInteractionType);
    }

    @TypeConverter
    public static UserInteractionEventPojo stringToUserInteractionEventPojo(String json) {
        return gson.fromJson(json, userInteractionType);
    }

    @TypeConverter
    public static String customerJourneyConfigResponsePojoToString(CustomerJourneyConfigResponsePojo customerJourneyConfigResponsePojo) {
        return gson.toJson(customerJourneyConfigResponsePojo, customerJourneyConfigResponseType);
    }

    @TypeConverter
    public static CustomerJourneyConfigResponsePojo stringToCustomerJourneyConfigResponsePojo(String json) {
        return gson.fromJson(json, customerJourneyConfigResponseType);
    }
}
