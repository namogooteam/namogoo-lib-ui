package com.namogoo.android.namogoolib.db;

import android.content.Context;
import android.os.AsyncTask;


import androidx.databinding.Observable;
import androidx.lifecycle.LiveData;

import com.namogoo.android.namogoolib.NMGLibrary;
import com.namogoo.android.namogoolib.db.dao.DeviceDataScribe;
import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent;
import com.namogoo.android.namogoolib.helper.Utils;
import com.namogoo.android.namogoolib.pojo.enums.EventType;
import com.namogoo.android.namogoolib.pojo.enums.VendorEventType;
import com.namogoo.android.namogoolib.pojo.events.LifeCycleEventPojo;
import com.namogoo.android.namogoolib.pojo.events.UserInteractionEventPojo;
import com.namogoo.android.namogoolib.pojo.observables.ObservableLifeCycleEvents;
import com.namogoo.android.namogoolib.pojo.observables.ObservableReportCustomerJourneyEvent;
import com.namogoo.android.namogoolib.pojo.observables.ObservableUserInteractionsEvents;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CustomerJourneyEventScribe {

    private AppDatabase appDB;
    private String userId;
    private ObservableReportCustomerJourneyEvent observableReportCustomerJourneyEvent;
    private CustomerJourneyEvent lastReportedCustomerJourneyEvent;
    private int currentPageView;

    private static final String KEY_RULE_NAME = "ruleName";
    private static final String KEY_VENDOR_EVENT = "vendorEvent";

    private final Object uiEventSyncObj = new Object();

    public CustomerJourneyEventScribe(Context context) {
        appDB = DeviceDataScribe.getAppDB();
        observableReportCustomerJourneyEvent = new ObservableReportCustomerJourneyEvent();
    }

    public Observable.OnPropertyChangedCallback getLifeCycleCallback() {
        return lifeCycleChanged;
    }

    public Observable.OnPropertyChangedCallback getUserInteractionCallback() {
        return userInteractionCallback;
    }

    public Observable.OnPropertyChangedCallback getReportedCustomerJourneyCallback() {
        return reportedCustomerJourneyCallback;
    }

    private Observable.OnPropertyChangedCallback lifeCycleChanged = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(final Observable sender, int propertyId) {
            ObservableLifeCycleEvents observableLifeCycleEvents = (ObservableLifeCycleEvents) sender;
            CustomerJourneyEvent customerJourneyEvent = new CustomerJourneyEvent();
            customerJourneyEvent.id = UUID.randomUUID().toString();
            customerJourneyEvent.eventTime = observableLifeCycleEvents.getLifeCycleEventPojo().getEventTime();
            customerJourneyEvent.lifecycleEventData = observableLifeCycleEvents.getLifeCycleEventPojo().mutate();
            customerJourneyEvent.sessionId = observableLifeCycleEvents.getLifeCycleEventPojo().getSessionId();
            customerJourneyEvent.vendorSessionId = NMGLibrary.getInstance().getVendorSessionId();
            customerJourneyEvent.vendorCustomerId = NMGLibrary.getInstance().getVendorCustomerId();

            if (currentPageView < observableLifeCycleEvents.getLifeCycleEventPojo().getPageView()) {
                currentPageView  = observableLifeCycleEvents.getLifeCycleEventPojo().getPageView();
            }
            new AsyncTask<CustomerJourneyEvent, Void, Void>() {
                @Override
                protected Void doInBackground(CustomerJourneyEvent... customerJourneyEvents) {
                    appDB.customerJourneyEventsDao().insertAll(customerJourneyEvents[0]);
                    addJourneyEventForDispatch(customerJourneyEvents[0]);
                    return null;
                }
            }.execute(customerJourneyEvent);
        }
    };

    private Observable.OnPropertyChangedCallback userInteractionCallback = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(final Observable sender, int propertyId) {
            synchronized (uiEventSyncObj) {
                ObservableUserInteractionsEvents observableUserInteractionsEvents = (ObservableUserInteractionsEvents) sender;
                CustomerJourneyEvent customerJourneyEvent = new CustomerJourneyEvent();
                customerJourneyEvent.id = UUID.randomUUID().toString();
                customerJourneyEvent.eventTime = observableUserInteractionsEvents.getUserInteractionEventPojo().getEventTime();
                customerJourneyEvent.userInteractionEventData = observableUserInteractionsEvents.getUserInteractionEventPojo().mutate();
                customerJourneyEvent.sessionId = observableUserInteractionsEvents.getUserInteractionEventPojo().getSessionId();
                customerJourneyEvent.vendorSessionId = NMGLibrary.getInstance().getVendorSessionId();
                customerJourneyEvent.vendorCustomerId = NMGLibrary.getInstance().getVendorCustomerId();

                if (shouldReportCustomerJourneyEvent(customerJourneyEvent) == false) {
                    return;
                }
                lastReportedCustomerJourneyEvent = customerJourneyEvent;
            }
            new AsyncTask<CustomerJourneyEvent, Void, Void>() {
                @Override
                protected Void doInBackground(CustomerJourneyEvent... customerJourneyEvents) {
                    appDB.customerJourneyEventsDao().insertAll(customerJourneyEvents[0]);
                    addJourneyEventForDispatch(customerJourneyEvents[0]);
                    return null;
                }
            }.execute(lastReportedCustomerJourneyEvent);

        }
    };

    private Observable.OnPropertyChangedCallback reportedCustomerJourneyCallback = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(final Observable sender, int propertyId) {
            //appDB.customerJourneyEventsDao().deleteByEventId(((ObservableReportCustomerJourneyEvent)sender).getCustomerJourneyEvent().id);
            appDB.customerJourneyEventsDao().updateWasSent(((ObservableReportCustomerJourneyEvent)sender).getCustomerJourneyEvent().id);
        }
    };

    private void addJourneyEventForDispatch(CustomerJourneyEvent customerJourneyEvent) {
        observableReportCustomerJourneyEvent.setCustomerJourneyEvent(customerJourneyEvent);
    }

    public void addObservableReportCustomerJourneyEventCallback(Observable.OnPropertyChangedCallback customerJourneyEventCallback) {
        observableReportCustomerJourneyEvent.addOnPropertyChangedCallback(customerJourneyEventCallback);
    }

    private boolean shouldReportCustomerJourneyEvent(CustomerJourneyEvent customerJourneyEvent) {
        if (lastReportedCustomerJourneyEvent != null)
        {
            UserInteractionEventPojo lastUserInteractionEventData = lastReportedCustomerJourneyEvent.userInteractionEventData;
            if (lastUserInteractionEventData != null && customerJourneyEvent.userInteractionEventData != null) {
                if (lastUserInteractionEventData.getEventType().equals(customerJourneyEvent.userInteractionEventData.getEventType()) &&
                        lastUserInteractionEventData.getTargetId().equals(customerJourneyEvent.userInteractionEventData.getTargetId()) &&
                        customerJourneyEvent.userInteractionEventData.getEventTime() - lastUserInteractionEventData.getEventTime() < 50) {
                    return false;
                }
            }
        }
        return true;
    }

    public LiveData<List<CustomerJourneyEvent>> getLiveDataAllSessionEvents(String mSessionId) {
        return appDB.customerJourneyEventsDao().getLiveDataAllSessionEvents(mSessionId);
    }

    public void reportCustomEvent(VendorEventType eventType, Map<String, Object> dataMap, String sessionId) {
        CustomerJourneyEvent customerJourneyEvent = new CustomerJourneyEvent();
        customerJourneyEvent.id = UUID.randomUUID().toString();
        customerJourneyEvent.eventTime = System.currentTimeMillis();
        HashMap<String, Object> localDataMap = new HashMap<>();
        if (dataMap != null) {
            localDataMap.putAll(dataMap);
        }
        localDataMap.put(KEY_VENDOR_EVENT, "true");
        String targetId = localDataMap.containsKey("targetId") ? localDataMap.get("targetId").toString() : KEY_VENDOR_EVENT;


        String value = Utils.mapObjToJsonString(localDataMap);
        UserInteractionEventPojo userInteractionEventData = new UserInteractionEventPojo(EventType.VENDOR_EVENT, targetId, sessionId, currentPageView, eventType.name(), null, value);
        customerJourneyEvent.userInteractionEventData = userInteractionEventData;

        customerJourneyEvent.sessionId = sessionId;
        customerJourneyEvent.vendorSessionId = NMGLibrary.getInstance().getVendorSessionId();
        customerJourneyEvent.vendorCustomerId = NMGLibrary.getInstance().getVendorCustomerId();

        new AsyncTask<CustomerJourneyEvent, Void, Void>() {
            @Override
            protected Void doInBackground(CustomerJourneyEvent... customerJourneyEvents) {
                appDB.customerJourneyEventsDao().insertAll(customerJourneyEvents[0]);
                addJourneyEventForDispatch(customerJourneyEvents[0]);
                return null;
            }
        }.execute(customerJourneyEvent);
    }

}
