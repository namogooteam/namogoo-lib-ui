package com.namogoo.android.namogoolib.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface CustomerJourneyEventsDao {
    @Query("SELECT * FROM customerJourneyEvent")
    List<CustomerJourneyEvent> getAll();

    @Query("SELECT * FROM customerJourneyEvent WHERE id IN (:eventIds)")
    List<CustomerJourneyEvent> loadAllByIds(String[] eventIds);

    @Insert
    void insertAll(CustomerJourneyEvent... customerJourneyEvents);

    @Delete
    void delete(CustomerJourneyEvent customerJourneyEvent);

    @Update
    void update(CustomerJourneyEvent customerJourneyEvent);

    @Query("DELETE FROM customerJourneyEvent WHERE id LIKE :eventId")
    void deleteByEventId(String eventId);

    @Query("UPDATE customerJourneyEvent SET wasSent = 1 WHERE id LIKE :eventId")
    void updateWasSent(String eventId); //change 'was sent' from 0 to 1

    @Query("SELECT * from customerJourneyEvent WHERE wasSent = 0 or (wasSent is null) LIMIT 100")
    List<CustomerJourneyEvent> getAllNotSent();

    @Query("SELECT * FROM customerJourneyEvent WHERE sessionId LIKE :sessionID")
    List<CustomerJourneyEvent> getAllSessionEvents(String sessionID);

    @Query("SELECT * FROM customerJourneyEvent WHERE sessionId LIKE :sessionID")
    LiveData<List<CustomerJourneyEvent>> getLiveDataAllSessionEvents(String sessionID);

    @Query("DELETE FROM customerJourneyEvent WHERE eventTime <= :cutOffDateMilli")
    void deleteOldEvents(long cutOffDateMilli);
}
