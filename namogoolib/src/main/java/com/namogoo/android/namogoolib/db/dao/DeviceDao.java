package com.namogoo.android.namogoolib.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.namogoo.android.namogoolib.db.entities.Device;

import java.util.List;

@Dao
public interface  DeviceDao {
    @Query("SELECT * FROM device")
    List<Device> getAll();

    @Query("SELECT * FROM device WHERE deviceId IN (:deviceIds)")
    List<Device> loadAllByIds(int[] deviceIds);

    @Insert
    void insertAll(Device... devices);

    @Delete
    void delete(Device device);
}
