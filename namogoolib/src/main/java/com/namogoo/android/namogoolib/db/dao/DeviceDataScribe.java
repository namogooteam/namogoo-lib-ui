package com.namogoo.android.namogoolib.db.dao;

import android.content.Context;
import android.os.AsyncTask;

import androidx.room.Room;

import com.namogoo.android.namogoolib.db.AppDatabase;
import com.namogoo.android.namogoolib.db.entities.Device;
import com.namogoo.android.namogoolib.db.entities.JourneyConfig;
import com.namogoo.android.namogoolib.helper.Config;

import java.util.List;
import java.util.UUID;

import static com.namogoo.android.namogoolib.helper.KeyUtil.DATABASE_NAME;

public class DeviceDataScribe {

    private static AppDatabase appDB;
    private String deviceID;

    public DeviceDataScribe(Context context) {
        if (appDB == null)
        {
            appDB = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME).build();
            initDeviceUUID();
        }
    }

    private void initDeviceUUID() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                getDeviceUUID();
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static AppDatabase getAppDB()
    {
        return appDB;
    }

    public synchronized String getDeviceUUID()
    {
        if (deviceID != null)
        {
            return deviceID;
        }
        if (appDB.deviceDao().getAll().size() == 0) {
            deviceID = UUID.randomUUID().toString();
            Device device = new Device();
            device.deviceId = deviceID;
            appDB.deviceDao().insertAll(device);
        } else
        {
            deviceID = appDB.deviceDao().getAll().get(0).deviceId;
        }

        return deviceID;
    }


    public void initJourneyConfig() {
        List<JourneyConfig> journeyConfigs = appDB.journeyConfigDao().getAll();;
        if (journeyConfigs == null || journeyConfigs.size() == 0) {
            Config.setCustomerJourneyConfig(null);
        } else {
            Config.setCustomerJourneyConfig(journeyConfigs.get(0).config);
        }
    }
}
