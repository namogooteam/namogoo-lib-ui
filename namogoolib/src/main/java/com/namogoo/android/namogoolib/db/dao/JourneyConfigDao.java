package com.namogoo.android.namogoolib.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.namogoo.android.namogoolib.db.entities.Device;
import com.namogoo.android.namogoolib.db.entities.JourneyConfig;

import java.util.List;

@Dao
public interface JourneyConfigDao {
    @Query("SELECT * FROM journey_config")
    List<JourneyConfig> getAll();

    @Insert
    void insertAll(JourneyConfig... journeyConfigs);

    @Query("DELETE FROM journey_config")
    public void deleteAll();

    @Delete
    void delete(JourneyConfig journeyConfig);
}
