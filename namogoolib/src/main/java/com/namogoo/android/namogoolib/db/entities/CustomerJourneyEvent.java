package com.namogoo.android.namogoolib.db.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.namogoo.android.namogoolib.pojo.events.LifeCycleEventPojo;
import com.namogoo.android.namogoolib.pojo.events.UserInteractionEventPojo;

@Entity (tableName = "customerJourneyEvent")
public class CustomerJourneyEvent {
    @PrimaryKey
    @NonNull
    public String id;

    @ColumnInfo(name = "eventTime")
    public Long eventTime;

    @ColumnInfo(name = "lifecycleEventData")
    public LifeCycleEventPojo lifecycleEventData;

    @ColumnInfo(name = "userInteractionEventData")
    public UserInteractionEventPojo userInteractionEventData;

    @ColumnInfo(name = "sessionId")
    public String sessionId;

    @ColumnInfo(name = "wasSent", defaultValue = "0")
    public Boolean wasSent;

    @ColumnInfo(name = "vendorSessionId")
    public String vendorSessionId;

    @ColumnInfo(name = "vendorCustomerId")
    public String vendorCustomerId;
}
