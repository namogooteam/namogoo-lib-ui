package com.namogoo.android.namogoolib.db.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.namogoo.android.namogoolib.pojo.server.CustomerJourneyConfigResponsePojo;

@Entity (tableName = "journey_config")
public class JourneyConfig {
    @PrimaryKey
    @NonNull
    public String id;

    @ColumnInfo(name = "config")
    public CustomerJourneyConfigResponsePojo config;
}
