package com.namogoo.android.namogoolib.handler;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Observable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.namogoo.android.namogoolib.pojo.events.LifeCycleEventPojo;
import com.namogoo.android.namogoolib.pojo.enums.EventType;
import com.namogoo.android.namogoolib.pojo.observables.ObservableLifeCycleEvents;

import java.util.HashMap;

import static android.provider.Browser.EXTRA_APPLICATION_ID;

public class ActivityLifecycleCallbacksHandler implements Application.ActivityLifecycleCallbacks {

    private final int PAGE_VIEW_JUMPS = 100;

    private int pageView_Created = 0;
    private int pageView_Started = 0;
    private int pageView_Resumed = 0;
    private int pageView_Paused = 0;
    private int pageView_Stopped = 0;
    private int pageView_Destroyed = 0;
    private String sessionId;
    private ObservableLifeCycleEvents observableLifeCycleEvents;
    private LifecycleObserver lifecycleListener;
    protected ActivityLifecycleCallbacksHandler(String sessionId) {
        this.observableLifeCycleEvents = new ObservableLifeCycleEvents();
        this.sessionId = sessionId;

        ProcessLifecycleOwner.get().getLifecycle().addObserver(getLifecycleObserver());
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
        pageView_Created += PAGE_VIEW_JUMPS;
        HashMap<String, String> intentData = new HashMap<>();
        if (activity.getIntent() != null) {
            Intent i = activity.getIntent();
            if (i.getData() != null) {
                //Log.d(TAG, "onActivityCreated, data: " + i.getData().toString());
                intentData.put("data", i.getData().toString());
            }
            if (i.getExtras() != null)
            {
                for (String key : i.getExtras().keySet())
                {
                    try {
                        intentData.put(key, i.getExtras().get(key).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (i.getExtras().containsKey(EXTRA_APPLICATION_ID))
                {
                    if (i.getData() != null) {
                        HashMap<String, String> referrerData = new HashMap<>();
                        referrerData.put("data", i.getData().toString());
                        referrerData.put(EXTRA_APPLICATION_ID, i.getExtras().getString(EXTRA_APPLICATION_ID));
                        observableLifeCycleEvents.setLifeCycleEventPojo(new LifeCycleEventPojo(EventType.APP_REFERRER, activity.getLocalClassName(), sessionId, pageView_Created, null, referrerData));
                    }
                }
            }
        }

        LifeCycleEventPojo lifeCycleEventPojo = new LifeCycleEventPojo(EventType.COMPONENT_INIT, activity.getLocalClassName(), sessionId, pageView_Created, null, intentData);
        observableLifeCycleEvents.setLastKnownActivity(activity);
        observableLifeCycleEvents.setLifeCycleEventPojo(lifeCycleEventPojo);
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        pageView_Started += PAGE_VIEW_JUMPS;
        LifeCycleEventPojo lifeCycleEventPojo = new LifeCycleEventPojo(EventType.COMPONENT_ADDED_VIS_HIERARCHY, activity.getLocalClassName(), sessionId, pageView_Started, null);
        observableLifeCycleEvents.setLifeCycleEventPojo(lifeCycleEventPojo);
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        pageView_Resumed += PAGE_VIEW_JUMPS;
        LifeCycleEventPojo lifeCycleEventPojo = new LifeCycleEventPojo(EventType.COMPONENT_FOREGROUND, activity.getLocalClassName(), sessionId, pageView_Resumed, null);
        observableLifeCycleEvents.setLifeCycleEventPojo(lifeCycleEventPojo);
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        pageView_Paused+= PAGE_VIEW_JUMPS;
        LifeCycleEventPojo lifeCycleEventPojo = new LifeCycleEventPojo(EventType.COMPONENT_BACKGROUND, activity.getLocalClassName(), sessionId, pageView_Paused, null);
        observableLifeCycleEvents.setLifeCycleEventPojo(lifeCycleEventPojo);
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
        pageView_Stopped += PAGE_VIEW_JUMPS;
        LifeCycleEventPojo lifeCycleEventPojo = new LifeCycleEventPojo(EventType.COMPONENT_REMOVED_VIS_HIERARCHY, activity.getLocalClassName(), sessionId, pageView_Stopped, null);
        observableLifeCycleEvents.setLifeCycleEventPojo(lifeCycleEventPojo);
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        pageView_Destroyed += PAGE_VIEW_JUMPS;
        LifeCycleEventPojo lifeCycleEventPojo = new LifeCycleEventPojo(EventType.COMPONENT_DESTROYED, activity.getLocalClassName(), sessionId, pageView_Destroyed, null);
        observableLifeCycleEvents.setLifeCycleEventPojo(lifeCycleEventPojo);
    }

    public void addOnChangedCallback(Observable.OnPropertyChangedCallback callback)
    {
        observableLifeCycleEvents.addOnPropertyChangedCallback(callback);
    }

    private LifecycleObserver getLifecycleObserver() {
        if (lifecycleListener == null) {
            lifecycleListener = new LifecycleObserver() {
                @OnLifecycleEvent(Lifecycle.Event.ON_START)
                private void onMoveToForeground() {

                    String activityClass = "";
                    if (observableLifeCycleEvents != null && observableLifeCycleEvents.getLastKnownActivity() != null) {
                        activityClass = observableLifeCycleEvents.getLastKnownActivity().getLocalClassName();
                    }
                    LifeCycleEventPojo lifeCycleEventPojo = new LifeCycleEventPojo(EventType.APP_FOREGROUND, activityClass, sessionId, pageView_Resumed, null);
                    observableLifeCycleEvents.setLifeCycleEventPojo(lifeCycleEventPojo);
                }

                @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
                private void onMoveToBackground() {
                    String activityClass = "";
                    if (observableLifeCycleEvents != null && observableLifeCycleEvents.getLastKnownActivity() != null) {
                        activityClass = observableLifeCycleEvents.getLastKnownActivity().getLocalClassName();
                    }
                    LifeCycleEventPojo lifeCycleEventPojo = new LifeCycleEventPojo(EventType.APP_BACKGROUND, activityClass, sessionId, pageView_Resumed, null);
                    observableLifeCycleEvents.setLifeCycleEventPojo(lifeCycleEventPojo);
                }
            };
        }
        return lifecycleListener;
    }
}
