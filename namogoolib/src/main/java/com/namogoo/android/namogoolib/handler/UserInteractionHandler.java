package com.namogoo.android.namogoolib.handler;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.databinding.Observable;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.namogoo.android.namogoolib.db.dao.DeviceDataScribe;
import com.namogoo.android.namogoolib.helper.Config;
import com.namogoo.android.namogoolib.helper.Encryption;
import com.namogoo.android.namogoolib.helper.TouchUtils;
import com.namogoo.android.namogoolib.helper.WebClientReplacer;
import com.namogoo.android.namogoolib.pojo.events.UserInteractionEventPojo;
import com.namogoo.android.namogoolib.pojo.enums.EventType;
import com.namogoo.android.namogoolib.pojo.info.WebViewSendDataPojo;
import com.namogoo.android.namogoolib.pojo.observables.ObservableLifeCycleEvents;
import com.namogoo.android.namogoolib.pojo.observables.ObservableUserInteractionsEvents;
import com.namogoo.android.namogoolib.pojo.server.ClickTerm;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserInteractionHandler {
    private static final int SCAN_INTERVAL = 250;
    private String sessionId;
    private int pageView;
    private DeviceDataScribe deviceDataScribe;
    private String vendorId;

    private ObservableUserInteractionsEvents observableUserInteractionsEvents;
    private Handler activityScanHandler;
    private ArrayList<String> openDialogs;
    private boolean mIsActivityCreated = false;

    private boolean showDebugBackground = false;
    private HashMap<Integer, ConfigMatchResult> knownUIElements;
    private WebViewSendDataPojo webViewSendDataPojo;
    private Gson gson;

    public UserInteractionHandler(String sessionId, DeviceDataScribe deviceDataScribe, String vendorId) {
        this.pageView = 0;
        this.observableUserInteractionsEvents = new ObservableUserInteractionsEvents();
        this.sessionId = sessionId;
        this.knownUIElements = new HashMap<>();
        webViewSendDataPojo = new WebViewSendDataPojo(sessionId, 0);
        this.deviceDataScribe = deviceDataScribe;
        this.vendorId = vendorId;
        this.gson = new Gson();
    }

    public void addOnChangedCallback(Observable.OnPropertyChangedCallback callback)
    {
        observableUserInteractionsEvents.addOnPropertyChangedCallback(callback);
    }

    public Observable.OnPropertyChangedCallback getLifeCycleCallback() {
        return lifeCycleChanged;
    }

    private Observable.OnPropertyChangedCallback lifeCycleChanged = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            if (Config.getCustomerJourneyConfig().getEventTypesConfig().getEnableUIButton() == false && Config.getCustomerJourneyConfig().getEventTypesConfig().getEnableUIControl() == false && Config.getCustomerJourneyConfig().getEventTypesConfig().getEnableWebView() == false)
            {
                return;
            }
            ObservableLifeCycleEvents observableLifeCycleEvents = (ObservableLifeCycleEvents) sender;
            int potentialPageView = observableLifeCycleEvents.getLifeCycleEventPojo().getPageView();
            if (potentialPageView > pageView) {
                pageView = potentialPageView;
            }
            switch (observableLifeCycleEvents.getLifeCycleEventPojo().getEventType()) {
                case COMPONENT_INIT:
                    onActivityCreated();
                    break;
                case COMPONENT_FOREGROUND:
                    onActivityResumed(observableLifeCycleEvents.getLastKnownActivity());
                    break;
            }
        }
    };

    private void onActivityCreated() {
        mIsActivityCreated = true;
        openDialogs = new ArrayList<>();
        knownUIElements.clear();
    }

    private void onActivityResumed(final Activity activity) {
        if (mIsActivityCreated)
        {
            mIsActivityCreated = false;
            if (activityScanHandler != null)
            {
                activityScanHandler.removeCallbacksAndMessages(null);
            }
            recursiveScan(activity);

        }
    }

    private void recursiveScan(final Activity activity) {
        startAttachViewListeners(activity);
        activityScanHandler = new Handler();
        activityScanHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activity != null) {
                    recursiveScan(activity);
                }
            }
        }, SCAN_INTERVAL);
    }

    private void startAttachViewListeners(Activity activity) {
        View root = activity.findViewById(android.R.id.content).getRootView();
        if (root == null) {
            root = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        }
        if (root == null)
        {
            return;
        }

        ArrayList<String> removedFromScreen = new ArrayList();
        //removedFromScreen.addAll(viewListeners.keySet());
        //attachViewListeners(activity, root, removedFromScreen);
        List<View> roots = getViewRoots();
        reportOpenDialogs(roots, activity.getLocalClassName());
        //Log.e(TAG, "roots number:" + roots.size());
        for (View view : roots) {
            attachViewListeners(activity, view, new ArrayList<String>(), null, removedFromScreen);
        }
    }

    public static List<View> getViewRoots() {

        List<View> viewRoots = new ArrayList<>();

        try {
            Object windowManager;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                windowManager = Class.forName("android.view.WindowManagerGlobal")
                        .getMethod("getInstance").invoke(null);
            } else {
                Field f = Class.forName("android.view.WindowManagerImpl")
                        .getDeclaredField("sWindowManager");
                f.setAccessible(true);
                windowManager = f.get(null);
            }

            Field rootsField = windowManager.getClass().getDeclaredField("mRoots");
            rootsField.setAccessible(true);

            Field stoppedField = Class.forName("android.view.ViewRootImpl")
                    .getDeclaredField("mStopped");
            stoppedField.setAccessible(true);

            Field rootsView = Class.forName("android.view.ViewRootImpl").getDeclaredField("mView");
            rootsView.setAccessible(true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                List<ViewParent> viewParents = (List<ViewParent>) rootsField.get(windowManager);
                // Filter out inactive view roots
                for (ViewParent viewParent : viewParents) {
                    boolean stopped = (boolean) stoppedField.get(viewParent);
                    if (!stopped) {
                        viewRoots.add((View) rootsView.get(viewParent));
                    }
                }
            } else {
                ViewParent[] viewParents = (ViewParent[]) rootsField.get(windowManager);
                // Filter out inactive view roots
                for (ViewParent viewParent : viewParents) {
                    boolean stopped = (boolean) stoppedField.get(viewParent);
                    if (!stopped) {
                        viewRoots.add((View) rootsView.get(viewParent));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return viewRoots;
    }

    private void reportOpenDialogs(List<View> roots, String currentActivity) {
        List <View> filteredRoots = new ArrayList<>();
        for (View v: roots)
        {
            if (v.toString().contains(currentActivity))
            {
                filteredRoots.add(v);
            }
        }
        if (filteredRoots.size() < 2)
        {
            if (openDialogs.size() > 0) {
                for (String openDialog : openDialogs) {
                    reportDialogState(openDialog, false);
                }
                openDialogs.clear();
            }
            return;
        }

        //remove the main view and leave only the dialogs
        View maxView = filteredRoots.get(0);
        for (int i=1; i<filteredRoots.size(); i++)
        {
            if (maxView.getWidth() < filteredRoots.get(i).getWidth() || maxView.getHeight() < filteredRoots.get(i).getHeight())
            {
                maxView = filteredRoots.get(i);
            }
        }
        filteredRoots.remove(maxView);

        ArrayList<String> dialogsToClose = new ArrayList<>();
        dialogsToClose.addAll(openDialogs);
        Rect r = new Rect();
        filteredRoots.get(0).getWindowVisibleDisplayFrame(r);
        for (int i=0; i<filteredRoots.size(); i++)
        {
            View v = filteredRoots.get(i);
            String dialogName = getFirstTextViewChildValue(v);
            for (String dialogToClose : dialogsToClose) {
                if (dialogToClose.equals(dialogName))
                {
                    dialogsToClose.remove(dialogToClose);
                    break;
                }
            }
            boolean dialogAlreadyOpen = false;
            for (String openDialog : openDialogs)
            {
                if (openDialog.equals(dialogName))
                {
                    dialogAlreadyOpen = true;
                    continue;
                }
            }
            if (dialogAlreadyOpen == true) {
                continue;
            } else {
                openDialogs.add(dialogName);
                reportDialogState(dialogName, true);
            }
        }
        for (String dialogToClose : dialogsToClose) {
            reportDialogState(dialogToClose, false);
            openDialogs.remove(dialogToClose);
        }

    }

    private String getFirstTextViewChildValue(View v) {
        if (v instanceof ViewGroup)
        {
            ViewGroup vg = (ViewGroup)v;
            for (int i=0; i<vg.getChildCount(); i++) {
                String dialogName = getFirstTextViewChildValue(((ViewGroup) v).getChildAt(i));
                if (!dialogName .startsWith("hash-"))
                {
                    if (dialogName.length() > 30)
                    {
                        dialogName = dialogName.substring(0,30);
                    }
                    return dialogName;
                }
            }
        }
        if (v instanceof TextView)
        {
            return ((TextView)v).getText().toString();
        }
        return "hash-"+v.toString().hashCode();
    }

    private void attachViewListeners(Activity activity, View view, final ArrayList<String> hierarchy, String listItemString, ArrayList removedFromScreen)
    {
        final View.OnTouchListener viewTouchListener = TouchUtils.getOnTouchListener(activity, view);
        final View.OnClickListener viewClickListener = viewTouchListener != null ? null : TouchUtils.getOnClickListener(view);

        boolean clickAlreadyConnected = false;
        boolean touchAlreadyConnected = false;
        if (viewClickListener != null)
        {
            if (viewClickListener.getClass().getName().contains("UserInteractionHandler"))
            {
                clickAlreadyConnected = true;
            }
        }

        if (viewTouchListener != null)
        {
            if (viewTouchListener.getClass().getName().contains("UserInteractionHandler"))
            {
                touchAlreadyConnected = true;
            }
        }
        boolean alreadyConnected = clickAlreadyConnected || touchAlreadyConnected;

        boolean isParentList = (view.getParent() instanceof ListView || view.getParent().getClass().getSimpleName().contains("RecyclerView"));

        if (isParentList) {
            listItemString = getFirstTextViewChildValue(view);
        }
        final String currentView = getViewId(view, listItemString);

        if (!alreadyConnected && view instanceof EditText)
        {
            //EditText e = (EditText)view;
            //e.addTextChangedListener(new NamogooTextWatcher(customerJourneyPojo, hierarchy, currentView));
        }

        if (Config.getCustomerJourneyConfig().getEventTypesConfig() != null && Config.getCustomerJourneyConfig().getEventTypesConfig().getEnableWebView() && !alreadyConnected && view instanceof WebView)
        {
            WebView wv = (WebView) view;
            WebClientReplacer webClientReplacer = new WebClientReplacer();
            webViewSendDataPojo.setCurrentPage(pageView);
            webViewSendDataPojo.setUuid(deviceDataScribe.getDeviceUUID());
            String encryptedData = Encryption.Encrypt(gson.toJson(webViewSendDataPojo));
            webClientReplacer.insertNamogooToWebViewClient(wv, vendorId, Config.getCustomerJourneyConfig().getFeatures().getWebViewRemoveSubdomain(), encryptedData);
            alreadyConnected = true;
        }

        //viewClickListener.getClass().getName().contains(this.getClass().getPackage().getName())
        if (!alreadyConnected && (viewTouchListener != null || viewClickListener != null || !(view instanceof ViewGroup)))
        {
            final ConfigMatchResult matchResult = checkConfigMatching(activity, view);
            if (matchResult.isShouldTrackEvent()) {
                if (viewClickListener != null) {
                    if (showDebugBackground)
                        view.setBackgroundColor(activity.getResources().getColor(android.R.color.holo_blue_light));
                    final JSONObject value = viewToJsonObject(activity, view, "CLICK");

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reportEvent(EventType.ACTION_SENT, hierarchy, currentView, value, matchResult.getRuleName());
                            if (viewClickListener != null) {
                                viewClickListener.onClick(v);
                            }
                        }
                    });
                } else {
                    if (showDebugBackground)
                        view.setBackgroundColor(activity.getResources().getColor(android.R.color.holo_red_light));
                    final JSONObject value = viewToJsonObject(activity, view, "ACTION_DOWN");
                    view.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            //Log.d(TAG, "onTouch " + v.toString());
                            //customerJourneyPojo.addEvent(CustomerJourneyEventPojo.EventType.PRESS, "BB-"+currentView, null);

                        /*
                        if (viewTouchListener == null && event.getAction() == MotionEvent.ACTION_DOWN) {
                            reportEvent(EventType.TOUCH_DOWN, hierarchy, currentView, null);
                        } else
                        {
                            activityGestureListener.setView(hierarchy, currentView);
                            gestureDetector.onTouchEvent(event);
                        } */
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                reportEvent(EventType.ACTION_SENT, hierarchy, currentView, value, matchResult.getRuleName());
                            }
                            if (viewTouchListener != null) {
                                return viewTouchListener.onTouch(v, event);
                            }
                            return false;
                        }
                    });
                }
                //return;
            }
        }
        if (viewClickListener != null & !(view.getParent() instanceof FrameLayout) && hierarchy.size() > 4 )
        {
            return;
        }
        if(view instanceof ViewGroup)
        {
            ViewGroup vg = (ViewGroup)view;
            for (int i = 0; i < vg.getChildCount(); i++)
            {
                ArrayList<String> childHierarchy = new ArrayList<>();
                childHierarchy.addAll(hierarchy);
                childHierarchy.add(currentView);
                attachViewListeners(activity, vg.getChildAt(i), childHierarchy, listItemString, removedFromScreen);  // Recursive call.
            }
        }
    }

    private String getViewId(View v, String listItemString) {
        boolean hasId = false;
        try {
            if (v.getId() != View.NO_ID) {
                String id = v.getResources().getResourceName(v.getId());
                hasId = true;
            }
        } catch (Exception e) {
        }
        if (hasId == false)
        {
            if (listItemString != null)
            {
                return v.getClass().getSimpleName()+"::"+v.toString().hashCode()+"::"+listItemString;
            }
            if (!(v instanceof ViewGroup)) {
                if (v instanceof TextView)
                {
                    return "nmgtv::"+((TextView)v).getText().toString();
                }
            } else {
                ViewGroup vg = (ViewGroup)v;
                String id;
                for (int i = 0; i < vg.getChildCount(); i++)
                {
                    id = getViewId(vg.getChildAt(i), listItemString);
                    if (!id.startsWith("no-id")) {
                        return v.getClass().getSimpleName()+"-"+id.substring(id.lastIndexOf("::"));
                    }
                }
            }
            return "nmgnoid::"+v.toString().hashCode();
        }
        else {
            String id = v.getResources().getResourceName(v.getId());
            if (id.indexOf("/") > 0)
            {
                id = id.substring(id.indexOf("/")+1);
            }
            id += "::";
            if (v instanceof TextView)
            {
                id += ((TextView)v).getText().toString();
            } else
            {
                if (listItemString != null)
                {
                    id +=listItemString;
                } else {
                    id += v.toString().hashCode();
                }
            }
            return id;
        }
    }

    private void reportDialogState(String dialogName, boolean isOpen) {
        UserInteractionEventPojo userInteractionEventPojo = new UserInteractionEventPojo(isOpen ? EventType.ALERT_POPUP_OPENED : EventType.ALERT_POPUP_CLOSED, sessionId, dialogName, pageView, null);
        observableUserInteractionsEvents.setUserInteractionEventPojo(userInteractionEventPojo);
    }

    private void reportEvent(EventType eventType, ArrayList<String> hierarchy, String targetId, JSONObject value, String ruleName) {
        String strValue = value != null ? value.toString() : null;
        UserInteractionEventPojo userInteractionEventPojo = new UserInteractionEventPojo(eventType, targetId, sessionId, pageView, ruleName, hierarchy, strValue);
        observableUserInteractionsEvents.setUserInteractionEventPojo(userInteractionEventPojo);
    }

    private ConfigMatchResult checkConfigMatching(Activity activity, View view) {
        if (view.getId() != 0) {
            if (knownUIElements.containsKey(view.getId())) {
                return knownUIElements.get(view.getId());
            }
        }
        ConfigMatchResult noMatch = new ConfigMatchResult(false, null);

        if (Config.getCustomerJourneyConfig().getEventTypesConfig().getEnableUIControl() == false && Config.getCustomerJourneyConfig().getEventTypesConfig().getEnableUIButton() == false)
        {
            return noMatch;
        }
        HashMap<String, ArrayList<ClickTerm>> clickKeyMap = Config.getCustomerJourneyConfig().getClickKeyMap();
        if (clickKeyMap == null || clickKeyMap.size() == 0 /*|| true*/) {
            return new ConfigMatchResult(true, null);
        }
        ArrayList<ClickTerm> clickTerms = clickKeyMap.get("textSearch");
        if (clickTerms == null || clickTerms.size() == 0) {
            return new ConfigMatchResult(true, null);
        }
        for (ClickTerm clickTerm : clickTerms) {
            //Term has activity container class but not equal to activity name
            if (clickTerm.getViewContainer() != null && !activity.getLocalClassName().toLowerCase().equals(clickTerm.getViewContainer())) {
                continue;
            }
            //Term has target class but not equal to view
            if (clickTerm.getTargetClass() != null && !view.getClass().getSimpleName().toLowerCase().equals(clickTerm.getTargetClass().toLowerCase()))
            {
                continue;
            }
            ConfigMatchResult foundMatch = new ConfigMatchResult(true, clickTerm.getRuleName());
            if (clickTerm.getSearchTerm() == null || clickTerm.getSearchTerm().length() == 0) {
                return foundMatch;
            }
            if (view.getClass().getSimpleName().toLowerCase().indexOf(clickTerm.getSearchTerm().toLowerCase()) > 0) {
                return foundMatch;
            }
            try {
                if (view.getId() != 0){
                    String vId = activity.getResources().getResourceName(view.getId()).toLowerCase().replace("-", "").replace(" ", "");
                    String term = "/"+clickTerm.getSearchTerm().toLowerCase().replace(" ", "").replace("-", "");
                    if (vId.indexOf(term) >= 0) {
                        return foundMatch;
                    }
                }
            } catch (Exception e) {
            }
            try {
                if (view instanceof TextView) {
                    if (((TextView)view).getText().toString().toLowerCase().indexOf(clickTerm.getSearchTerm().toLowerCase()) == 0){
                        return foundMatch;
                    }
                }
            } catch (Exception e) {
            }
        }
        return noMatch;
    }

    final class ConfigMatchResult {
        private final boolean shouldTrackEvent;
        private final String ruleName;

        public ConfigMatchResult(boolean shouldTrackEvent, String ruleName) {
            this.shouldTrackEvent = shouldTrackEvent;
            this.ruleName = ruleName;
        }

        public boolean isShouldTrackEvent() {
            return shouldTrackEvent;
        }

        public String getRuleName() {
            return ruleName;
        }
    }

    private JSONObject viewToJsonObject(Activity activity, View view, String action) {
        if (activity == null || view == null) {
            return null;
        }
        JSONObject value = new JSONObject();
        try {
            value.put("class", view.getClass().getSimpleName());
            value.put("objId", activity.getResources().getResourceName(view.getId()));
            value.put("action", action);
            if (view instanceof TextView)
                value.put("text", ((TextView) view).getText());
            if (view instanceof EditText)
                value.put("text", ((EditText) view).getHint());
        } catch (Exception e) {
        }
        return null;
    }
}
