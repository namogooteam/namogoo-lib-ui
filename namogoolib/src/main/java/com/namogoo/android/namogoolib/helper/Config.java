package com.namogoo.android.namogoolib.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.namogoo.android.namogoolib.pojo.server.ConfigFeaturesPojo;
import com.namogoo.android.namogoolib.pojo.server.CustomerJourneyConfigResponsePojo;
import com.namogoo.android.namogoolib.pojo.server.EventTypesConfig;

import java.util.HashMap;
import java.util.Random;

public class Config {

    private static final String BASE_URL = "https://mobile-sdk.sitelabweb.com/";
    private static final String BASE_URL_PREPROD = "https://mobile-sdk-preprod.sitelabweb.com/";

    private static final String SHARED_PREFERENCES = "_nmg_sdk_config";
    private static final String KEY_BASE_URL = "_base_url";
    private static final String KEY_BASE_URL_PREPROD = "_base_url_preprod";

    private static String baseUrl;
    private static String baseUrlPreprod;
    private static CustomerJourneyConfigResponsePojo customerJourneyConfig;

    private static Boolean shouldSendExtraTracking;

    private static final Object syncConfigObj = new Object();

    public static String getBaseUrl(Context context) {
        if (baseUrl != null)
        {
            return baseUrl;
        }
        baseUrl = getStoredConfig(context, KEY_BASE_URL, BASE_URL);
        return baseUrl;
    }

    public static String getBaseUrlPreprod(Context context) {
        if (baseUrlPreprod != null)
        {
            return baseUrlPreprod;
        }
        baseUrlPreprod = getStoredConfig(context, KEY_BASE_URL_PREPROD, BASE_URL_PREPROD);
        return baseUrlPreprod;
    }

    public static void setCustomerJourneyConfig(CustomerJourneyConfigResponsePojo customerJourneyConfigResponsePojo) {
        synchronized (syncConfigObj) {
            customerJourneyConfig = customerJourneyConfigResponsePojo;
        }
    }

    public static CustomerJourneyConfigResponsePojo getCustomerJourneyConfig() {
        synchronized (syncConfigObj) {
            if (customerJourneyConfig == null) {
                initEmptyCustomerJourneyConfig();
            }
        }
        return customerJourneyConfig;
    }

    private static void setBaseUrl(Context context, String baseUrl) {
        if(baseUrl == null)
        {
            return;
        }
        saveConfig(context, KEY_BASE_URL, baseUrl);
        Config.baseUrl = baseUrl;
    }

    private static void setBaseUrlPreprod(Context context, String baseUrlPreprod) {
        if(baseUrlPreprod == null)
        {
            return;
        }
        saveConfig(context, KEY_BASE_URL_PREPROD, baseUrlPreprod);
        Config.baseUrlPreprod = baseUrlPreprod;
    }

    private static void saveConfig(Context context, String key, int value) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private static void saveConfig(Context context, String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private static String getStoredConfig(Context context, String key, String defaultValue) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return prefs.getString(key, defaultValue);
    }

    private static int getStoredConfig(Context context, String key, int defaultValue) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return prefs.getInt(key, defaultValue);
    }

    private static void initEmptyCustomerJourneyConfig()
    {
        EventTypesConfig eventTypesConfig = new EventTypesConfig(true, true, true, true);

        Boolean webViewRemoveSubdomain = Boolean.TRUE;

        String webViewService =
                    "try {" +
                    "window.localStorage.setItem('$DOMAIN$_$VENDOR_ID$_m', '$ENCRYPTED_DATA$');" +
                    "} catch (e) {}";

        ConfigFeaturesPojo features = new ConfigFeaturesPojo(webViewRemoveSubdomain, webViewService);

        customerJourneyConfig = new CustomerJourneyConfigResponsePojo(eventTypesConfig, features);
    }
}
