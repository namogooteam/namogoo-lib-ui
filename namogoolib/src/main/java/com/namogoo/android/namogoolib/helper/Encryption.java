package com.namogoo.android.namogoolib.helper;

import android.os.Build;
import android.util.Base64;

import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

public class Encryption {

    /**
     * Perform a URL-safe encryption.
     * It is based on a base64 encoding.
     * The first character of the result represents the number of seed characters.
     * Transform the character to its b64 index, modulo 10. If the result is < 5 then add 5.
     * The next 5-9 characters are the seed by which the rest of the base64 string is encoded.
     * Used base64 characters and their ascii values:
     * -    charcode:   45
     * 0-9  charcodes:  48-57
     * A-Z  charcodes:  65-90
     * _    charcode:   95
     * a-z  charcodes:  97-122
     * ~    charcode:   126
     * @method encrypt
     * @param {String} str
     * @return {String}
     */
    public static String Encrypt(String str)
    {
        Character seedLenChar = generateRandomB64Char();
        int seedLen =  b64CharToIndex(seedLenChar) % 10;
        if (seedLen < 5) seedLen += 5;
        String seed = generateSeed(seedLen);
        int changer = -1;
        int moveby;
        int newIndex;
        String b64 = utf8_to_b64(str).replaceAll(Pattern.quote("+"), "-").replaceAll(Pattern.quote("="), "_").replaceAll(Pattern.quote("/"), "~");
        String enc = seedLenChar + seed;

        for (int i = 0, leni = b64.length(); i < leni; i++) {
            if (changer != 0) {
                moveby   = b64CharToIndex(seed.charAt(i % seedLen)) * changer;
                newIndex = b64CharToIndex(b64.charAt(i)) + moveby;
                newIndex = newIndex < 0 ? newIndex + 65 : newIndex >= 65 ? newIndex - 65 : newIndex;
                enc += indexToB64Char(newIndex);
            } else {
                enc += b64.charAt(i);
            }
            changer = cyclic3(changer);
        }
        return enc;
    }

    public static String Decrypt (String str) {

        Character seedLenChar = str.charAt(0);
        int seedLen = b64CharToIndex(seedLenChar) % 10;
        if (seedLen < 5) seedLen += 5;
        String seed = str.substring(1, seedLen + 1);
        int changer = -1;
        int moveby;
        int newIndex;
        String dec = "";

        str = str.substring(seedLen + 1);
        for (int i = 0, leni = str.length(); i < leni; i++) {
            if (changer != 0) {
                moveby = b64CharToIndex(seed.charAt(i % seedLen)) * changer * -1;
                newIndex = b64CharToIndex(str.charAt(i)) + moveby;
                newIndex = newIndex < 0 ? newIndex + 65 : newIndex >= 65 ? newIndex - 65 : newIndex;
                dec += indexToB64Char(newIndex);
            } else {
                dec += str.charAt(i);
            }
            changer = cyclic3(changer);
        }
        try {
            dec = b64_to_utf8(dec.replaceAll("~", "/").replaceAll( "_", "=").replace("-", "+"));
        } catch (Exception ex) {
            dec = str;
        }
        return dec;
    }

    private static int cyclic3 (int n) {
        return (n >> -n) + (-n | 1);
    };


    /**
     * Safe Base64 decoding (handles utf8 characters properly)
     * @method b64_to_utf8
     * @param {String} str
     * @return {String}
     */
    private static String b64_to_utf8 (String base64) {
        byte[] data = Base64.decode(base64, Base64.DEFAULT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return new String(data, StandardCharsets.UTF_8);
        }
        return new String(data);
    };


    /**
     * Safe Base64 encoding (handles utf8 characters properly)
     * @method utf8_to_b64
     * @param {String} str
     * @return {String}
     */
    private static String utf8_to_b64 (String str) {
        try {
            byte[] data = str.getBytes("utf-8");
            return Base64.encodeToString(data, Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    };

    private static String generateSeed(int seedLen) {
        String seed = "";
        for (int i=0; i< seedLen; i++) {
            seed += generateRandomB64Char();
        }
        return seed;
    }


    /**
     * Convert a base64 character to its index
     * @method b64CharToIndex
     * @param {String} ch
     * @return {Number}
     */
    private static int b64CharToIndex (Character ch) {
        int n = ch;
        if (n > 122) n -= 3;
        if (n > 95) n -= 1;
        if (n > 90) n -= 4;
        if (n > 57) n -= 7;
        if (n > 45) n -= 2;
        return n - 45;
    };

    /**
     * @method indexToB64Char
     * @param {Number} n
     * @return {String}
     */
    private static Character indexToB64Char (int n) {
        n += 45;
        if (n > 45) n += 2;
        if (n > 57) n += 7;
        if (n > 90) n += 4;
        if (n > 95) n += 1;
        if (n > 122) n += 3;
        return (char)(n);
    };
    /**
     * Generate a random base64 character.
     * This version of base64 uses - _ and ~ instead of + = and / to be URL-safe
     * @method generateRandomB64Char
     * @return {string}
     */
    private static Character generateRandomB64Char () {
        return indexToB64Char((int)(Math.floor(Math.random() * 65)));
    };
}
