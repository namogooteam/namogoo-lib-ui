package com.namogoo.android.namogoolib.helper;


/**
 * Created by Udi
 */

public class KeyUtil {
    public static final String datePattern = "dd MMM yyyy HH:mm";
    public static final String dateSecondsPattern = "dd MMM yyyy HH:mm:ss";
    public static final String KEY_MODE = "key_mode";

    /*** Sensor */
    public static final String KEY_SENSOR_NAME = "key_sensor_name";
    public static final String KEY_SENSOR_TYPE = "key_sensor_type";
    public static final String KEY_SENSOR_ICON = "key_sensor_icon";

    public static float KEY_LAST_KNOWN_HUMIDITY = 0f;

    public static final int KEY_CAMERA_CODE = 101;
    public static final int KEY_CALL_PERMISSION = 102;
    public static final int KEY_READ_PHONE_STATE = 103;
    public static final int IS_USER_COME_FROM_SYSTEM_APPS = 1;
    public static final int IS_USER_COME_FROM_USER_APPS = 2;

    public static final String DATABASE_NAME = "namogoo-database";

    public static final String EVENT_TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
