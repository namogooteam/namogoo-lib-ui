package com.namogoo.android.namogoolib.helper;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.ClientCertRequest;
import android.webkit.HttpAuthHandler;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.SafeBrowsingResponse;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class NMGWebViewClient extends WebViewClient {
    private WebViewClient oldWebViewClient;
    private String vendorId;
    private boolean removeSubDomain;
    private String storageData;

    public NMGWebViewClient(WebViewClient oldWebViewClient, String vendorId, boolean removeSubDomain, String storageData) {
        this.oldWebViewClient = oldWebViewClient;
        this.vendorId = vendorId;
        this.removeSubDomain = removeSubDomain;
        this.storageData = storageData;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return oldWebViewClient.shouldOverrideUrlLoading(view, url);
    }

    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return oldWebViewClient.shouldOverrideUrlLoading(view, request);
        }
        return false;
    }

    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        oldWebViewClient.onPageStarted(view, url, favicon);
    }

    public void onPageFinished(WebView view, String url) {
        oldWebViewClient.onPageFinished(view, url);

        String domain = calcDomain(url);
        if (domain == null)
            return;

        String js = Config.getCustomerJourneyConfig().getFeatures().getWebViewService();
        if (js == null) {
            return;
        }
        js = "javascript:(function() {" + js + "})()";
        js = js.replace("$DOMAIN$", domain);
        js = js.replace("$VENDOR_ID$", vendorId);
        js = js.replace("$ENCRYPTED_DATA$", storageData);
        view.loadUrl(js);
    }

    public void onLoadResource(WebView view, String url) {
        oldWebViewClient.onLoadResource(view, url);
    }

    public void onPageCommitVisible(WebView view, String url) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            oldWebViewClient.onPageCommitVisible(view, url);
        }
    }

    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        oldWebViewClient.shouldInterceptRequest(view, url);
        return null;
    }

    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return oldWebViewClient.shouldInterceptRequest(view, request);
        }
        return null;
    }

    public void onTooManyRedirects(WebView view, Message cancelMsg, Message continueMsg) {
        oldWebViewClient.onTooManyRedirects(view, cancelMsg, continueMsg);
    }

    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        oldWebViewClient.onReceivedError(view, errorCode, description, failingUrl);
    }

    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            oldWebViewClient.onReceivedError(view, request, error);
        }
    }

    public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            oldWebViewClient.onReceivedHttpError(view, request, errorResponse);
        }
    }

    public void onFormResubmission(WebView view, Message dontResend, Message resend) {
        oldWebViewClient.onFormResubmission(view, dontResend, resend);
    }

    public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
        oldWebViewClient.doUpdateVisitedHistory(view, url, isReload);
    }

    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        oldWebViewClient.onReceivedSslError(view, handler, error);
    }

    public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            oldWebViewClient.onReceivedClientCertRequest(view, request);
        }
    }

    public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
        oldWebViewClient.onReceivedHttpAuthRequest(view, handler, host, realm);
    }

    public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
        return oldWebViewClient.shouldOverrideKeyEvent(view, event);
    }

    public void onUnhandledKeyEvent(WebView view, KeyEvent event) {
        oldWebViewClient.onUnhandledKeyEvent(view, event);
    }

    public void onScaleChanged(WebView view, float oldScale, float newScale) {
        oldWebViewClient.onScaleChanged(view, oldScale, newScale);
    }

    public void onReceivedLoginRequest(WebView view, String realm, String account, String args) {
        oldWebViewClient.onReceivedLoginRequest(view, realm, account, args);
    }

    public boolean onRenderProcessGone(WebView view, RenderProcessGoneDetail detail) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            oldWebViewClient.onRenderProcessGone(view, detail);
        }
        return false;
    }

    public void onSafeBrowsingHit(WebView view, WebResourceRequest request,  int threatType, SafeBrowsingResponse callback) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            oldWebViewClient.onSafeBrowsingHit(view, request, threatType, callback);
        }
    }

    private String calcDomain(String url) {
        if (!url.toLowerCase().startsWith("http://") && !url.toLowerCase().startsWith("https://"))
        {
            return null;
        }
        String urlOnly = url.substring(7);
        if (urlOnly.startsWith("/")) {
            urlOnly = urlOnly.substring(1);
        }
        String[] parts = urlOnly.split("/");
        if (parts.length == 0) {
            return null;
        }
        String host = parts[0];
        if (removeSubDomain) {
            String[] domainParts = host.split("\\.");
            if (domainParts.length > 2)
            {
                host = domainParts[domainParts.length-2] + "." + domainParts[domainParts.length-1];
            }
        }
        return host;
    }
}
