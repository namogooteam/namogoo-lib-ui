package com.namogoo.android.namogoolib.helper;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.View;

import java.lang.reflect.Field;

public class TouchUtils {
    private static String TAG = "NMG-Utils";

    public static View.OnClickListener getOnClickListener(View view) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return getOnClickListenerV14(view);
        } else {
            return getOnClickListenerV(view);
        }
    }

    //Used for APIs lower than ICS (API 14)
    private static View.OnClickListener getOnClickListenerV(View view) {
        View.OnClickListener retrievedListener = null;
        String viewStr = "android.view.View";
        Field field;

        try {
            field = Class.forName(viewStr).getDeclaredField("mOnClickListener");
            retrievedListener = (View.OnClickListener) field.get(view);
        } catch (NoSuchFieldException ex) {
            Log.e("Reflection", "No Such Field.");
        } catch (IllegalAccessException ex) {
            Log.e("Reflection", "Illegal Access.");
        } catch (ClassNotFoundException ex) {
            Log.e("Reflection", "Class Not Found.");
        }

        return retrievedListener;
    }

    //Used for new ListenerInfo class structure used beginning with API 14 (ICS)
    private static View.OnClickListener getOnClickListenerV14(View view) {
        View.OnClickListener retrievedListener = null;
        String viewStr = "android.view.View";
        String lInfoStr = "android.view.View$ListenerInfo";
        //Log.d(TAG, "view" + view.toString());
        try {
            Field listenerField = Class.forName(viewStr).getDeclaredField("mListenerInfo");
            Object listenerInfo = null;
            //Log.d(TAG, "listenerField: " + (listenerField != null));
            if (listenerField != null) {
                listenerField.setAccessible(true);
                listenerInfo = listenerField.get(view);
            }
            //Log.d(TAG, "listenerInfo: " + (listenerInfo != null));
            Field clickListenerField = Class.forName(lInfoStr).getDeclaredField("mOnClickListener");
            //Log.d(TAG, "clickListenerField: " + (clickListenerField != null));

            if (clickListenerField != null && listenerInfo != null) {
                retrievedListener = (View.OnClickListener) clickListenerField.get(listenerInfo);
            }
            //Log.d(TAG, "retrievedListener: " + (retrievedListener != null));

        } catch (NoSuchFieldException ex) {
            //Log.e("Reflection", "No Such Field.");
        } catch (IllegalAccessException ex) {
            //Log.e("Reflection", "Illegal Access.");
        } catch (ClassNotFoundException ex) {
            //Log.e("Reflection", "Class Not Found.");
        }

        return retrievedListener;
    }

    public static View.OnTouchListener getOnTouchListener(Activity activity, View view) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return getOnTouchListenerV14(view);
        } else {
            return getOnTouchListenerV(view);
        }
    }

    //Used for APIs lower than ICS (API 14)
    private static View.OnTouchListener getOnTouchListenerV(View view) {
        View.OnTouchListener retrievedListener = null;
        String viewStr = "android.view.View";
        Field field;

        try {
            field = Class.forName(viewStr).getDeclaredField("mOnTouchListener");
            retrievedListener = (View.OnTouchListener) field.get(view);
        } catch (NoSuchFieldException ex) {
            Log.e("Reflection", "No Such Field.");
        } catch (IllegalAccessException ex) {
            Log.e("Reflection", "Illegal Access.");
        } catch (ClassNotFoundException ex) {
            Log.e("Reflection", "Class Not Found.");
        }

        return retrievedListener;
    }

    //Used for new ListenerInfo class structure used beginning with API 14 (ICS)
    private static View.OnTouchListener getOnTouchListenerV14(View view) {
        View.OnTouchListener retrievedListener = null;
        String viewStr = "android.view.View";
        String lInfoStr = "android.view.View$ListenerInfo";
        //Log.d(TAG, "view" + view.toString());
        try {
            Field listenerField = Class.forName(viewStr).getDeclaredField("mListenerInfo");
            Object listenerInfo = null;
            //Log.d(TAG, "listenerField: " + (listenerField != null));
            if (listenerField != null) {
                listenerField.setAccessible(true);
                listenerInfo = listenerField.get(view);
            }
            //Log.d(TAG, "listenerInfo: " + (listenerInfo != null));
            Field touchListenerField = Class.forName(lInfoStr).getDeclaredField("mOnTouchListener");
            touchListenerField.setAccessible(true);
            //Log.d(TAG, "clickListenerField: " + (clickListenerField != null));

            if (touchListenerField != null && listenerInfo != null) {
                retrievedListener = (View.OnTouchListener) touchListenerField.get(listenerInfo);
            }
            //Log.d(TAG, "retrievedListener: " + (retrievedListener != null));

        } catch (NoSuchFieldException ex) {
            Log.e("Reflection", "No Such Field.");
        } catch (IllegalAccessException ex) {
            Log.e("Reflection", "Illegal Access.");
        } catch (ClassNotFoundException ex) {
            Log.e("Reflection", "Class Not Found.");
        }

        return retrievedListener;
    }

    /*
    public static View.OnTouchListener getOnTouchListener(Activity activity, View v) {
        Field field_mListenerInfo = null;
        Object originalListener = null;
        Object myListenerInfo = null;
        Field field_mOnTouchListener = null;

        try {
            View viewForField = new View(activity); //needed just to get field name
            field_mListenerInfo = viewForField.getClass().getDeclaredField("mListenerInfo");
            field_mListenerInfo.setAccessible(true);
            if (myListenerInfo == null)
            {
                return null;
            }
            originalListener = field_mOnTouchListener.get(myListenerInfo);
            myListenerInfo = field_mListenerInfo.get(v);
            field_mOnTouchListener = myListenerInfo.getClass().getDeclaredField("mOnTouchListener");
            field_mOnTouchListener.setAccessible(true);

            if (field_mOnTouchListener != null && myListenerInfo != null) {
                return (View.OnTouchListener) field_mOnTouchListener.get(myListenerInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

     */
}
