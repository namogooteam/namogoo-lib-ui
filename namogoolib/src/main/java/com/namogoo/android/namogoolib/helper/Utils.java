package com.namogoo.android.namogoolib.helper;

import android.annotation.SuppressLint;

import com.namogoo.android.namogoolib.NMGLibrary;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class Utils {

    private static String USER_AGENT_DELIMITER = ";";
    private static int USER_AGENT_FORMAT_VERSION = 1;
    private static String userAgent;

    public static String getDateString(long timeStamp) {

        try {
            @SuppressLint("SimpleDateFormat") DateFormat sdf = new SimpleDateFormat(KeyUtil.datePattern);
            return sdf.format(getDate(timeStamp));
        } catch (Exception ex) {
            return "-";
        }
    }

    public static String getDateStringSeconds(long timeStamp) {

        try {
            @SuppressLint("SimpleDateFormat") DateFormat sdf = new SimpleDateFormat(KeyUtil.dateSecondsPattern);
            return sdf.format(getDate(timeStamp));
        } catch (Exception ex) {
            return "-";
        }
    }

    public static Date getDate(long timeStamp) {

        try {
            return new Date(timeStamp);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getUserAgent()
    {
        if (userAgent == null) {
            userAgent = USER_AGENT_FORMAT_VERSION + USER_AGENT_DELIMITER +
                    "Android" + USER_AGENT_DELIMITER +
                    NMGLibrary.getInstance().getDeviceInfo().getOS().getApiLevel() + USER_AGENT_DELIMITER +
                    NMGLibrary.getInstance().getDeviceInfo().getBuild().getManufacturer() + USER_AGENT_DELIMITER +
                    NMGLibrary.getInstance().getDeviceInfo().getBuild().getModel() + USER_AGENT_DELIMITER +
                    NMGLibrary.getInstance().getVersion() + USER_AGENT_DELIMITER +
                    NMGLibrary.getInstance().getDeviceInfo().getBuild().getPackageName() + USER_AGENT_DELIMITER +
                    NMGLibrary.getInstance().getDeviceInfo().getBuild().getPackageVersion();
        }
        return userAgent;
    }

    public static String mapToString(HashMap<String, String> map) {
        if (map == null)
        {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();

        for (String key : map.keySet()) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append("&");
            }
            String value = map.get(key);
            try {
                stringBuilder.append((key != null ? URLEncoder.encode(key, "UTF-8") : ""));
                stringBuilder.append("=");
                stringBuilder.append(value != null ? URLEncoder.encode(value, "UTF-8") : "");
            } catch (Exception e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e);
            }
        }

        return stringBuilder.toString();
    }

    public static String mapToJsonString (HashMap<String, String> map) {
        if (map == null) {
            return null;
        }
        try {
            return new JSONObject(map).toString();
        }
        catch (Exception e) {
            return null;
        }
    }

    public static String mapObjToJsonString (HashMap<String, Object> map) {
        if (map == null) {
            return null;
        }
        try {
            return new JSONObject(map).toString();
        } catch (Exception e) {
            return null;
        }
    }
}
