package com.namogoo.android.namogoolib.helper;


import android.os.Build;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.lang.reflect.Field;

public class WebClientReplacer {

    public void insertNamogooToWebViewClient(WebView webView, String vendorId, boolean removeSubDomain, String storageData)  {
        WebViewClient oldWebViewClient = extractWebViewClient(webView);
        if (oldWebViewClient == null) {
            return;
        }
        if (oldWebViewClient instanceof NMGWebViewClient) {
            return;
        }
        NMGWebViewClient newWebViewClient = new NMGWebViewClient(oldWebViewClient, vendorId, removeSubDomain, storageData);
        webView.setWebViewClient(newWebViewClient);
    }

    private WebViewClient extractWebViewClient(WebView webView){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return webView.getWebViewClient();
        }
        Object providerInstance = getProviderInstance(webView);
        Object contentsClientAdapter = getContentsClientAdapter(providerInstance);
        Object webViewClient = getWebViewClientFromClientAdapter(contentsClientAdapter);
        if (webViewClient instanceof WebViewClient) {
            return  (WebViewClient) webViewClient;
        }
        return null;
    }

    public Object getProviderInstance(WebView webView) {
        Object returnValue = null;
        try {
            Class<? extends WebView> mWebViewClass = webView.getClass();
            Field providerField = mWebViewClass.getDeclaredField("mProvider");
            providerField.setAccessible(true);
            Object mProvider = providerField.get(webView);
            returnValue = mProvider;

        } catch (NoSuchFieldException aE) {
            aE.printStackTrace();
        } catch (IllegalAccessException aE) {
            aE.printStackTrace();
        }

        return returnValue;
    }


    private Object getContentsClientAdapter(Object aProviderInstance) {
        Object returnValue = null;
        try {
            Class<?> aClass = aProviderInstance.getClass();
            Field contentsClientAdapterField = aClass.getDeclaredField("mContentsClientAdapter");
            contentsClientAdapterField.setAccessible(true);
            Object mContentsClientAdapter = contentsClientAdapterField.get(aProviderInstance);
            returnValue = mContentsClientAdapter;

        } catch (NoSuchFieldException aE) {
            aE.printStackTrace();
        } catch (IllegalAccessException aE) {
            aE.printStackTrace();
        }

        return returnValue;
    }


    private Object getWebViewClientFromClientAdapter(Object aContentsClientAdapter) {
        Object returnValue = null;
        try {
            Class<?> aClass = aContentsClientAdapter.getClass();
            Field WebViewClientField = aClass.getDeclaredField("mWebViewClient");
            WebViewClientField.setAccessible(true);
            Object mWebViewClient = WebViewClientField.get(aContentsClientAdapter);
            returnValue = mWebViewClient;

        } catch (NoSuchFieldException aE) {
            aE.printStackTrace();
        } catch (IllegalAccessException aE) {
            aE.printStackTrace();
        }

        return returnValue;
    }

}
