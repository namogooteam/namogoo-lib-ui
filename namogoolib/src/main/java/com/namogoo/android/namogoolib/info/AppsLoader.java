package com.namogoo.android.namogoolib.info;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import com.namogoo.android.namogoolib.pojo.info.AppInfo;
import com.namogoo.android.namogoolib.pojo.info.DeviceInfoReportPojo;

import java.util.ArrayList;
import java.util.List;

public class AppsLoader {
    private ArrayList<AppInfo> deviceSystemApps;
    private ArrayList<AppInfo> deviceUserApps;

    private void init(Context context)
    {
        deviceSystemApps = new ArrayList<>();
        deviceUserApps = new ArrayList<>();

        int flags = PackageManager.GET_META_DATA | PackageManager.GET_SHARED_LIBRARY_FILES;

        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> applications = pm.getInstalledApplications(flags);

        for (ApplicationInfo appInfo : applications) {
            PackageInfo packageInfo = getPackageInfo(pm, appInfo.packageName);
            if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                // System application
                Drawable icon = pm.getApplicationIcon(appInfo);
                deviceSystemApps.add(new AppInfo(1, icon, pm.getApplicationLabel(appInfo).toString(), appInfo.packageName, packageInfo.firstInstallTime, packageInfo.lastUpdateTime, packageInfo.versionName));
            } else {
                // Installed by User
                try {
                    Drawable icon = pm.getApplicationIcon(appInfo);
                    deviceUserApps.add(new AppInfo(2, icon, pm.getApplicationLabel(appInfo).toString(), appInfo.packageName, packageInfo.firstInstallTime, packageInfo.lastUpdateTime, packageInfo.versionName));
                } catch (Exception e) {
                }
            }
        }
    }

    public ArrayList<AppInfo> getAppsList(Context context, boolean userApps) {
        if (deviceSystemApps == null)
        {
            init(context);
        }
        if (userApps) {
            return deviceUserApps;
        }
        return deviceSystemApps;
    }

    public ArrayList<DeviceInfoReportPojo> getAppsListForReport(Context context, boolean userApps) {
        ArrayList<AppInfo> appsList = getAppsList(context, userApps);
        ArrayList<DeviceInfoReportPojo> reportAppList= new ArrayList<>();
        for (AppInfo app : appsList)
        {
            reportAppList.add(new DeviceInfoReportPojo(app.getAppLable(), app.getPackageName(), app.getInstallTime(), app.getLastUpdateTime(), app.getVersionName()));
        }
        return reportAppList;
    }

    private PackageInfo getPackageInfo(PackageManager pm, String packageName) {
        try {
            return pm.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS
                    | PackageManager.GET_ACTIVITIES | PackageManager.GET_RECEIVERS | PackageManager.GET_PROVIDERS
                    | PackageManager.GET_SERVICES | PackageManager.GET_URI_PERMISSION_PATTERNS
                    | PackageManager.GET_SIGNATURES | PackageManager.GET_CONFIGURATIONS);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }
}
