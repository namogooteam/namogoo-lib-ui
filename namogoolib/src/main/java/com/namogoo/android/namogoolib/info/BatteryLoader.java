package com.namogoo.android.namogoolib.info;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import com.namogoo.android.namogoolib.listener.BatteryListener;
import com.namogoo.android.namogoolib.pojo.info.BatteryPojo;

public class BatteryLoader {

    private BatteryPojo batteryPojo = new BatteryPojo();;
    private BatteryListener batteryListener;

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {

            batteryPojo.setDeviceStatus(intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1));

            batteryPojo.setLevel(intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0));
            batteryPojo.setHealth(intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0));
            batteryPojo.setIcon_small(intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL, 0));

            batteryPojo.setPlugged(intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0));
            batteryPojo.setPresent(intent.getExtras().getBoolean(BatteryManager.EXTRA_PRESENT));

            batteryPojo.setScale(intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0));
            batteryPojo.setStatus(intent.getIntExtra(BatteryManager.EXTRA_STATUS, 0));

            batteryPojo.setTechnology(intent.getExtras().getString(BatteryManager.EXTRA_TECHNOLOGY));
            batteryPojo.setTemperature((intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10));

            batteryPojo.setVoltage(intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0));

            try {
                if (batteryListener != null)
                {
                    batteryListener.batteryValueChange(batteryPojo);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    };

    public void registerListener (Context context, BatteryListener batteryListener) {
        this.batteryListener = batteryListener;
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        context.registerReceiver(mBatInfoReceiver, filter);
    }

    public void unregisterReceiver(Context context) {
        try {
            context.unregisterReceiver(mBatInfoReceiver);
        } catch (Exception e) {
        }
        batteryListener = null;
    }
}
