package com.namogoo.android.namogoolib.info;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import com.namogoo.android.namogoolib.pojo.info.BluetoothPojo;

import java.util.ArrayList;

public class BluetoothLoader {

    private BluetoothPojo bluetoothPojo;

    private void init() {
        bluetoothPojo = new BluetoothPojo();
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter != null) {
            bluetoothPojo.setName(bluetoothAdapter.getName());
            bluetoothPojo.setConnected(bluetoothAdapter.isEnabled());
            setPairedDevices();
        }
    }

    public void setPairedDevices()
    {
        ArrayList paired_devices = new ArrayList<>();
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        for (BluetoothDevice device: bluetoothAdapter.getBondedDevices())
        {
            paired_devices.add(device.getName());
        }

        bluetoothPojo.setBluetoothDevices(paired_devices);
    }

    public BluetoothPojo getBluetooth() {
        if (bluetoothPojo == null) {
            init();
        }
        return bluetoothPojo;
    }

    public void update() {
        init();
    }
}
