package com.namogoo.android.namogoolib.info;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;

import com.namogoo.android.namogoolib.helper.Orientation;
import com.namogoo.android.namogoolib.pojo.info.BuildPojo;

import java.util.Locale;
import java.util.TimeZone;

public class BuildLoader {
    private BuildPojo buildPojo;
    private void init(Context context)
    {
        buildPojo = new BuildPojo();
        buildPojo.setManufacturer(Build.MANUFACTURER);
        buildPojo.setBrand(Build.BRAND);
        buildPojo.setModel(Build.MODEL);
        buildPojo.setBoard(Build.BOARD);
        buildPojo.setHardware(Build.HARDWARE);
        buildPojo.setSerial(Build.SERIAL);
        @SuppressLint("HardwareIds") String androidID = (Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        buildPojo.setAndroidID(Build.HARDWARE);
        buildPojo.setBootloader(Build.BOOTLOADER);
        buildPojo.setHost(Build.HOST);
        buildPojo.setUser(Build.USER);
        buildPojo.setDateTimeLocation(TimeZone.getDefault().getID());
        buildPojo.setTextOrient(Orientation.isTextRTL(Locale.getDefault().getLanguage()) ? "Right to Left" : "Left to Right");
        buildPojo.setDefaultLanguage(Locale.getDefault().toString() );
        buildPojo.setLastReboot(System.currentTimeMillis()-SystemClock.elapsedRealtime());
        buildPojo.setPackageName(context.getPackageName());
        try {
            String appPackage = context.getPackageName();
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(appPackage, 0);
            buildPojo.setPackageVersion(pInfo.versionName);
        } catch (Exception e) {
            e.printStackTrace();
            final String unknown = "UNKNOWN";
            buildPojo.setPackageVersion(unknown);
        };
    }

    public BuildPojo getBuild(Context context)
    {
        if (buildPojo == null) {
            init(context);
        }
        return buildPojo;
    }
}
