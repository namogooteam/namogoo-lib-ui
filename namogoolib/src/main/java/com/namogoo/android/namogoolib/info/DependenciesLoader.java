package com.namogoo.android.namogoolib.info;

import android.content.Context;

import com.namogoo.android.namogoolib.pojo.info.DependencyPojo;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import dalvik.system.DexFile;

public class DependenciesLoader {
    private ArrayList<DependencyPojo> dependencies;

    private void init(Context context)
    {
        dependencies = new ArrayList<>();
        Map<String, String> dependenciesMap = new HashMap<>();

        String packageName = context.getPackageName();
        try {
            String packageCodePath = context.getPackageCodePath();
            DexFile df = new DexFile(packageCodePath);
            for (Enumeration<String> iter = df.entries(); iter.hasMoreElements(); ) {
                String className = iter.nextElement();
                if (className.contains(packageName)) {
                    continue;
                }

                String currentPackageName = className.substring(0, className.lastIndexOf("."));

                if (dependencyNeedsToBeAdded(currentPackageName, dependenciesMap))
                {
                    dependenciesMap.put(currentPackageName, null);
                    dependencies.add(new DependencyPojo(currentPackageName, calcVersion(currentPackageName)));
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String calcVersion(String currentPackageName) {
        try {
            Class buildConfig = Class.forName(currentPackageName + ".BuildConfig");
            //public static final String LIBRARY_PACKAGE_NAME = "com.namogoo.android.namogoolib";
            //public static final String APPLICATION_ID = "com.namogoo.android.namogoolib";
            if (currentPackageName.contains("namogoo"))
            {
                int i = 0;
                if (i > 0)
                {
                    i+=1;
                }
            }
            Field packageVersionNameField = buildConfig.getDeclaredField("VERSION_NAME");
            packageVersionNameField.setAccessible(true);
            return packageVersionNameField.get(null).toString();
        } catch (Exception e) {
        }
        return null;
    }

    private boolean dependencyNeedsToBeAdded(String currentPackageName, Map<String, String> dependenciesMap) {
        if (dependenciesMap.containsKey(currentPackageName))
        {
            return false;
        }
        if (currentPackageName.indexOf(".") > 0)
            return dependencyNeedsToBeAdded(currentPackageName.substring(0, currentPackageName.lastIndexOf(".")), dependenciesMap);
        return true;
    }

    public ArrayList<DependencyPojo> getDependencies(Context context) {
        if (dependencies == null)
        {
            init(context);
        }
        return dependencies;
    }
}
