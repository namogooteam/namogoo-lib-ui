package com.namogoo.android.namogoolib.info;

import android.content.Context;

import com.namogoo.android.namogoolib.listener.BatteryListener;
import com.namogoo.android.namogoolib.pojo.info.AppInfo;
import com.namogoo.android.namogoolib.pojo.info.BluetoothPojo;
import com.namogoo.android.namogoolib.pojo.info.BuildPojo;
import com.namogoo.android.namogoolib.pojo.info.DependencyPojo;
import com.namogoo.android.namogoolib.pojo.info.DisplayPojo;
import com.namogoo.android.namogoolib.pojo.info.FeaturesPojo;
import com.namogoo.android.namogoolib.pojo.info.NetworkPojo;
import com.namogoo.android.namogoolib.pojo.info.OSPojo;
import com.namogoo.android.namogoolib.pojo.info.ProcessorPojo;
import com.namogoo.android.namogoolib.pojo.info.SimPojo;
import com.namogoo.android.namogoolib.pojo.info.StoragePojo;

import java.util.ArrayList;

public class DeviceInfoManager {

    private Context mContext;

    private BuildPojo buildPojo;
    private OSPojo osPojo;

    private ProcessorLoader processorLoader;
    private BatteryLoader batteryLoader;
    private AppsLoader appsLoader;
    private NetworkLoader networkLoader;
    private SimLoader simLoader;
    private StorageLoader storageLoader;
    private BluetoothLoader bluetoothLoader;
    private DisplayLoader displayLoader;
    private FeaturesLoader featuresLoader;
    private DependenciesLoader dependenciesLoader;

    public DeviceInfoManager(Context context) {
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        getBuild();
        getOS();
        getProcessor();
        getNetwork();
        getStorage(true);
        getBluetooth();
        getDisplay();
        getFeatures();
    }

    public BuildPojo getBuild()
    {
        if (buildPojo == null)
        {
            BuildLoader buildLoader = new BuildLoader();
            buildPojo = buildLoader.getBuild(mContext);
        }
        return buildPojo;
    }

    public OSPojo getOS()
    {
        if (osPojo == null)
        {
            OSLoader osLoader = new OSLoader(mContext);
            osPojo = osLoader.getOS(mContext);
        }
        return osPojo;
    }

    public ProcessorPojo getProcessor()
    {
        ProcessorPojo processorPojo;
        if (processorLoader == null)
        {
            processorLoader = new ProcessorLoader();
            processorPojo = processorLoader.getProcessorPojo(mContext);
        } else {
            processorPojo = processorLoader.getProcessorPojo(mContext);
        }
        return processorPojo;
    }

    public void registerBatteryListener(BatteryListener batteryListener)
    {
        if (batteryLoader == null)
        {
            batteryLoader= new BatteryLoader();
        }
        batteryLoader.registerListener(mContext, batteryListener);
    }

    public void unregisterBatteryListener()
    {
        if (batteryLoader != null) {
            batteryLoader.unregisterReceiver(mContext);
        }
    }

    public ArrayList<AppInfo> getApps(boolean userApps)
    {
        if (appsLoader == null)
        {
            appsLoader = new AppsLoader();
        }
        return appsLoader.getAppsList(mContext, userApps);
    }

    public NetworkPojo getNetwork()
    {
        if (networkLoader == null)
        {
            networkLoader = new NetworkLoader(mContext);
        }
        return networkLoader.getNetwork();
    }

    public SimPojo getSim()
    {
        if (simLoader == null)
        {
            simLoader = new SimLoader();
        }
        return simLoader.getSim(mContext);
    }

    public StoragePojo getStorage(boolean internal)
    {
        if (storageLoader == null)
        {
            storageLoader = new StorageLoader();
            storageLoader.getExternalStorage(mContext);
            storageLoader.getInternalStorage();

        }
        return internal ? storageLoader.getInternalStorage() : storageLoader.getExternalStorage(mContext);
    }

    public BluetoothPojo getBluetooth()
    {
        if (bluetoothLoader == null) {
            bluetoothLoader = new BluetoothLoader();
        } else {
            bluetoothLoader.update();
        }
        return bluetoothLoader.getBluetooth();
    }

    public DisplayPojo getDisplay()
    {
        if (displayLoader == null)
        {
            displayLoader = new DisplayLoader();
        }
        return displayLoader.getDisplay(mContext);
    }

    public FeaturesPojo getFeatures()
    {
        if (featuresLoader == null)
        {
            featuresLoader = new FeaturesLoader();
        }
        return featuresLoader.getFeatures(mContext);
    }

    public ArrayList<DependencyPojo> getDependencies()
    {
        if (dependenciesLoader == null)
        {
            dependenciesLoader = new DependenciesLoader();
        }
        return dependenciesLoader.getDependencies(mContext);
    }
}
