package com.namogoo.android.namogoolib.info;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.namogoo.android.namogoolib.R;
import com.namogoo.android.namogoolib.pojo.info.DisplayPojo;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;

public class DisplayLoader {
    private DisplayPojo displayPojo;

    private void init(Context context)
    {
        displayPojo = new DisplayPojo();

        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = context.getResources().getDisplayMetrics();

        /*** Screen Size */
        String screenSize;
        switch (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
        {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                screenSize = "Large screen";
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                screenSize = "Normal screen";
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                screenSize = "Small screen";
                break;
            default:
                screenSize = "NA";
    }
        displayPojo.setScreenSize(screenSize);

        /*** Screen physical size */
        Point realSize = new Point();
        try {
            Display.class.getMethod("getRealSize", Point.class).invoke(display, realSize);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        int pWidth = realSize.x;
        int pHeight = realSize.y;
        float wi = pWidth / dm.xdpi;
        float hi = pHeight / dm.ydpi;
        double x = Math.pow(wi, 2.0);
        double y = Math.pow(hi, 2.0);
        double screenInches = Math.sqrt(x + y);
        displayPojo.setPhysicalSize(returnTwoDecimalPlaces(screenInches));

        /*** Screen default orientation */
        int orientation;
        switch (context.getResources().getConfiguration().orientation)
        {
            case Configuration.ORIENTATION_PORTRAIT:
                orientation = R.string.nmg_orientation_portrait;
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                orientation = R.string.nmg_orientation_landscape;
                break;
            default:
                orientation = R.string.nmg_orientation_undefined;
        }
        displayPojo.setScreenDefaultOrientation(context.getResources().getString(orientation));

        /*** Display screen width and height */
        displayPojo.setScreenTotalWidth(pWidth);
        displayPojo.setScreenTotalHeight(pHeight);

        /*** Screen refresh rate */
        displayPojo.setRefreshRate((int)display.getRefreshRate());

        /*** Display name */
        displayPojo.setScreenName(display.getName());

        /*** Screen Display buckets */
        displayPojo.setScreenDensityBucket(context.getResources().getString(R.string.nmg_screen_display_bucket));

        /*** Screen Dpi */
        displayPojo.setScreenDPI(dm.densityDpi);

        /*** Screen logical density */
        displayPojo.setScreenLogicalDensity(dm.density);

        /*** Screen scaled density */
        displayPojo.setScreenScaledDensity(dm.scaledDensity);

        /*** Screen xDpi and yDpi */
        displayPojo.setxDpi((int)dm.xdpi);
        displayPojo.setyDpi((int)dm.ydpi);


        /*** Screen usable width and height */
        Point size = new Point();
        display.getSize(size);
        displayPojo.setScreenUsableWidth(size.x);
        displayPojo.setScreenUsableHeight(size.y);

    }

    private Double returnTwoDecimalPlaces(Double values){
        //DecimalFormat df = new DecimalFormat("#.00");
        //return df.format(values);
        return Math.round(values * 100.0) / 100.0;
    }

    public DisplayPojo getDisplay(Context context)
    {
        if (displayPojo == null)
        {
            init(context);
        }
        return displayPojo;
    }
}
