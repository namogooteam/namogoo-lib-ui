package com.namogoo.android.namogoolib.info;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.namogoo.android.namogoolib.pojo.info.FeaturesPojo;

public class FeaturesLoader {

    FeaturesPojo featuresPojo;

    private void init(Context context)
    {
        featuresPojo = new FeaturesPojo();
        PackageManager packageManager = context.getPackageManager();

        /** WIFI feature */
        ConnectivityManager connManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        // WIFI
        featuresPojo.setWifi(mWifi.isAvailable());

        // WIFI Direct
        featuresPojo.setWifiDirect(packageManager.hasSystemFeature(PackageManager.FEATURE_WIFI_DIRECT));

        // Bluetooth
        featuresPojo.setBluetooth(packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH));

        // Bluetooth LE
        featuresPojo.setBluetoothLE(packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE));

        // GPS
        featuresPojo.setGps(packageManager.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS));

        // Camera Flash
        featuresPojo.setCameraFlash(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH));

        // Camera Front
        featuresPojo.setCameraFront(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT));

        // Microphone
        featuresPojo.setMicrophone(packageManager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE));

        // NFC
        featuresPojo.setNfc(packageManager.hasSystemFeature(PackageManager.FEATURE_NFC));

        // USB Host
        featuresPojo.setUsbHost(packageManager.hasSystemFeature(PackageManager.FEATURE_USB_HOST));

        // USB Accessory
        featuresPojo.setUsbAccessory(packageManager.hasSystemFeature(PackageManager.FEATURE_USB_ACCESSORY));

        // Multitouch
        featuresPojo.setMultitouch(packageManager.hasSystemFeature(PackageManager.FEATURE_TOUCHSCREEN_MULTITOUCH));

        // Audio low-latency
        featuresPojo.setAudioLowLatency(packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_LOW_LATENCY));

        // Audio Output
        featuresPojo.setAudioOutput(packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_OUTPUT));

        // Professional Audio
        featuresPojo.setProfessionalAudio(packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_PRO));

        // Consumer IR
        featuresPojo.setConsumerIR(packageManager.hasSystemFeature(PackageManager.FEATURE_CONSUMER_IR));

        // Gamepad Support
        featuresPojo.setGamepadSupport(packageManager.hasSystemFeature(PackageManager.FEATURE_GAMEPAD));

        // HIFI Sensor
        featuresPojo.setHifiSensor(packageManager.hasSystemFeature(PackageManager.FEATURE_HIFI_SENSORS));

        // Printing
        featuresPojo.setPrinting(packageManager.hasSystemFeature(PackageManager.FEATURE_PRINTING));

        // CDMA
        featuresPojo.setCdma(packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_CDMA));

        // GSM
        featuresPojo.setGsm(packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_GSM));

        // Finger-print
        featuresPojo.setFingerprint(packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT));

        // App Widgets
        featuresPojo.setAppWidgets(packageManager.hasSystemFeature(PackageManager.FEATURE_APP_WIDGETS));

        // SIP
        featuresPojo.setIp(packageManager.hasSystemFeature(PackageManager.FEATURE_SIP));

        // SIP based VOIP
        featuresPojo.setIpBasedVOIP(packageManager.hasSystemFeature(PackageManager.FEATURE_SIP_VOIP));

    }

    public FeaturesPojo getFeatures(Context context)
    {
        if (featuresPojo == null)
        {
            init(context);
        }
        return featuresPojo;
    }
}
