package com.namogoo.android.namogoolib.info;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.namogoo.android.namogoolib.pojo.info.NetworkPojo;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;

import static android.content.Context.WIFI_SERVICE;

public class NetworkLoader {

    private ConnectivityManager cm;
    private WifiManager wifiManager;
    private NetworkPojo networkPojo;

    public NetworkLoader(Context context) {
        networkPojo = new NetworkPojo();
        cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
    }

    private void updateNetworkData()
    {
        try {
            if (cm == null) {
                markAsDisconnected();
                return;
            }
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork == null) {
                markAsDisconnected();
                return;
            }
            networkPojo.setIpAddress(getIPAddress(true));
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                networkPojo.setNetworkStatus(NetworkPojo.NetworkStatus.WIFI);
                if (wifiManager != null) {
                    networkPojo.setLinkSpeed(wifiManager.getConnectionInfo().getLinkSpeed());
                }
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                networkPojo.setNetworkStatus(NetworkPojo.NetworkStatus.NETWORK);
            }
            getConnectionVPN();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public void getConnectionVPN() {
        this.networkPojo.setVpnConnection(isConnectionVPN());
    }

    public boolean isConnectionVPN()
    {
        String iface = "";
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.isUp())
                    iface = networkInterface.getName();
                if ( iface.contains("tun") || iface.contains("ppp") || iface.contains("pptp")) {
                    return true;
                }
            }
        } catch (SocketException e1) {
            e1.printStackTrace();
        }

        return false;
    }

    public NetworkPojo getNetwork() {
        updateNetworkData();
        return networkPojo;

    }

    private void markAsDisconnected() {
        networkPojo.setIpAddress(null);
        networkPojo.setLinkSpeed(0);
        networkPojo.setNetworkStatus(NetworkPojo.NetworkStatus.DISCONNECTED);
    }
}
