package com.namogoo.android.namogoolib.info;

import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;

import com.namogoo.android.namogoolib.helper.Utils;
import com.namogoo.android.namogoolib.pojo.info.OSPojo;

public class OSLoader {
    private OSPojo osPojo;

    public OSLoader(Context context) {
        init(context);
    }

    private void init(Context context)
    {
        osPojo = new OSPojo();
        int apiLevel = Build.VERSION.SDK_INT;
        osPojo.setApiLevel(apiLevel);
        osPojo.setVersion(Build.VERSION.RELEASE);
        osPojo.setId(Build.ID);
        osPojo.setBuildTime(Utils.getDateString(Build.TIME));
        osPojo.setFingerPrint(Build.FINGERPRINT);
        osPojo.setBrightness(getBrightness(context));
        try {
            osPojo.setSdkName(Build.VERSION_CODES.class.getFields()[Build.VERSION.SDK_INT].getName());
        } catch (Exception e) {
            e.printStackTrace();
            osPojo.setSdkName("");
        }
        osPojo.setLastReboot(getLastReboot());
    }

    public int getBrightness(Context context) {
        return Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS,-1);
    }

    public OSPojo getOS(Context context)
    {
        if (osPojo == null)
        {
            init(context);
        }
        return osPojo;
    }

    public long getLastReboot() {
        return System.currentTimeMillis() - SystemClock.elapsedRealtime();
    }
}
