package com.namogoo.android.namogoolib.info;

import android.app.ActivityManager;
import android.content.Context;

import com.namogoo.android.namogoolib.R;
import com.namogoo.android.namogoolib.pojo.info.FeaturesHW;
import com.namogoo.android.namogoolib.pojo.info.ProcessorPojo;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class ProcessorLoader {

    private ProcessorPojo processorPojo;

    private void init(Context context)
    {
        processorPojo = new ProcessorPojo();

        ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);

        processorPojo.setTotalMemory(memoryInfo.totalMem);
        processorPojo.setFreeMemory(memoryInfo.availMem);
        String processor = context.getString(R.string.nmg_processor);
        processorPojo.setTotalCPUs(getCpuCount(processor));
    }

    private int getCpuCount(String processor) {
        ArrayList<FeaturesHW> lists = new ArrayList<>();
        int totalCPUs = 0;
        try {
            Scanner s = new Scanner(new File("/proc/cpuinfo"));
            while (s.hasNextLine()) {
                String[] vals = s.nextLine().split(": ");
                if (vals.length > 1) {
                    FeaturesHW feature = new FeaturesHW(vals[0].trim(), vals[1].trim());
                    lists.add(feature);
                    if (feature.getFeatureLable().equals(processor))
                        totalCPUs += 1;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return totalCPUs;
    }

    public ProcessorPojo getProcessorPojo(Context context)
    {
        if (processorPojo == null) {
            init(context);
        }
        return processorPojo;
    }
}
