package com.namogoo.android.namogoolib.info;

import com.namogoo.android.namogoolib.NMGLibrary;
import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent;
import com.namogoo.android.namogoolib.pojo.info.AppInfo;
import com.namogoo.android.namogoolib.pojo.info.OSPojo;
import com.namogoo.android.namogoolib.pojo.info.SegmentationPojo;

import java.util.ArrayList;


public class SegmentationLoader {
    SegmentationPojo segmentationPojo;

    private void initSegmentationIfNeeded()
    {
        if (segmentationPojo == null)
        {
            segmentationPojo = new SegmentationPojo();
        }
    }

    public SegmentationPojo getSegmentation(boolean recalc) {
        initSegmentationIfNeeded();
        if (recalc) {
            calcSegmentation();
        }
        return segmentationPojo;
    }

    private void calcSegmentation() {
        ArrayList<AppInfo> userApps = NMGLibrary.getInstance().getDeviceInfo().getApps(true);
        ArrayList<String> userAppsPackageNames = new ArrayList<>();
        for(AppInfo app : userApps)
        {
            userAppsPackageNames.add(app.getPackageName());
        }
        OSPojo os = NMGLibrary.getInstance().getDeviceInfo().getOS();

        boolean isTechSavvy = (os.getApiLevel() > 28);
        segmentationPojo.addSegment(SegmentationPojo.Segment.TECH_SAVVY, isTechSavvy);

        boolean isPrivacySensitive = userAppsPackageNames.contains("com.ghostery.android.ghostery") || userAppsPackageNames.contains("org.torproject.torbrowser") || userAppsPackageNames.contains("com.enchantedcloud.photovault");
        segmentationPojo.addSegment(SegmentationPojo.Segment.PRIVACY_SENSITIVE, isPrivacySensitive);

        boolean isPriceSensitive = userAppsPackageNames.contains("com.coupons.forwayfair");
        if (!isPriceSensitive)
        {
            /*ArrayList<CustomerJourneyEvent> customerJourneyEventPojos = NMGLibrary.getInstance().getCustomerJourney();
            for (CustomerJourneyEvent event : customerJourneyEventPojos) {
                isPriceSensitive = isPriceSensitiveEvent(event);
                if (isPriceSensitive)
                {
                    break;
                }
            }*/

        }
        segmentationPojo.addSegment(SegmentationPojo.Segment.PRICE_SENSITIVE, isPriceSensitive);

        boolean isNamogooGroopie = userAppsPackageNames.contains("com.asos.app") && userAppsPackageNames.contains("com.tigerspike.newlook");
        segmentationPojo.addSegment(SegmentationPojo.Segment.NAMOGOO_GROUPIE, isNamogooGroopie);

    }


    private boolean isPriceSensitiveEvent(CustomerJourneyEvent event)
    {
        return event.userInteractionEventData == null ? false : event.userInteractionEventData.getTargetId().contains("Sort by Price");
    }

    public boolean addSegmentByEvent(CustomerJourneyEvent event)
    {
        initSegmentationIfNeeded();
        if (segmentationPojo.getSegments().contains(SegmentationPojo.Segment.PRICE_SENSITIVE))
        {
            return false;
        }
        if (isPriceSensitiveEvent(event)) {
            segmentationPojo.addSegment(SegmentationPojo.Segment.PRICE_SENSITIVE, true);
            return true;
        }
        return false;
    }
}
