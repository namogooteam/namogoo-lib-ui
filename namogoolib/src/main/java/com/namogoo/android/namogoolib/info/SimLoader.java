package com.namogoo.android.namogoolib.info;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import androidx.annotation.RequiresApi;

import com.namogoo.android.namogoolib.pojo.info.SimPojo;

public class SimLoader {
    private SimPojo simPojo;

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void init(Context context) {
        simPojo = new SimPojo();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null && isSimAvailable(context, 0) && telephonyManager.getSimState() == TelephonyManager.SIM_STATE_READY) {
            simPojo.setState(simState(telephonyManager.getSimState()));
            simPojo.setCountryIso(telephonyManager.getNetworkCountryIso());
            simPojo.setOperatorName(telephonyManager.getNetworkOperatorName());
            //if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                //return;
            //}
            //simPojo.setNetworkType(networkType(telephonyManager.getDataNetworkType()));
        }
    }

    private String simState(int simState) {
        switch (simState){
            case 0:
                return "UNKNOWN";
            case 1:
                return "ABSENT";
            case 2:
                return "REQUIRED";
            case 3:
                return "PUK_REQUIRED";
            case 4:
                return "NETWORK_LOCKED";
            case 5:
                return "READY";
            case 6:
                return "NOT_READY";
            case 7:
                return "PERM_DISABLED";
            case 8:
                return "CARD_IO_ERROR";
            default:
                return String.valueOf(simState);
        }
    }

    private String networkType(int networkType) {
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS";
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE";
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CDMA";
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "1xRTT";
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "IDEN";

            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "EVDO 0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "EVDO A";
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA";
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return "EVDO B";
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return "EHRPD";
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "HSPAP";

            case TelephonyManager.NETWORK_TYPE_LTE:
                return "LTE";
            case TelephonyManager.NETWORK_TYPE_GSM:
                return "GSM";
            case TelephonyManager.NETWORK_TYPE_TD_SCDMA:
                return "TD_SCDMA";
            case TelephonyManager.NETWORK_TYPE_IWLAN:
                return "IWLAN";
            case TelephonyManager.NETWORK_TYPE_NR:
                return "5G - New Radio";
            default:
                return String.valueOf(networkType);
        }
    }

    @SuppressLint("MissingPermission")
    private boolean isSimAvailable(Context context, int slotId) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
        SubscriptionManager sManager = (SubscriptionManager)context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
        SubscriptionInfo infoSim = sManager.getActiveSubscriptionInfoForSimSlotIndex(slotId);
        if (infoSim != null) {
            return true;
        }
    } else {
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getSimSerialNumber() != null) {
            return true;
        }
    }
    return false;
}

    public SimPojo getSim(Context context)
    {
        if (checkPermission(context) == false)
        {
            return null;
        }

        if (simPojo == null)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                init(context);
            }
        }
        return simPojo;
    }

    private boolean checkPermission(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasPermission = context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            return (hasPermission == PackageManager.PERMISSION_GRANTED);
        } else {
            return true;
        }
    }
}
