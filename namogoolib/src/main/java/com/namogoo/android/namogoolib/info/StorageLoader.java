package com.namogoo.android.namogoolib.info;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import com.namogoo.android.namogoolib.pojo.info.StoragePojo;

import java.io.File;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Locale;

public class StorageLoader {
    private StoragePojo internalStorage;
    private StoragePojo externalStorage;

    private void initExternalStorage(Context context)
    {
        if ( Build.VERSION.SDK_INT >= 19) {
            externalStorage = new StoragePojo();
            File[] dirs = context.getExternalFilesDirs( null);
            externalStorage.setAvailable(getAvailableExternalMemorySize(dirs));
            externalStorage.setTotal(getTotalExternalMemorySize(dirs));
            if (externalStorage.getTotal() == 0)
            {
                externalStorage = null;
            }
        }
    }

    private void initInternalStorage()
    {
        internalStorage = new StoragePojo();
        internalStorage.setAvailable(getAvailableInternalMemorySize());
        internalStorage.setTotal(getTotalInternalMemorySize());
    }

    public StoragePojo getExternalStorage(Context context) {
        if (externalStorage == null)
        {
            initExternalStorage(context);
        }
        return externalStorage;
    }

    public StoragePojo getInternalStorage() {
        if (internalStorage == null)
        {
            initInternalStorage();
        }
        return internalStorage;
    }

    private long getAvailableInternalMemorySize(){
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return availableBlocks * blockSize;
    }

    private long getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long totalBlocks = stat.getBlockCountLong();
        return totalBlocks * blockSize;
    }

    private long getTotalExternalMemorySize(File[] dirs){
        if (dirs.length > 1) {
            try {
                StatFs stat = new StatFs(dirs[1].getPath());
                long blockSize = stat.getBlockSizeLong();
                long totalBlocks = stat.getBlockCountLong();
                return totalBlocks * blockSize;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            return 0;
        }
    }

    private long getAvailableExternalMemorySize(File[] dirs) {
        if (dirs.length > 1) {
            try {
                StatFs stat = new StatFs(dirs[1].getPath());
                long blockSize = stat.getBlockSizeLong();
                long availableBlocks = stat.getAvailableBlocksLong();
                return availableBlocks * blockSize;
            }
            catch (Exception e) {
                Log.e("ERROR", e.toString());
                return 0;
            }
        } else {
            return 0;
        }
    }

    private HashSet<String> getExternalMounts() {
        final HashSet<String> out = new HashSet<String>();
        String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
        String s = "";
        try {
            final Process process = new ProcessBuilder().command("mount")
                    .redirectErrorStream(true).start();
            process.waitFor();
            final InputStream is = process.getInputStream();
            final byte[] buffer = new byte[1024];
            while (is.read(buffer) != -1) {
                s = s + new String(buffer);
            }
            is.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        // parse output
        final String[] lines = s.split("\n");
        for (String line : lines) {
            if (!line.toLowerCase(Locale.US).contains("asec")) {
                if (line.matches(reg)) {
                    String[] parts = line.split(" ");
                    for (String part : parts) {
                        if (part.startsWith("/"))
                            if (!part.toLowerCase(Locale.US).contains("vold"))
                                out.add(part);
                    }
                }
            }
        }
        return out;
    }
}
