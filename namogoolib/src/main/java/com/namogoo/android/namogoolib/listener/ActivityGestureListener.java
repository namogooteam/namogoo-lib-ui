package com.namogoo.android.namogoolib.listener;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

import java.util.ArrayList;

public class ActivityGestureListener implements GestureDetector.OnGestureListener {
    private static final String DEBUG_TAG = "NMG-Gestures";
    private String currentView;
    private ArrayList<String> currentViewHierarchy;
    //private CustomerJourneyPojo customerJourneyPojo;

    /*public ActivityGestureListener(CustomerJourneyPojo customerJourneyPojo) {
        this.customerJourneyPojo = customerJourneyPojo;
    }

     */

    @Override
    public boolean onDown(MotionEvent event) {
        //Log.d(DEBUG_TAG,"onDown: " + event.toString());
        //customerJourneyPojo.addEvent(CustomerJourneyEventPojo.EventType.TOUCH_DOWN, currentViewHierarchy, "DOWN-"+currentView, null);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
        //Log.d(DEBUG_TAG,"onShowPress: " + event.toString());
        //customerJourneyPojo.addEvent(CustomerJourneyEventPojo.EventType.TOUCH_DOWN, currentViewHierarchy, "SHOWPRESS-"+currentView, null);
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        Log.d(DEBUG_TAG,"onSingleTapUp: " + event.toString());
        //customerJourneyPojo.addEvent(CustomerJourneyEventPojo.EventType.CLICKED, currentViewHierarchy, "SINGLETAP-"+currentView, null);
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY) {
        //Log.d(DEBUG_TAG, "onScroll: " + event1.toString() +", " + event2.toString());
        return false;
    }

    @Override
    public void onLongPress(MotionEvent event) {
        //customerJourneyPojo.addEvent(CustomerJourneyEventPojo.EventType.LONG_PRESS, currentViewHierarchy, "LONG_PRESS-"+currentView, null);

        Log.d(DEBUG_TAG,"onLongPress: " + event.toString());
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
        //Log.d(DEBUG_TAG, "onFling: " + event1.toString() +", " + event2.toString());
        return false;
    }

    public void setView(ArrayList<String> currentViewHierarchy, String viewId) {
        this.currentViewHierarchy = currentViewHierarchy;
        currentView = viewId;
    }
}
