package com.namogoo.android.namogoolib.listener;

import com.namogoo.android.namogoolib.pojo.info.BatteryPojo;

public abstract class BatteryListener {
    public abstract void batteryValueChange(BatteryPojo batteryPojo);
}
