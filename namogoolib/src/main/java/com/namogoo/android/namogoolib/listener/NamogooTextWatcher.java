package com.namogoo.android.namogoolib.listener;

import android.text.Editable;
import android.text.TextWatcher;

import java.util.ArrayList;

public class NamogooTextWatcher implements TextWatcher {

    private static String TAG = "NMG-TextWatcher";
    private String currentView;
    //private CustomerJourneyPojo customerJourneyPojo;
    private ArrayList<String> hierarchy;

    public NamogooTextWatcher(/*CustomerJourneyPojo customerJourneyPojo,*/ ArrayList<String> hierarchy, String currentView) {
        //this.customerJourneyPojo = customerJourneyPojo;
        this.currentView = currentView;
        this.hierarchy = hierarchy;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //customerJourneyPojo.addEvent(CustomerJourneyEventPojo.EventType.TEXT_CHANGE, hierarchy, currentView, s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
