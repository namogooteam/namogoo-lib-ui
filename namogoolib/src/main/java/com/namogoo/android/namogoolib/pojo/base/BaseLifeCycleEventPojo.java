package com.namogoo.android.namogoolib.pojo.base;

import com.namogoo.android.namogoolib.pojo.enums.EventType;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class BaseLifeCycleEventPojo extends BasePojo {


    private long eventTime;
    private EventType eventType;
    private String targetId;
    private String sessionId;
    private int pageView;
    private String ruleName;

    public BaseLifeCycleEventPojo(EventType eventType, String targetId, String sessionId, int pageView, String ruleName) {
        this.eventType = eventType;
        this.targetId = targetId;
        this.sessionId = sessionId;
        this.pageView = pageView;
        this.ruleName = ruleName;

        try {
            this.eventTime = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
        } catch (Exception e) {
            e.printStackTrace();
            this.eventTime = System.currentTimeMillis();
        }
    }

    public long getEventTime() {
        return eventTime;
    }

    public void setEventTime(long eventTime) {
        this.eventTime = eventTime;
    }

    public EventType getEventType() {
        return eventType;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public int getPageView() {
        return pageView;
    }

    public String getRuleName() {
        return ruleName;
    }
}
