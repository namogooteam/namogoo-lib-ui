package com.namogoo.android.namogoolib.pojo.enums;

public enum ServiceRequest {
    INIT_JOURNEY_SESSION,
    SEND_JOURNEY_DATA,
    CONFIG_REQUEST
}
