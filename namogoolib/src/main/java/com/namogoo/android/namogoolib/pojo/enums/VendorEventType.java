package com.namogoo.android.namogoolib.pojo.enums;

public enum VendorEventType {
    CLICK_LOG_IN,
    CLICK_LOG_OUT,
    CLICK_CREATE_ACCOUNT,
    CLICK_ADD_TO_CART,
    CLICK_REMOVE_FROM_CART,
    CLICK_SUBSCRIBE,
    CLICK_UNSUBSCRIBE,
    CLICK_START_REVIEW,
    CLICK_SUBMIT_REVIEW,
    CLICK_CHECK_OUT,
    CLICK_CONTINUE_AS_GUEST,
    PAID_WITH_SPECIFIC_PAYMENT_PROVIDER,
    UPDATED_ACCOUNT,
    ENGAGED_WITH_LIVE_CHAT,
    SCREEN_ENTERED,
    SCREEN_EXIT,
}
