package com.namogoo.android.namogoolib.pojo.events;

import com.namogoo.android.namogoolib.helper.Utils;
import com.namogoo.android.namogoolib.pojo.base.BaseLifeCycleEventPojo;
import com.namogoo.android.namogoolib.pojo.enums.EventType;

import java.util.HashMap;
import java.util.Map;

public class LifeCycleEventPojo extends BaseLifeCycleEventPojo {

    private HashMap<String, String> intentData;

    public LifeCycleEventPojo(EventType eventType, String targetId, String sessionId, int pageView, String ruleName) {
        this(eventType, targetId, sessionId, pageView, ruleName, null);
    }

    public LifeCycleEventPojo(EventType eventType, String targetId, String sessionId, int pageView, String ruleName, HashMap<String, String> intentData) {
        super(eventType, targetId, sessionId, pageView, ruleName);
        this.intentData = intentData;
    }

    public HashMap<String, String> getIntentData() {
        return intentData;
    }

    public void setIntentData(HashMap<String, String> intentData) {
        this.intentData = intentData;
    }

    public LifeCycleEventPojo mutate() {
        LifeCycleEventPojo mutant = new LifeCycleEventPojo(getEventType(), getTargetId(), getSessionId(), getPageView(), getRuleName(), intentData);
        mutant.setEventTime(getEventTime());
        return mutant;
    }
}
