package com.namogoo.android.namogoolib.pojo.events;

import com.namogoo.android.namogoolib.helper.Utils;
import com.namogoo.android.namogoolib.pojo.base.BaseLifeCycleEventPojo;
import com.namogoo.android.namogoolib.pojo.enums.EventType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Keep;

@Keep
public class UserInteractionEventPojo extends BaseLifeCycleEventPojo {

    ArrayList<String> targetHierarchy;
    String value;

    public UserInteractionEventPojo(EventType eventType, String targetId, String sessionId, int pageView, String ruleName)
    {
        this(eventType, targetId, sessionId, pageView, ruleName, null, null);

    }

    public UserInteractionEventPojo(EventType eventType, String targetId, String sessionId, int pageView, String ruleName, ArrayList<String> targetHierarchy, String value) {
        super(eventType, targetId, sessionId, pageView, ruleName);
        this.targetHierarchy = targetHierarchy;
        this.value = value;
    }

    public ArrayList<String> getTargetHierarchy() {
        return targetHierarchy;
    }

    public String getValue() {
        return value;
    }

    public void setTargetHierarchy(ArrayList<String> targetHierarchy) {
        this.targetHierarchy = targetHierarchy;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Object convertToReportObject()
    {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put("time", Utils.getDateStringSeconds(getEventTime()) + " ::: " + getEventTime());
        retVal.put("eventType", getEventType());
        retVal.put("targetHierarchy", targetHierarchy);
        retVal.put("targetObjectId", getTargetId());
        retVal.put("value", value);
        return retVal;
    }

    public UserInteractionEventPojo mutate() {
        UserInteractionEventPojo mutant = new UserInteractionEventPojo(getEventType(), getTargetId(), getSessionId(), getPageView(), getRuleName(), targetHierarchy, value);
        mutant.setEventTime(getEventTime());
        return mutant;
    }
}
