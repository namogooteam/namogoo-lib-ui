package com.namogoo.android.namogoolib.pojo.info

import android.graphics.drawable.Drawable
import androidx.annotation.Keep

/**
 * Created by Udi
 */

@Keep
class AppInfo (var flags: Int, var appLogo: Drawable, var appLable: String, var packageName: String, var installTime: Long, var lastUpdateTime:Long, var versionName:String)
