package com.namogoo.android.namogoolib.pojo.info;
import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

import java.util.ArrayList;

@Keep
public class BluetoothPojo extends BasePojo {
    private boolean connected;
    private String name;
    private ArrayList<String> bluetoothDevices;

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getBluetoothDevices() {
        return bluetoothDevices;
    }

    public void setBluetoothDevices(ArrayList<String> bluetoothDevices) {
        this.bluetoothDevices = bluetoothDevices;
    }
}
