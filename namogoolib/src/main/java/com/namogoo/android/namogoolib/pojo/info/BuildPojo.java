package com.namogoo.android.namogoolib.pojo.info;

import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

@Keep
public class BuildPojo extends BasePojo {
    private String manufacturer;
    private String brand;
    private String model;
    private String board;
    private String hardware;
    private String serial;
    private String androidID;
    private String bootloader;
    private String host;
    private String packageVersion;
    private String user;
    private String dateTimeLocation;
    private String defaultLanguage;
    private String textOrient;
    private long lastReboot;
    private String packageName;


    public String getManufacturer() {
        return manufacturer;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getBoard() {
        return board;
    }

    public String getHardware() {
        return hardware;
    }

    public String getSerial() {
        return serial;
    }

    public String getAndroidID() {
        return androidID;
    }

    public String getBootloader() {
        return bootloader;
    }

    public String getHost() {
        return host;
    }

    public String getUser() {
        return user;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setAndroidID(String androidID) {
        this.androidID = androidID;
    }

    public void setBootloader(String bootloader) {
        this.bootloader = bootloader;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDateTimeLocation() {
        return dateTimeLocation;
    }

    public void setDateTimeLocation(String dateTimeLocation) {
        this.dateTimeLocation = dateTimeLocation;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public long getLastReboot() {
        return lastReboot;
    }

    public void setLastReboot(long lastReboot) {
        this.lastReboot = lastReboot;
    }

    public String getTextOrient() {
        return textOrient;
    }

    public void setTextOrient(String textOrient) {
        this.textOrient = textOrient;
    }

    public String getPackageVersion() {
        return packageVersion;
    }

    public void setPackageVersion(String packageVersion) {
        this.packageVersion = packageVersion;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }
}
