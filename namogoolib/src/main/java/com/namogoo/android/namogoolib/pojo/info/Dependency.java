package com.namogoo.android.namogoolib.pojo.info;

import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

@Keep
public class Dependency extends BasePojo {

    String packageName;
    String version;

    public Dependency(String packageName, String version) {
        this.packageName = packageName;
        this.version = version;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getVersion() {
        return version;
    }
}
