package com.namogoo.android.namogoolib.pojo.info;

import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

@Keep
public class DependencyPojo extends BasePojo {
    String name;
    String version;

    public DependencyPojo(String name, String version) {
        this.name = name;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }
}
