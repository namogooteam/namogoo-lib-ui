package com.namogoo.android.namogoolib.pojo.info;

import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

@Keep
public class DeviceInfoReportPojo extends BasePojo {

    String appLable;
    String packageName;
    Long installTime;
    Long lastUpdateTime;
    String versionName;

    public DeviceInfoReportPojo(String appLable, String packageName, Long installTime, Long lastUpdateTime, String versionName) {
        this.appLable = appLable;
        this.packageName = packageName;
        this.installTime = installTime;
        this.lastUpdateTime = lastUpdateTime;
        this.versionName = versionName;
    }

    public String getAppLable() {
        return appLable;
    }

    public String getPackageName() {
        return packageName;
    }

    public Long getInstallTime() {
        return installTime;
    }

    public Long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public String getVersionName() {
        return versionName;
    }
}
