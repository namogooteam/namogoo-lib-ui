package com.namogoo.android.namogoolib.pojo.info;

import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

@Keep
public class DisplayPojo extends BasePojo {

    private String screenSize;
    private Double physicalSize;
    private int refreshRate;
    private int xDpi;
    private int yDpi;
    private String screenDensityBucket;
    private String screenName;
    private int screenDPI;
    private float screenLogicalDensity;
    private float screenScaledDensity;
    private int screenUsableWidth;
    private int screenUsableHeight;
    private int screenTotalWidth;
    private int screenTotalHeight;
    private String screenDefaultOrientation;

    public String getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(String screenSize) {
        this.screenSize = screenSize;
    }

    public Double getPhysicalSize() {
        return physicalSize;
    }

    public void setPhysicalSize(Double physicalSize) {
        this.physicalSize = physicalSize;
    }

    public int getRefreshRate() {
        return refreshRate;
    }

    public void setRefreshRate(int refreshRate) {
        this.refreshRate = refreshRate;
    }

    public int getxDpi() {
        return xDpi;
    }

    public void setxDpi(int xDpi) {
        this.xDpi = xDpi;
    }

    public int getyDpi() {
        return yDpi;
    }

    public void setyDpi(int yDpi) {
        this.yDpi = yDpi;
    }

    public String getScreenDensityBucket() {
        return screenDensityBucket;
    }

    public void setScreenDensityBucket(String screenDensityBucket) {
        this.screenDensityBucket = screenDensityBucket;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public int getScreenDPI() {
        return screenDPI;
    }

    public void setScreenDPI(int screenDPI) {
        this.screenDPI = screenDPI;
    }

    public float getScreenLogicalDensity() {
        return screenLogicalDensity;
    }

    public void setScreenLogicalDensity(float screenLogicalDensity) {
        this.screenLogicalDensity = screenLogicalDensity;
    }

    public float getScreenScaledDensity() {
        return screenScaledDensity;
    }

    public void setScreenScaledDensity(float screenScaledDensity) {
        this.screenScaledDensity = screenScaledDensity;
    }

    public int getScreenUsableWidth() {
        return screenUsableWidth;
    }

    public void setScreenUsableWidth(int screenUsableWidth) {
        this.screenUsableWidth = screenUsableWidth;
    }

    public int getScreenUsableHeight() {
        return screenUsableHeight;
    }

    public void setScreenUsableHeight(int screenUsableHeight) {
        this.screenUsableHeight = screenUsableHeight;
    }

    public int getScreenTotalWidth() {
        return screenTotalWidth;
    }

    public void setScreenTotalWidth(int screenTotalWidth) {
        this.screenTotalWidth = screenTotalWidth;
    }

    public int getScreenTotalHeight() {
        return screenTotalHeight;
    }

    public void setScreenTotalHeight(int screenTotalHeight) {
        this.screenTotalHeight = screenTotalHeight;
    }

    public String getScreenDefaultOrientation() {
        return screenDefaultOrientation;
    }

    public void setScreenDefaultOrientation(String screenDefaultOrientation) {
        this.screenDefaultOrientation = screenDefaultOrientation;
    }
}
