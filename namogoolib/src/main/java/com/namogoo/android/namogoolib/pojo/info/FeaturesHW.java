package com.namogoo.android.namogoolib.pojo.info;

public class FeaturesHW {

    private String featureLable;
    private String featureValue;

    public FeaturesHW(String featureLable, String featureValue) {
        this.featureLable = featureLable;
        this.featureValue = featureValue;
    }

    public String getFeatureLable() {
        return featureLable;
    }

    public void setFeatureLable(String featureLable) {
        this.featureLable = featureLable;
    }

    public String getFeatureValue() {
        return featureValue;
    }

    public void setFeatureValue(String featureValue) {
        this.featureValue = featureValue;
    }
}
