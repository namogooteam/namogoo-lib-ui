package com.namogoo.android.namogoolib.pojo.info;

import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

@Keep
public class FeaturesPojo extends BasePojo {
    private boolean wifi;
    private boolean wifiDirect;
    private boolean bluetooth;
    private boolean bluetoothLE;
    private boolean gps;
    private boolean cameraFlash;
    private boolean cameraFront;
    private boolean microphone;
    private boolean nfc;
    private boolean usbHost;
    private boolean usbAccessory;
    private boolean multitouch;
    private boolean audioLowLatency;
    private boolean audioOutput;
    private boolean professionalAudio;
    private boolean consumerIR;
    private boolean gamepadSupport;
    private boolean hifiSensor;
    private boolean printing;
    private boolean cdma;
    private boolean gsm;
    private boolean fingerprint;
    private boolean appWidgets;
    private boolean ip;
    private boolean ipBasedVOIP;

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public boolean isWifiDirect() {
        return wifiDirect;
    }

    public void setWifiDirect(boolean wifiDirect) {
        this.wifiDirect = wifiDirect;
    }

    public boolean isBluetooth() {
        return bluetooth;
    }

    public void setBluetooth(boolean bluetooth) {
        this.bluetooth = bluetooth;
    }

    public boolean isBluetoothLE() {
        return bluetoothLE;
    }

    public void setBluetoothLE(boolean bluetoothLE) {
        this.bluetoothLE = bluetoothLE;
    }

    public boolean isGps() {
        return gps;
    }

    public void setGps(boolean gps) {
        this.gps = gps;
    }

    public boolean isCameraFlash() {
        return cameraFlash;
    }

    public void setCameraFlash(boolean cameraFlash) {
        this.cameraFlash = cameraFlash;
    }

    public boolean isCameraFront() {
        return cameraFront;
    }

    public void setCameraFront(boolean cameraFront) {
        this.cameraFront = cameraFront;
    }

    public boolean isMicrophone() {
        return microphone;
    }

    public void setMicrophone(boolean microphone) {
        this.microphone = microphone;
    }

    public boolean isNfc() {
        return nfc;
    }

    public void setNfc(boolean nfc) {
        this.nfc = nfc;
    }

    public boolean isUsbHost() {
        return usbHost;
    }

    public void setUsbHost(boolean usbHost) {
        this.usbHost = usbHost;
    }

    public boolean isUsbAccessory() {
        return usbAccessory;
    }

    public void setUsbAccessory(boolean usbAccessory) {
        this.usbAccessory = usbAccessory;
    }

    public boolean isMultitouch() {
        return multitouch;
    }

    public void setMultitouch(boolean multitouch) {
        this.multitouch = multitouch;
    }

    public boolean isAudioLowLatency() {
        return audioLowLatency;
    }

    public void setAudioLowLatency(boolean audioLowLatency) {
        this.audioLowLatency = audioLowLatency;
    }

    public boolean isAudioOutput() {
        return audioOutput;
    }

    public void setAudioOutput(boolean audioOutput) {
        this.audioOutput = audioOutput;
    }

    public boolean isProfessionalAudio() {
        return professionalAudio;
    }

    public void setProfessionalAudio(boolean professionalAudio) {
        this.professionalAudio = professionalAudio;
    }

    public boolean isConsumerIR() {
        return consumerIR;
    }

    public void setConsumerIR(boolean consumerIR) {
        this.consumerIR = consumerIR;
    }

    public boolean isGamepadSupport() {
        return gamepadSupport;
    }

    public void setGamepadSupport(boolean gamepadSupport) {
        this.gamepadSupport = gamepadSupport;
    }

    public boolean isHifiSensor() {
        return hifiSensor;
    }

    public void setHifiSensor(boolean hifiSensor) {
        this.hifiSensor = hifiSensor;
    }

    public boolean isPrinting() {
        return printing;
    }

    public void setPrinting(boolean printing) {
        this.printing = printing;
    }

    public boolean isCdma() {
        return cdma;
    }

    public void setCdma(boolean cdma) {
        this.cdma = cdma;
    }

    public boolean isGsm() {
        return gsm;
    }

    public void setGsm(boolean gsm) {
        this.gsm = gsm;
    }

    public boolean isFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(boolean fingerprint) {
        this.fingerprint = fingerprint;
    }

    public boolean isAppWidgets() {
        return appWidgets;
    }

    public void setAppWidgets(boolean appWidgets) {
        this.appWidgets = appWidgets;
    }

    public boolean isIp() {
        return ip;
    }

    public void setIp(boolean ip) {
        this.ip = ip;
    }

    public boolean isIpBasedVOIP() {
        return ipBasedVOIP;
    }

    public void setIpBasedVOIP(boolean ipBasedVOIP) {
        this.ipBasedVOIP = ipBasedVOIP;
    }
}
