package com.namogoo.android.namogoolib.pojo.info;

import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

@Keep
public class NetworkPojo extends BasePojo {
    public enum NetworkStatus {
        DISCONNECTED,
        WIFI,
        NETWORK,
    }
    private boolean vpnConnection;

    private String ipAddress;
    private NetworkStatus networkStatus;
    private long linkSpeed;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public NetworkStatus getNetworkStatus() {
        return networkStatus;
    }

    public void setNetworkStatus(NetworkStatus networkStatus) {
        this.networkStatus = networkStatus;
    }

    public long getLinkSpeed() {
        return linkSpeed;
    }

    public void setLinkSpeed(long linkSpeed) {
        this.linkSpeed = linkSpeed;
    }

    public boolean isVpnConnection() {
        return vpnConnection;
    }

    public void setVpnConnection(boolean vpnConnection) {
        this.vpnConnection = vpnConnection;
    }
}
