package com.namogoo.android.namogoolib.pojo.info;

import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

@Keep
public class ProcessorPojo extends BasePojo {

    private long freeMemory;
    private long totalMemory;
    private int totalCPUs;

    public long getUsedMemory() {
        return totalMemory - freeMemory;
    }

    public long getFreeMemory() {
        return freeMemory;
    }

    public void setFreeMemory(long freeMemory) {
        this.freeMemory = freeMemory;
    }

    public long getTotalMemory() {
        return totalMemory;
    }

    public void setTotalMemory(long totalMemory) {
        this.totalMemory = totalMemory;
    }

    public int getTotalCPUs() {
        return totalCPUs;
    }

    public void setTotalCPUs(int totalCPUs) {
        this.totalCPUs = totalCPUs;
    }
}
