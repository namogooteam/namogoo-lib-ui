package com.namogoo.android.namogoolib.pojo.info;


import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

import java.util.ArrayList;

@Keep
public class SegmentationPojo extends BasePojo {

    public enum Segment {
        PRIVACY_SENSITIVE,
        PRICE_SENSITIVE,
        TECH_SAVVY,
        NAMOGOO_GROUPIE
    }

    private ArrayList<Segment> segments;

    public SegmentationPojo() {
        segments = new ArrayList<>();
    }

    public synchronized void addSegment(Segment segment, boolean add)
    {
        if (add) {
            if (segments.contains(segment))
                return;
            segments.add(segment);
        } else
        {
            if (!segments.contains(segment))
                return;
            segments.remove(segment);
        }
    }

    public ArrayList<Segment> getSegments() {
        return segments;
    }
}
