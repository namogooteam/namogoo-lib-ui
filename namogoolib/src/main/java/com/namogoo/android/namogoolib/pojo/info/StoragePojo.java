package com.namogoo.android.namogoolib.pojo.info;

import androidx.annotation.Keep;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

@Keep
public class StoragePojo extends BasePojo {
    private long total;
    private long available;

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getAvailable() {
        return available;
    }

    public void setAvailable(long available) {
        this.available = available;
    }

    public long used()
    {
        return total - available;
    }
}
