package com.namogoo.android.namogoolib.pojo.info;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

public class WebViewSendDataPojo extends BasePojo {
    private String uuid;
    private String sess_id;
    private int pv;

    public WebViewSendDataPojo(String sessionId, int pageNumber) {
        this.uuid = uuid;
        this.sess_id = sessionId;
        this.pv = pageNumber;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setCurrentPage(int pageNumber) {
        this.pv = pageNumber;
    }
}
