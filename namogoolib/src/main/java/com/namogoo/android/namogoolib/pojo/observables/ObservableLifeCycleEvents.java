package com.namogoo.android.namogoolib.pojo.observables;

import android.app.Activity;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.namogoo.android.namogoolib.pojo.events.LifeCycleEventPojo;

public class ObservableLifeCycleEvents extends BaseObservable {

    private LifeCycleEventPojo lifeCycleEventPojo;
    private Activity lastKnownActivity;

    @Bindable
    public LifeCycleEventPojo getLifeCycleEventPojo() {
        return lifeCycleEventPojo;
    }


    public void setLifeCycleEventPojo(LifeCycleEventPojo lifeCycleEventPojo) {
        this.lifeCycleEventPojo = lifeCycleEventPojo;
        notifyChange();
    }

    public Activity getLastKnownActivity() {
        return lastKnownActivity;
    }

    public void setLastKnownActivity(Activity lastKnownActivity) {
        this.lastKnownActivity = lastKnownActivity;
    }
}
