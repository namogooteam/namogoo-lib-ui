package com.namogoo.android.namogoolib.pojo.observables;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent;

public class ObservableReportCustomerJourneyEvent extends BaseObservable {
    private CustomerJourneyEvent customerJourneyEvent;

    @Bindable
    public CustomerJourneyEvent getCustomerJourneyEvent() {
        return customerJourneyEvent;
    }


    public void setCustomerJourneyEvent(CustomerJourneyEvent customerJourneyEvent) {
        this.customerJourneyEvent = customerJourneyEvent;
        notifyChange();
    }
}
