package com.namogoo.android.namogoolib.pojo.observables;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.namogoo.android.namogoolib.pojo.events.UserInteractionEventPojo;

public class ObservableUserInteractionsEvents extends BaseObservable {

    private UserInteractionEventPojo userInteractionEventPojo;

    @Bindable
    public UserInteractionEventPojo getUserInteractionEventPojo() {
        return userInteractionEventPojo;
    }

    public void setUserInteractionEventPojo(UserInteractionEventPojo userInteractionEventPojo) {
        this.userInteractionEventPojo = userInteractionEventPojo;
        notifyChange();
    }
}
