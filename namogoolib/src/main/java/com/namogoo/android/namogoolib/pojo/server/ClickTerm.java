package com.namogoo.android.namogoolib.pojo.server;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

public class ClickTerm extends BasePojo {
    private String searchTerm;
    private String ruleName;
    private String ruleId;
    private String targetClass;
    private String viewContainer;

    public String getSearchTerm() {
        return searchTerm;
    }

    public String getTargetClass() {
        return targetClass;
    }

    public String getViewContainer() {
        return viewContainer;
    }

    public String getRuleName() {
        return ruleName;
    }

    public String getRuleId() {
        return ruleId;
    }
}
