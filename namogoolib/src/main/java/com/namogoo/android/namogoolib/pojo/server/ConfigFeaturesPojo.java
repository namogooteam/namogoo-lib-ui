package com.namogoo.android.namogoolib.pojo.server;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

public class ConfigFeaturesPojo extends BasePojo {

    private Boolean webViewRemoveSubdomain;
    private String webViewService;

    public ConfigFeaturesPojo(Boolean webViewRemoveSubdomain, String webViewService) {
        this.webViewRemoveSubdomain = webViewRemoveSubdomain;
        this.webViewService = webViewService;
    }

    public Boolean getWebViewRemoveSubdomain() {
        return webViewRemoveSubdomain;
    }

    public String getWebViewService() {
        return webViewService;
    }
}
