package com.namogoo.android.namogoolib.pojo.server;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

public class CustomerJourneyConfigRequestPojo extends BasePojo {
    private String userAgent;
    private String vendorId;
    private String displaySize;

    public CustomerJourneyConfigRequestPojo(String userAgent, String vendorId, String displaySize) {
        this.userAgent = userAgent;
        this.vendorId = vendorId;
        this.displaySize = displaySize;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getVendorId() {
        return vendorId;
    }

    public String getDisplaySize() {
        return displaySize;
    }
}
