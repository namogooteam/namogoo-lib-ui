package com.namogoo.android.namogoolib.pojo.server;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomerJourneyConfigResponsePojo extends BasePojo {
    private EventTypesConfig eventTypesConfig;
    private HashMap<String, ArrayList<ClickTerm>> clickKeyMap;
    private Long updateDate;
    private ConfigFeaturesPojo features;

    public CustomerJourneyConfigResponsePojo(EventTypesConfig eventTypesConfig, ConfigFeaturesPojo features) {
        this.eventTypesConfig = eventTypesConfig;
        this.features = features;
    }

    public EventTypesConfig getEventTypesConfig() {
        return eventTypesConfig;
    }

    public HashMap<String, ArrayList<ClickTerm>> getClickKeyMap() {
        return clickKeyMap;
    }

    public Long getUpdateDate() {
        return updateDate;
    }

    public ConfigFeaturesPojo getFeatures() {
        return features;
    }
}
