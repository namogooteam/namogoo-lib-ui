package com.namogoo.android.namogoolib.pojo.server;

import com.namogoo.android.namogoolib.pojo.base.BasePojo;

public class EventTypesConfig extends BasePojo {
    private Boolean enableLifecycle;
    private Boolean enableUIButton;
    private Boolean enableUIControl;
    private Boolean enableWebView;

    public EventTypesConfig(Boolean enableLifecycle, Boolean enableUIButton, Boolean enableUIControl, Boolean enableWebView) {
        this.enableLifecycle = enableLifecycle;
        this.enableUIButton = enableUIButton;
        this.enableUIControl = enableUIControl;
        this.enableWebView = enableWebView;
    }

    public Boolean getEnableLifecycle() {
        return enableLifecycle;
    }

    public Boolean getEnableUIButton() {
        return enableUIButton;
    }

    public Boolean getEnableUIControl() {
        return enableUIControl;
    }

    public Boolean getEnableWebView() {
        return enableWebView;
    }
}
