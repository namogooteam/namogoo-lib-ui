package com.namogoo.android.namogoolib.pojo.server;

import com.google.gson.annotations.SerializedName;
import com.namogoo.android.namogoolib.pojo.base.BasePojo;

public class InitSessionDispatchPojo extends BasePojo {

    @SerializedName("ua")
    private String userAgent;

    @SerializedName("sdk_version")
    private String sdkVersion;

    @SerializedName("clienttag")
    private String vendorId;

    @SerializedName("uuid")
    private String customerId;

    @SerializedName("client_attr1")
    private String vendorSessionId;

    @SerializedName("client_attr2")
    private String vendorCustomerId;

    @SerializedName("sessionid")
    private String sessionId;

    private String os;

    @SerializedName("osversion")
    private String osVersion;

    @SerializedName("domain")
    private String hostPackage;

    @SerializedName("domain_version")
    private String hostVersion;

    @SerializedName("manufacturer")
    private String manufacturer;

    @SerializedName("model")
    private String model;

    @SerializedName("local_time_zone")
    private String localTimeZone;

    @SerializedName("default_language")
    private String defaultLanguage;

    @SerializedName("last_reboot")
    private Long lastOSReboot;

    @SerializedName("number_cpus")
    private Integer numberOfCPUs;

    @SerializedName("ram_usage")
    private Long ramUsage;

    @SerializedName("ram_free")
    private Long ramFree;

    @SerializedName("power_is_charching")
    private Integer powerIsCharging;

    @SerializedName("power_percent")
    private Integer powerPercent;

    @SerializedName("internet_connection")
    private String internetConnection;

    @SerializedName("local_ip")
    private String localIP;

    @SerializedName("vpn_connection")
    private Integer vpnConnection;

    @SerializedName("connection_speed")
    private Long connectionSpeed;

    @SerializedName("internal_storage_used")
    private Long internalStorageUsed;

    @SerializedName("internal_storage_available")
    private Long internalStorageAvailable;

    @SerializedName("external_storage_used")
    private Long externalStorageUsed;

    @SerializedName("external_storage_available")
    private Long externalStorageAvailable;

    @SerializedName("screen_width")
    private Integer screenWidth;

    @SerializedName("screen_height")
    private Integer screenHeight;

    @SerializedName("dpi")
    private Integer dpi;

    @SerializedName("physical_size")
    private Double physicalSize;

    @SerializedName("user_apps")
    private String userApps;

    @SerializedName("timestamp")
    private String timeStamp;

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getSdkVersion() {
        return sdkVersion;
    }

    public void setSdkVersion(String sdkVersion) {
        this.sdkVersion = sdkVersion;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getVendorSessionId() {
        return vendorSessionId;
    }

    public void setVendorSessionId(String vendorSessionId) {
        this.vendorSessionId = vendorSessionId;
    }

    public String getVendorCustomerId() {
        return vendorCustomerId;
    }

    public void setVendorCustomerId(String vendorCustomerId) {
        this.vendorCustomerId = vendorCustomerId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getHostPackage() {
        return hostPackage;
    }

    public void setHostPackage(String hostPackage) {
        this.hostPackage = hostPackage;
    }

    public String getHostVersion() {
        return hostVersion;
    }

    public void setHostVersion(String hostVersion) {
        this.hostVersion = hostVersion;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLocalTimeZone() {
        return localTimeZone;
    }

    public void setLocalTimeZone(String localTimeZone) {
        this.localTimeZone = localTimeZone;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public Long getLastOSReboot() {
        return lastOSReboot;
    }

    public void setLastOSReboot(Long lastOSReboot) {
        this.lastOSReboot = lastOSReboot;
    }

    public Integer getNumberOfCPUs() {
        return numberOfCPUs;
    }

    public void setNumberOfCPUs(Integer numberOfCPUs) {
        this.numberOfCPUs = numberOfCPUs;
    }

    public Long getRamUsage() {
        return ramUsage;
    }

    public void setRamUsage(Long ramUsage) {
        this.ramUsage = ramUsage;
    }

    public Long getRamFree() {
        return ramFree;
    }

    public void setRamFree(Long ramFree) {
        this.ramFree = ramFree;
    }

    public Integer getPowerIsCharging() {
        return powerIsCharging;
    }

    public void setPowerIsCharging(Integer powerIsCharging) {
        this.powerIsCharging = powerIsCharging;
    }

    public Integer getPowerPercent() {
        return powerPercent;
    }

    public void setPowerPercent(Integer powerPercent) {
        this.powerPercent = powerPercent;
    }

    public String getInternetConnection() {
        return internetConnection;
    }

    public void setInternetConnection(String internetConnection) {
        this.internetConnection = internetConnection;
    }

    public String getLocalIP() {
        return localIP;
    }

    public void setLocalIP(String localIP) {
        this.localIP = localIP;
    }

    public Integer getVpnConnection() {
        return vpnConnection;
    }

    public void setVpnConnection(Integer vpnConnection) {
        this.vpnConnection = vpnConnection;
    }

    public Long getConnectionSpeed() {
        return connectionSpeed;
    }

    public void setConnectionSpeed(Long connectionSpeed) {
        this.connectionSpeed = connectionSpeed;
    }

    public Long getInternalStorageUsed() {
        return internalStorageUsed;
    }

    public void setInternalStorageUsed(Long internalStorageUsed) {
        this.internalStorageUsed = internalStorageUsed;
    }

    public Long getInternalStorageAvailable() {
        return internalStorageAvailable;
    }

    public void setInternalStorageAvailable(Long internalStorageAvailable) {
        this.internalStorageAvailable = internalStorageAvailable;
    }

    public Long getExternalStorageUsed() {
        return externalStorageUsed;
    }

    public void setExternalStorageUsed(Long externalStorageUsed) {
        this.externalStorageUsed = externalStorageUsed;
    }

    public Long getExternalStorageAvailable() {
        return externalStorageAvailable;
    }

    public void setExternalStorageAvailable(Long externalStorageAvailable) {
        this.externalStorageAvailable = externalStorageAvailable;
    }

    public Integer getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(Integer screenWidth) {
        this.screenWidth = screenWidth;
    }

    public Integer getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(Integer screenHeight) {
        this.screenHeight = screenHeight;
    }

    public Integer getDpi() {
        return dpi;
    }

    public void setDpi(Integer dpi) {
        this.dpi = dpi;
    }

    public Double getPhysicalSize() {
        return physicalSize;
    }

    public void setPhysicalSize(Double physicalSize) {
        this.physicalSize = physicalSize;
    }

    public String getUserApps() {
        return userApps;
    }

    public void setUserApps(String userApps) {
        this.userApps = userApps;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
