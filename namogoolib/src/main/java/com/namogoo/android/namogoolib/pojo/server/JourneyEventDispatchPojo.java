package com.namogoo.android.namogoolib.pojo.server;

import com.google.gson.annotations.SerializedName;
import com.namogoo.android.namogoolib.pojo.base.BasePojo;
import com.namogoo.android.namogoolib.pojo.enums.EventType;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.namogoo.android.namogoolib.helper.KeyUtil.EVENT_TIME_STAMP_FORMAT;

public class JourneyEventDispatchPojo extends BasePojo {

    @SerializedName("cb")
    private Long eventTime;

    @SerializedName("eventname")
    private String eventName;

    @SerializedName("eventtype")
    private EventType eventType;

    @SerializedName("targetid")
    private String targetId;

    @SerializedName("sessionid")
    private String sessionId;

    private String value;

    @SerializedName("pv")
    private Integer pageView;

    @SerializedName("client_attr1")
    private String vendorSessionId;

    @SerializedName("client_attr2")
    private String vendorCustomerId;

    @SerializedName("rulename")
    private String ruleName;

    @SerializedName("timestamp")
    private String timeStamp;

    public Long getEventTime() {
        return eventTime;
    }

    public String getEventName() {
        return eventName;
    }

    public EventType getEventType() {
        return eventType;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getValue() {
        return value;
    }

    public Integer getPageView() {
        return pageView;
    }

    public void setEventTime(Long eventTime) {
        this.eventTime = eventTime;
        Date date=new Date(eventTime);
        SimpleDateFormat df2 = new SimpleDateFormat(EVENT_TIME_STAMP_FORMAT);
        this.timeStamp = df2.format(date);
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setPageView(Integer pageView) {
        this.pageView = pageView;
    }

    public String getVendorSessionId() {
        return vendorSessionId;
    }

    public void setVendorSessionId(String vendorSessionId) {
        this.vendorSessionId = vendorSessionId;
    }

    public String getVendorCustomerId() {
        return vendorCustomerId;
    }

    public void setVendorCustomerId(String vendorCustomerId) {
        this.vendorCustomerId = vendorCustomerId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
}
