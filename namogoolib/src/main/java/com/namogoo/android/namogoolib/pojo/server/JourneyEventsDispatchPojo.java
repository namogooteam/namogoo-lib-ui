package com.namogoo.android.namogoolib.pojo.server;

import com.google.gson.annotations.SerializedName;
import com.namogoo.android.namogoolib.NMGLibrary;
import com.namogoo.android.namogoolib.pojo.base.BasePojo;

import java.util.ArrayList;

public class JourneyEventsDispatchPojo extends BasePojo {

    private Long dispatchTime;
    private ArrayList<JourneyEventDispatchPojo> journeyEvents;
    private Integer count;

    @SerializedName("ua")
    private String userAgent;

    @SerializedName("clienttag")
    private String vendorId;

    @SerializedName("uuid")
    private String customerId;

    private String os;

    @SerializedName("osversion")
    private String osVersion;

    @SerializedName("domain")
    private String hostPackage;

    public JourneyEventsDispatchPojo(ArrayList<JourneyEventDispatchPojo> journeyEvents, Integer count, String userAgent, String vendorId, String customerId)
    {
        this.journeyEvents = journeyEvents;
        this.count = count;
        this.userAgent = userAgent;
        this.vendorId = vendorId;
        this.customerId = customerId;
        this.dispatchTime = System.currentTimeMillis();

        this.os = "Android";
        this.osVersion = String.valueOf(NMGLibrary.getInstance().getDeviceInfo().getOS().getApiLevel());
        this.hostPackage = NMGLibrary.getInstance().getDeviceInfo().getBuild().getPackageName();
    }

    public Long getDispatchTime() {
        return dispatchTime;
    }

    public ArrayList<JourneyEventDispatchPojo> getJourneyEvents() {
        return journeyEvents;
    }

    public Integer getCount() {
        return count;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getVendorId() {
        return vendorId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getOs() {
        return os;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getHostPackage() {
        return hostPackage;
    }
}
