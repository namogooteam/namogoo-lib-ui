package com.namogoo.android.namogoolib.reporter;

import android.content.Context;
import android.os.AsyncTask;

import com.namogoo.android.namogoolib.NMGLibrary;
import com.namogoo.android.namogoolib.db.dao.DeviceDataScribe;
import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent;
import com.namogoo.android.namogoolib.helper.Config;
import com.namogoo.android.namogoolib.helper.Utils;
import com.namogoo.android.namogoolib.info.DeviceInfoManager;
import com.namogoo.android.namogoolib.listener.BatteryListener;
import com.namogoo.android.namogoolib.pojo.info.BatteryPojo;
import com.namogoo.android.namogoolib.pojo.server.CustomerJourneyConfigRequestPojo;
import com.namogoo.android.namogoolib.pojo.server.CustomerJourneyConfigResponsePojo;
import com.namogoo.android.namogoolib.pojo.server.InitSessionDispatchPojo;
import com.namogoo.android.namogoolib.pojo.server.JourneyEventDispatchPojo;
import com.namogoo.android.namogoolib.pojo.server.JourneyEventsDispatchPojo;
import com.namogoo.android.namogoolib.reporter.api.Callback;
import com.namogoo.android.namogoolib.reporter.api.WebApiRequest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.namogoo.android.namogoolib.helper.KeyUtil.EVENT_TIME_STAMP_FORMAT;

public class DataDispatcher {
    private boolean isSending;
    private WebApiRequest webApiRequest;
    private DeviceDataScribe deviceDataScribe;

    private Object syncObj;

    public DataDispatcher(Context context, DeviceDataScribe deviceDataScribe) {
        syncObj = new Object();
        webApiRequest = new WebApiRequest(Config.getBaseUrl(context));
        this.deviceDataScribe = deviceDataScribe;
    }

    public void sendCustomerJourneyEvents(ArrayList<CustomerJourneyEvent> customerJourneyEvents, Callback<String> callback) {
        if (customerJourneyEvents.isEmpty())
        {
            return;
        }
        final ArrayList<JourneyEventDispatchPojo> sendingCustomerJourneyEvents = new ArrayList<>();
        synchronized (syncObj){
            for (CustomerJourneyEvent customerJourneyEvent: customerJourneyEvents) {
                JourneyEventDispatchPojo journeyEventDispatchPojo = new JourneyEventDispatchPojo ();
                journeyEventDispatchPojo.setEventTime(customerJourneyEvent.eventTime);
                journeyEventDispatchPojo.setSessionId(customerJourneyEvent.sessionId);
                journeyEventDispatchPojo.setEventName(customerJourneyEvent.lifecycleEventData != null ? "lifecycle" : "userinteraction");
                journeyEventDispatchPojo.setEventType(customerJourneyEvent.lifecycleEventData != null ? customerJourneyEvent.lifecycleEventData.getEventType() : customerJourneyEvent.userInteractionEventData.getEventType());
                journeyEventDispatchPojo.setTargetId(customerJourneyEvent.lifecycleEventData != null ? customerJourneyEvent.lifecycleEventData.getTargetId() : customerJourneyEvent.userInteractionEventData.getTargetId());
                journeyEventDispatchPojo.setValue(customerJourneyEvent.lifecycleEventData != null ? Utils.mapToJsonString(customerJourneyEvent.lifecycleEventData.getIntentData()) : customerJourneyEvent.userInteractionEventData.getValue());
                journeyEventDispatchPojo.setPageView(customerJourneyEvent.lifecycleEventData != null ? customerJourneyEvent.lifecycleEventData.getPageView() : customerJourneyEvent.userInteractionEventData.getPageView());
                journeyEventDispatchPojo.setVendorSessionId(customerJourneyEvent.vendorSessionId);
                journeyEventDispatchPojo.setVendorCustomerId(customerJourneyEvent.vendorCustomerId);
                journeyEventDispatchPojo.setRuleName(customerJourneyEvent.lifecycleEventData != null ? customerJourneyEvent.lifecycleEventData.getRuleName() : customerJourneyEvent.userInteractionEventData.getRuleName());
                sendingCustomerJourneyEvents.add(journeyEventDispatchPojo);
            }
        }
        JourneyEventsDispatchPojo journeyEventsDispatchPojo = new JourneyEventsDispatchPojo(sendingCustomerJourneyEvents, sendingCustomerJourneyEvents.size(), Utils.getUserAgent(), NMGLibrary.getInstance().getVendorId(), getCustomerId());

        webApiRequest.postEvents(journeyEventsDispatchPojo, callback);

    }

    private String getCustomerId() {
        return deviceDataScribe.getDeviceUUID();
    }

    public void sendSessionInit(final String sessionId) {
        getBatteryInfo(sessionId);
    }

    private void getBatteryInfo(final String sessionId) {
        NMGLibrary nmgLibrary = NMGLibrary.getInstance();
        final DeviceInfoManager deviceInfoManager = nmgLibrary.getDeviceInfo();

        BatteryListener batteryListener = new BatteryListener() {
            @Override
            public void batteryValueChange(final BatteryPojo batteryPojo) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        createdSessionInitAndSend(sessionId, batteryPojo);
                        return null;
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                deviceInfoManager.unregisterBatteryListener();
            }
        };
        deviceInfoManager.registerBatteryListener(batteryListener);
    }

    private void createdSessionInitAndSend(String sessionId, BatteryPojo batteryPojo) {
        NMGLibrary nmgLibrary = NMGLibrary.getInstance();
        final DeviceInfoManager deviceInfoManager = nmgLibrary.getDeviceInfo();

        InitSessionDispatchPojo initSessionDispatchPojo = new InitSessionDispatchPojo();
        initSessionDispatchPojo.setVendorId(nmgLibrary.getVendorId());
        initSessionDispatchPojo.setUserAgent(Utils.getUserAgent());
        initSessionDispatchPojo.setSdkVersion(nmgLibrary.getVersion());
        initSessionDispatchPojo.setCustomerId(getCustomerId());
        initSessionDispatchPojo.setVendorSessionId(nmgLibrary.getVendorSessionId());
        initSessionDispatchPojo.setVendorCustomerId(nmgLibrary.getVendorCustomerId());
        initSessionDispatchPojo.setSessionId(sessionId);
        initSessionDispatchPojo.setOs("Android");
        initSessionDispatchPojo.setOsVersion(String.valueOf(deviceInfoManager.getOS().getApiLevel()));
        initSessionDispatchPojo.setHostPackage(deviceInfoManager.getBuild().getPackageName());
        initSessionDispatchPojo.setHostVersion(deviceInfoManager.getBuild().getPackageVersion());
        initSessionDispatchPojo.setManufacturer(deviceInfoManager.getBuild().getManufacturer());
        initSessionDispatchPojo.setModel(deviceInfoManager.getBuild().getModel());
        initSessionDispatchPojo.setLocalTimeZone(deviceInfoManager.getBuild().getDateTimeLocation());
        initSessionDispatchPojo.setDefaultLanguage(deviceInfoManager.getBuild().getDefaultLanguage());
        initSessionDispatchPojo.setLastOSReboot(deviceInfoManager.getBuild().getLastReboot());
        initSessionDispatchPojo.setNumberOfCPUs(deviceInfoManager.getProcessor().getTotalCPUs());
        initSessionDispatchPojo.setRamUsage(deviceInfoManager.getProcessor().getUsedMemory());
        initSessionDispatchPojo.setRamFree(deviceInfoManager.getProcessor().getFreeMemory());
        initSessionDispatchPojo.setPowerIsCharging(batteryPojo.getPlugged());
        int powerPercent = batteryPojo.getLevel();
        if (batteryPojo.getScale() != 0 && batteryPojo.getScale() != 100)
        {
            powerPercent = (batteryPojo.getLevel() * 100) / batteryPojo.getScale();
        }
        if (deviceInfoManager.getNetwork().getNetworkStatus() != null) {
            initSessionDispatchPojo.setInternetConnection(deviceInfoManager.getNetwork().getNetworkStatus().name().toLowerCase());
        } else {
            initSessionDispatchPojo.setInternetConnection("NA");
        }
        initSessionDispatchPojo.setLocalIP(deviceInfoManager.getNetwork().getIpAddress());
        initSessionDispatchPojo.setVpnConnection(deviceInfoManager.getNetwork().isVpnConnection() ? 1 : 0);
        initSessionDispatchPojo.setConnectionSpeed(deviceInfoManager.getNetwork().getLinkSpeed());
        initSessionDispatchPojo.setInternalStorageUsed(deviceInfoManager.getStorage(true).getTotal() - deviceInfoManager.getStorage(true).getAvailable());
        initSessionDispatchPojo.setInternalStorageAvailable(deviceInfoManager.getStorage(true).getAvailable());
        if (deviceInfoManager.getStorage(false) != null && deviceInfoManager.getStorage(false).getTotal() != 0) {
            initSessionDispatchPojo.setExternalStorageAvailable(deviceInfoManager.getStorage(false).getAvailable());
            initSessionDispatchPojo.setExternalStorageUsed(deviceInfoManager.getStorage(false).getTotal() - deviceInfoManager.getStorage(false).getAvailable());
        }
        initSessionDispatchPojo.setScreenWidth(deviceInfoManager.getDisplay().getScreenTotalWidth());
        initSessionDispatchPojo.setScreenHeight(deviceInfoManager.getDisplay().getScreenTotalHeight());
        initSessionDispatchPojo.setDpi(deviceInfoManager.getDisplay().getScreenDPI());
        initSessionDispatchPojo.setPhysicalSize(deviceInfoManager.getDisplay().getPhysicalSize());

        Date date=new Date();
        SimpleDateFormat df2 = new SimpleDateFormat(EVENT_TIME_STAMP_FORMAT);
        initSessionDispatchPojo.setTimeStamp(df2.format(date));
        //initSessionDispatchPojo.setUserApps(deviceInfoManager.getApps(true));

        webApiRequest.postInitSession(initSessionDispatchPojo, null);
    }

    public void sendJourneyConfigRequest(Callback<CustomerJourneyConfigResponsePojo> callback) {
        NMGLibrary nmgLibrary = NMGLibrary.getInstance();
        CustomerJourneyConfigRequestPojo customerJourneyConfigRequestPojo = new CustomerJourneyConfigRequestPojo(Utils.getUserAgent(), nmgLibrary.getVendorId(), nmgLibrary.getDeviceInfo().getDisplay().getScreenDensityBucket());
        webApiRequest.postConfigRequest(customerJourneyConfigRequestPojo, callback);
    }
}
