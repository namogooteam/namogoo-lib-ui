package com.namogoo.android.namogoolib.reporter.api;


public interface Callback<T> {
    void onResponse(T response);
    void onFailure(Throwable t);
}
