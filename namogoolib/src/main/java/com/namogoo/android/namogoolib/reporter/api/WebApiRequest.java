package com.namogoo.android.namogoolib.reporter.api;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.namogoo.android.namogoolib.pojo.server.CustomerJourneyConfigRequestPojo;
import com.namogoo.android.namogoolib.pojo.server.CustomerJourneyConfigResponsePojo;
import com.namogoo.android.namogoolib.pojo.server.InitSessionDispatchPojo;
import com.namogoo.android.namogoolib.pojo.server.JourneyEventsDispatchPojo;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class WebApiRequest {

    private static final String CONFIG_PATH = "api/android/get-config";
    private static final String INIT_SESSION_PATH = "api/android/init-session";
    private static final String POST_EVENTS_PATH = "api/android/post-event";
    private Gson gson;
    private String baseUrl;

    public WebApiRequest(String baseUrl)
    {
        this.baseUrl = baseUrl;
        gson = new Gson();
    }

    public void postConfigRequest(CustomerJourneyConfigRequestPojo customerJourneyConfigRequestPojo, Callback<CustomerJourneyConfigResponsePojo> callback)  {
        postJSON(CONFIG_PATH, gson.toJson(customerJourneyConfigRequestPojo), callback, CustomerJourneyConfigResponsePojo.class);
    }

    public void postInitSession(InitSessionDispatchPojo initSessionDispatchPojo, Callback<String> callback)  {
        postJSON(INIT_SESSION_PATH, gson.toJson(initSessionDispatchPojo), callback, String.class);
    }

    public void postEvents(JourneyEventsDispatchPojo journeyEventsDispatchPojo, Callback<String> callback)  {
        postJSON(POST_EVENTS_PATH, gson.toJson(journeyEventsDispatchPojo), callback, String.class);
    }

    private <T> void postJSON(String path, String message, Callback callback, Class<T> tClass) {
        post(path, message, "application/json;charset=utf-8", callback, tClass);
    }

    private <T> void post(String path, String message, String messageContentType, Callback callback, Class<T> tClass) {
        new AsyncTask<Void, Void, String>() {
            Throwable t = null;
            @Override
            protected String doInBackground(Void... voids) {
                try {
                    URL url = new URL(baseUrl + path);

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000 /*milliseconds*/);
                    conn.setConnectTimeout(15000 /* milliseconds */);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setFixedLengthStreamingMode(message.getBytes().length);

                    //make some HTTP header nicety
                    conn.setRequestProperty("Content-Type", messageContentType);
                    //conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

                    //open
                    conn.connect();

                    //setup send
                    OutputStream os = new BufferedOutputStream(conn.getOutputStream());
                    os.write(message.getBytes());
                    //clean up
                    os.flush();
                    os.close();
                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                        br.close();
                        conn.disconnect();
                        return response.toString();
                    } catch (Exception e) {
                        t = e;
                        e.printStackTrace();
                    }
                    conn.disconnect();
                } catch (Exception e) {
                    t = e;
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (callback != null) {
                    if (t != null) {
                        callback.onFailure(t);
                        return;
                    }
                    if (s != null) {
                        if (tClass == String.class)
                        {
                            callback.onResponse(s);
                        } else {
                            callback.onResponse(gson.fromJson(s, tClass));
                        }
                    } else {
                        callback.onResponse(null);
                    }
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }
}
