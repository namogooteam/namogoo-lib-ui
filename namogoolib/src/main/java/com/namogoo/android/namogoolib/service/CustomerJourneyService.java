package com.namogoo.android.namogoolib.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.namogoo.android.namogoolib.NMGLibrary;
import com.namogoo.android.namogoolib.db.AppDatabase;
import com.namogoo.android.namogoolib.db.dao.DeviceDataScribe;
import com.namogoo.android.namogoolib.db.entities.CustomerJourneyEvent;
import com.namogoo.android.namogoolib.db.entities.JourneyConfig;
import com.namogoo.android.namogoolib.helper.Config;
import com.namogoo.android.namogoolib.pojo.info.NetworkPojo;
import com.namogoo.android.namogoolib.pojo.server.CustomerJourneyConfigResponsePojo;
import com.namogoo.android.namogoolib.reporter.DataDispatcher;
import com.namogoo.android.namogoolib.reporter.api.Callback;

import java.util.ArrayList;
import java.util.UUID;

public class CustomerJourneyService extends Service {

    public static String SEND_JOURNEY_DATA = "_send_journey_data";
    public static String INIT_JOURNEY_REQUEST = "_init_journey_session";
    public static String JOURNEY_CONFIG_REQUEST = "_journey_config_request";

    private static final int DAYS_TO_CUT_OFF = 30;
    private static final int A_DAY_IN_MILLI = 86400000;
    private static final int WAIT_BETWEEN_DB_CHECKS = 3000;

    private static AppDatabase appDB;
    private DataDispatcher dataDispatcher;
    private DeviceDataScribe deviceDataScribe;
    private ArrayList<CustomerJourneyEvent> eventsToSend;
    private boolean isSendingCycle;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //android.os.Debug.waitForDebugger();
        isSendingCycle = false;
        initDB();
        removeOldEventsFromDB();
        initSendEvents();
    }

    private Callback<String> customerJourneyEventsCallback = new Callback<String>() {
        @Override
        public void onResponse(String response) {
            journeyEventsDispatched(eventsToSend);
        }

        @Override
        public void onFailure(Throwable t) {
            if (!isConnectedToInternet()) {
                return;
            }
            journeyEventsDispatched(eventsToSend);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null || intent.getExtras() == null) {
            return START_NOT_STICKY;
        }

        if (intent.getExtras().get(SEND_JOURNEY_DATA) != null) {
            if (isSendingCycle)
            {
                return START_STICKY;
            }
            isSendingCycle = true;
            checkForMoreEventsToSend();
        } else {
            if (intent.getExtras().get(INIT_JOURNEY_REQUEST) != null) {
                sendInitSession(intent.getExtras().getString(INIT_JOURNEY_REQUEST));
            } else {
                if (intent.getExtras().get(JOURNEY_CONFIG_REQUEST) != null) {
                    sendJourneyConfigRequest();
                }
            }
        }
        return START_STICKY;
    }

    private void sendJourneyConfigRequest() {
        android.os.Debug.waitForDebugger();
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                Callback<CustomerJourneyConfigResponsePojo> journeyConfigCallback = new Callback<CustomerJourneyConfigResponsePojo>() {
                    @Override
                    public void onResponse(CustomerJourneyConfigResponsePojo response) {
                        if (response != null) {
                            updateConfigData(response);
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                    }
                };
                dataDispatcher.sendJourneyConfigRequest(journeyConfigCallback);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void updateConfigData(final CustomerJourneyConfigResponsePojo customerJourneyConfigResponse) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDB.journeyConfigDao().deleteAll();
                JourneyConfig journeyConfig = new JourneyConfig();
                journeyConfig.id = UUID.randomUUID().toString();
                journeyConfig.config = customerJourneyConfigResponse;
                appDB.journeyConfigDao().insertAll(journeyConfig);
                Config.setCustomerJourneyConfig(journeyConfig.config);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void initDB() {
        appDB = DeviceDataScribe.getAppDB();
    }

    private void removeOldEventsFromDB() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                long cutOffDateMilli = System.currentTimeMillis() - DAYS_TO_CUT_OFF*A_DAY_IN_MILLI;
                appDB.customerJourneyEventsDao().deleteOldEvents(cutOffDateMilli);
                return null;
            }
        };
    }

    private void initSendEvents() {
        deviceDataScribe = new DeviceDataScribe(getApplicationContext());
        dataDispatcher = new DataDispatcher(getApplicationContext(), deviceDataScribe);
        eventsToSend = new ArrayList<CustomerJourneyEvent>();
    }

    private void sendEventsIfNeeded() {
        eventsToSend.clear();
        eventsToSend.addAll(appDB.customerJourneyEventsDao().getAllNotSent());
        if (eventsToSend.size() == 0) {
            this.stopSelf();
            return;
        }
        dataDispatcher.sendCustomerJourneyEvents(eventsToSend, customerJourneyEventsCallback);

    }

    private void sendInitSession(final String sessionId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                dataDispatcher.sendSessionInit(sessionId);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void sendEventsIfNeededAsync() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                sendEventsIfNeeded();
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void journeyEventsDispatched(final ArrayList<CustomerJourneyEvent> sentCustomerJourneyEvents) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                for (CustomerJourneyEvent customerJourneyEvent : sentCustomerJourneyEvents) {
                    customerJourneyEvent.wasSent = true;
                    appDB.customerJourneyEventsDao().update(customerJourneyEvent);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                checkForMoreEventsToSend();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void checkForMoreEventsToSend() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sendEventsIfNeededAsync();
            }
        }, WAIT_BETWEEN_DB_CHECKS);
    }


    private boolean isConnectedToInternet() {
        return NMGLibrary.getInstance().getDeviceInfo().getNetwork().getNetworkStatus() != NetworkPojo.NetworkStatus.DISCONNECTED;
    }

}
