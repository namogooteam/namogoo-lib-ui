package com.namogoo.android.namogoolib.helper;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

public class EncryptionUnitTest {
    private static String expectedString;
    private static String actualEncryptedString;
    @BeforeClass
    public static void setup() {
        expectedString = UUID.randomUUID().toString();
        actualEncryptedString = Encryption.Encrypt(expectedString);
    }

    @Test
    public void Encrypt_Original_vs_Encrypted_Not_Equal() throws Exception {

        assertNotEquals(expectedString, actualEncryptedString);
    }

    @Test
    public void Encrypt_Original_vs_Encrypted_Decrypted_Equal() throws Exception {
        String actualDecryptedString = Encryption.Decrypt(actualEncryptedString);
        assertEquals(expectedString, actualDecryptedString);
    }

}