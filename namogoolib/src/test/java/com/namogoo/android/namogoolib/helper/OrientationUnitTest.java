package com.namogoo.android.namogoolib.helper;

import org.junit.Assert;
import org.junit.Test;
import java.util.UUID;
import static org.junit.Assert.*;

public class OrientationUnitTest {
    @Test
    public void isTextRTL_English_False()
    {
        assertFalse(Orientation.isTextRTL("en"));
    }

    @Test
    public void isTextRTL_Hebrew_True()
    {
        assertTrue(Orientation.isTextRTL("he"));
    }
}
